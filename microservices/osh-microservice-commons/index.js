const cookie = require('cookie');
const jwt = require('jsonwebtoken');

class VersionHolder {
    static version = '';

    static getVersion() {
        return this.version;
    }

    static setVersion(version) {
        this.version = version;
    }
}

class DBConnectionHolder {
    static instance;

    sessionRepository;

    constructor(
        sessionRepository
    ) {
        this.sessionRepository = sessionRepository;
    }

    static setConn(sessionRepository) {
        this.instance = new DBConnectionHolder(sessionRepository);
    }

    static getConn() {
        // TODO: fail save
        return this.instance;
    }

    findSessionByTokenOrFail(token) {
        return this.sessionRepository.findOneOrFail({ where: { token: token } });
    }
}

const configureSharedApiResources = (db, version) => {
    DBConnectionHolder.setConn(db);
    VersionHolder.setVersion(version);
}

function verifyAuthorization(authorization, next, res, preAuth) {
    if (authorization) {
        jwt.verify(authorization, preAuth ? process.env.JWT_PRE_AUTH_TOKEN_SECRET : process.env.JWT_TOKEN_SECRET, async (error, decoded) => {
            if (error) {
                const err = new Error(error);
                // eslint-disable-next-line
                // @ts-ignore
                err.status = 403;
                return next(err);
            }
            try {
                await DBConnectionHolder.getConn().findSessionByTokenOrFail(authorization);
                if (decoded.username && decoded.exp) {
                    if (preAuth) {
                        res.locals.preAuthorizationToken = authorization;
                    } else {
                        res.locals.authorizationToken = authorization;
                        res.setHeader('OSH-App-Version', VersionHolder.getVersion());
                    }
                    res.locals.decodedJwt = decoded;
                    return next();
                }
                const err = new Error('Not authorized!');
                // eslint-disable-next-line
                // @ts-ignore
                err.status = 401;
                return next(err);
            } catch (error) {
                const err = new Error('Unauthorized');
                // eslint-disable-next-line
                // @ts-ignore
                err.status = 401;
                return next(err);
            }
        });
    } else {
        const err = new Error('Not authorized! No cookie');
        // eslint-disable-next-line
        // @ts-ignore
        err.status = 403;
        return next(err);
    }
}

const isAuthorized = () => {
    return (req, res, next) => {
        if (!DBConnectionHolder.getConn()) {
            console.error('Not configured');
            return next(new Error());
        }

        const authorization = cookie.parse(req.headers.cookie || '').authorization;

        return verifyAuthorization(authorization, next, res);
    };
}

const isPreAuthorized = () => {
    return (req, res, next) => {
        if (!DBConnectionHolder.getConn()) {
            console.error('Not configured');
            return next(new Error());
        }

        const preAuthorization = cookie.parse(req.headers.cookie || '').pre_authorization;

        return verifyAuthorization(preAuthorization, next, res, true);
    };
}

module.exports.configureSharedApiResources = configureSharedApiResources;
module.exports.isAuthorized = isAuthorized;
module.exports.isPreAuthorized = isPreAuthorized;
