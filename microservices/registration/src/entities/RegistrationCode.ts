import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { User } from './User';

@Entity()
export class RegistrationCode {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({ unique: true })
    registrationCode: string;

    @ManyToOne(() => User, { nullable: true })
    @JoinColumn()
    user: User;

    @Column({ name: 'userId', nullable: true })
    userId: number;

    @Column()
    used: boolean;

}
