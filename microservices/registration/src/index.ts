import { createConnection } from 'typeorm';
import express from 'express';
import { DBConnectionHolder, RedisSingleton } from './types';
import { User } from './entities/User';
import { TypeOrm } from './Database';
import { RegistrationCode } from './entities/RegistrationCode';
import { registration } from './endpoints/registration';
import { health } from './endpoints/Health';
import { versionEndpoint } from './endpoints/VersionEndpoint';

require('dotenv').config();
const app = require('express')();
const http = require('http').Server(app);

RedisSingleton.getRedis();

app.use(express.json());

// Set Service Header
app.use(function (req, res, next) {
    res.setHeader('Service', process.env.HOSTNAME ? process.env.HOSTNAME : 'reg-service');
    next();
});

app.use('/health', health);
app.use('/version', versionEndpoint);

const port = process.env.PORT ? process.env.PORT : 3000;
http.listen(port, () => {
    return console.log(`server is listening on ${port}`);
});

createConnection().then(async connection => {

    const registrationCodeRepository = connection.getRepository(RegistrationCode);
    const userRepository = connection.getRepository(User);
    DBConnectionHolder.setConn(new TypeOrm(
        registrationCodeRepository,
        userRepository
    ));

    const apiPrefix = '/api/v1';
    app.use(apiPrefix + '/reg', registration);

}).catch(error => console.log(error));
