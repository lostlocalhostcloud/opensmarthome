import { Router } from 'express';
import { hasValidRegistrationSession } from '../security/Authentication';
import { authenticator } from 'otplib';
import { DBConnectionHolder, RedisSingleton } from '../types';

const sha512 = require('js-sha512');
const cookie = require('cookie');

export const registration = Router();

registration.post('/regcode', async (req, res) => {
    const regCode = req.body.regCode;

    try {
        await DBConnectionHolder.getConn().findActiveRegistrationCode(regCode);

        require('crypto').randomBytes(48, (err, buffer) => {
            const regSessionToken = buffer.toString('hex');
            const otpSecret = authenticator.generateSecret(32);
            RedisSingleton.getRedis().set(regSessionToken, otpSecret, { EX: 1200 });

            res.setHeader('Set-Cookie', cookie.serialize('regSession', regSessionToken, {
                httpOnly: true,
                path: '/api/v1/reg/',
                maxAge: 1200
            }));
            return res.status(200).send(otpSecret);
        });
    } catch (error) {
        return res.status(404).send('The requested Registration code was not found');
    }
});

registration.post('/otp/verify', hasValidRegistrationSession(), (req, res) => {
    const otpToken = req.body.otpToken;
    const baseSecret = req.body.baseSecret;

    if (authenticator.check(otpToken, baseSecret)) {
        return res.status(200).send('OK');
    }
    return res.status(409).send('OTP mismatch');
});

registration.post('/register/verify', hasValidRegistrationSession(), (req, res) => {
    const username = req.body.username;
    const emailAddress = req.body.emailAddress;
    DBConnectionHolder.getConn().isUsernameAndEmailUsed(username, emailAddress).then((used) => {
        if (!used) return res.status(200).send('OK');
        return res.status(409).send('Username or E-Mail in use');
    }).catch((e) => {
        console.error('Database Error /registration (Can \'t verify regCode) :', e);
        return res.status(501).send('Database Error');
    });
});

registration.post('/register', (req, res) => {
    // eslint-disable-next-line quotes
    const passwordRegex = new RegExp("^(?=(?:[^a-z]*[a-z]){2})(?=(?:[^0-9]*[0-9]){2})(?=.*[!-\\/:-@\\[-`{-~]).{8,}$");
    // eslint-disable-next-line quotes, no-control-regex
    const emailRegex = new RegExp("(?:[a-z0-9!#$%&'*+\\/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+\\/=?^_`{|}~-]+)*|\"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\\\[\x01-\x09\x0b\x0c\x0e-\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\\])");

    if (!passwordRegex.test(req.body.password)) {
        return res.status(501).send('Password must be at least 8 symbols long and must contain 2 symbols, 2 numbers and 2 letters');
    }

    const regCode = req.body.regCode;
    const username = req.body.username;
    const emailAddress = req.body.emailAddress;
    const baseSecret = req.body.baseSecret;
    const password = sha512(req.body.password + process.env.PASSWORD_SALT);
    const firstName = req.body?.firstName;
    const lastName = req.body?.lastName;

    if (!emailRegex.test(emailAddress)) {
        return res.status(501).send('Bad Email');
    }
    if (username.length < 1) {
        return res.status(501).send('Bad Username');
    }

    DBConnectionHolder.getConn().findActiveRegistrationCode(regCode).then((r) => {
        DBConnectionHolder.getConn().isUsernameAndEmailUsed(username, emailAddress).then((used) => {
            if (!used) {
                DBConnectionHolder.getConn().createUser(
                    username,
                    emailAddress,
                    password,
                    baseSecret,
                    firstName,
                    lastName
                ).then(value => {
                    DBConnectionHolder.getConn().deactivateRegistrationCode(regCode, value.id).then(() => {
                        return res.status(200).send('OK');
                    }).catch((e) => {
                        console.error('Database Error /registration (Can \'t disable regCode) :', e);
                        return res.status(501).send('Database Error');
                    });
                });
            } else {
                return res.status(409).send('Username or E-Mail in use');
            }
        }).catch((e) => {
            console.error('Database Error /registration:', e);
            return res.status(501).send('Database Error');
        });
    }).catch((e) => {
        return res.status(404).send('The requested Registration code was not found');
    });
});
