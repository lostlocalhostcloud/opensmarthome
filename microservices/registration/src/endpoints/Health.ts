import { Router } from 'express';
import { getConnection } from 'typeorm';
import { RedisSingleton } from '../types';

interface HealthStatus {
    name: string;
    status: string;
    error?: {
        code: number;
        message: string;
    }
}

export const health = Router();

health.get('/', async (req, res) => {
    let databaseHealth: HealthStatus;
    let redisHealth: HealthStatus;

    try {
        databaseHealth = await checkDatabase();
    } catch (error) {
        databaseHealth = error;
    }
    try {
        redisHealth = await checkRedis();
    } catch (error) {
        redisHealth = error;
    }

    if (!databaseHealth.error && !redisHealth.error) {
        return res.status(200).json({
            status: 'all systems up'
        });
    }
    return res.status(500).json({
        status: 'not all systems up',
        services: [databaseHealth, redisHealth]
    });
});

health.get('/database', (req, res) => {
    checkDatabase().then((databaseHealth) => {
        return res.status(200).json({
            status: databaseHealth.status
        });
    }).catch((databaseHealthError) => {
        return res.status(500).json({
            status: databaseHealthError.status,
            error: {
                code: databaseHealthError.error.code,
                message: databaseHealthError.error.message
            }
        });
    });
});

health.get('/redis', (req, res) => {
    checkRedis().then((redisHealth) => {
        return res.status(200).json({
            status: redisHealth.status
        });
    }).catch((redisHealthError) => {
        return res.status(500).json({
            status: redisHealthError.status,
            error: {
                code: redisHealthError.error.code,
                message: redisHealthError.error.message
            }
        });
    });
});

function checkDatabase(): Promise<HealthStatus> {
    return new Promise(async (resolve, reject) => {
        try {
            await getConnection().query('select 1');
            resolve({
                name: 'Database',
                status: 'database connection Ok'
            });
        } catch (error) {
            console.log(error);
            reject({
                name: 'Database',
                status: 'can\'t connect to database',
                error: {
                    code: error.code,
                    message: error.message
                }
            });
        }
    });
}

function checkRedis(): Promise<HealthStatus> {
    return new Promise<HealthStatus>(async (resolve, reject) => {
        RedisSingleton.getRedis().once('error', (error) => {
            reject({
                name: 'Redis',
                status: 'can\'t connect to redis',
                error: {
                    code: error.code,
                    message: error.message
                }
            });
        });
        try {
            if (await RedisSingleton.getRedis().ping() === 'PONG') resolve({
                name: 'Redis',
                status: 'redis connection Ok'
            });
        } catch (error) {
            reject({
                name: 'Redis',
                status: 'can\'t connect to redis',
                error: {
                    code: error.code,
                    message: error.message
                }
            });
        }
    });
}