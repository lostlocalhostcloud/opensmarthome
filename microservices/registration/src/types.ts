import { RedisClientType } from 'redis';
import { Database } from './Database';

const redis = require('redis');

class RedisSingleton {
    private redis: RedisClientType<any, any>;
    private static instance: RedisSingleton;

    constructor(redis: RedisClientType<any, any>) {
        this.redis = redis;
    }

    public static getRedis(): RedisClientType<any, any> {
        if (this.instance === undefined) {
            const redisUrl = process.env.REDIS_USERNAME === undefined || process.env.REDIS_PASSWORD === undefined
                ? 'redis://' + process.env.REDIS_HOST + ':' + process.env.REDIS_PORT
                : 'redis://' + process.env.REDIS_USERNAME + ':' + process.env.REDIS_PASSWORD + '@' + process.env.REDIS_HOST + ':' + process.env.REDIS_PORT;
            this.instance = new RedisSingleton(redis.createClient({
                url: redisUrl,
                socket: {
                    connectTimeout: 50000,
                },
            }).on('error', function(error) {
                console.error(error);
            }));
            this.instance.redis.connect().then(() => {
                return this.instance.redis;
            }).catch(error => console.error(error));
        } else {
            return this.instance.redis;
        }
    }
}

class DBConnectionHolder {
    private db: Database;
    private static instance: DBConnectionHolder;

    constructor(db: Database) {
        this.db = db;
    }

    public static setConn(db: Database): void {
        this.instance = new DBConnectionHolder(db);
    }

    public static getConn(): Database {
        // TODO: fail save
        return this.instance.db;
    }
}

export { RedisSingleton, DBConnectionHolder };