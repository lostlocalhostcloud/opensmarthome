import { RedisSingleton } from '../types';

const cookie = require('cookie');

function hasValidRegistrationSession() {
    return (req, res, next) => {
        const regSession = cookie.parse(req.headers.cookie || '').regSession;
        if (regSession) {
            RedisSingleton.getRedis().get(regSession).then(data => {
                if (data !== null) {
                    res.locals.regSessionData = data;
                    return next();
                } else {
                    const err = new Error('invalid registration session');
                    // eslint-disable-next-line
                    // @ts-ignore
                    err.status = 401;
                    return next(err);
                }
            }).catch(error => console.error(error));
        } else {
            const err = new Error('Not authorized! No cookie');
            // eslint-disable-next-line
            // @ts-ignore
            err.status = 403;
            return next(err);
        }
    };
}


export { hasValidRegistrationSession };