import { Repository } from 'typeorm';
import { User } from './entities/User';
import { RegistrationCode } from './entities/RegistrationCode';
import { UpdateResult } from 'typeorm/query-builder/result/UpdateResult';

interface Database {

    findActiveRegistrationCode(regCode: string): Promise<RegistrationCode>;

    isUsernameAndEmailUsed(username: string, emailAddress: string): Promise<boolean>;

    findUserByUsernameOrFail(username: string): Promise<User>;
    findUserByUsernameAndPassword(username: string, password: string): Promise<User>;

    createUser(username: string, email: string, password: string, baseSecret: string, firstName: string, lastName: string) : Promise<User>;
    deactivateRegistrationCode(regCode: string, userId: number): Promise<UpdateResult>;
}

class TypeOrm implements Database {

    constructor(
        private registrationCodeRepository: Repository<RegistrationCode>,
        private userRepository: Repository<User>
    ) {
    }

    findUserByUsernameOrFail(username: string): Promise<User> {
        return this.userRepository.findOneOrFail({ where: { username } });
    }

    isUsernameAndEmailUsed(username: string, emailAddress: string): Promise<boolean> {
        return new Promise(async (resolve, reject) => {
            try {
                const userWithUsedUsername = await this.userRepository.findOne({ where: { username } });
                const userWithUsedEmailAddress = await this.userRepository.findOne({ where: { emailAddress } });
                if (userWithUsedUsername === null && userWithUsedEmailAddress === null) resolve(false);
                resolve(true);
            } catch (error) {
                reject(error);
            }
        });
    }

    findUserByUsernameAndPassword(username: string, password: string): Promise<User> {
        return new Promise(async (resolve, reject) => {
            try {
                resolve(await this.userRepository.findOneOrFail({ where: { username: username, password: password } }));
            } catch (error) {
                reject(error);
            }
        });
    }

    findActiveRegistrationCode(regCode: string): Promise<RegistrationCode> {
        return new Promise(async (resolve, reject) => {
            try {
                const registrationCode = await this.registrationCodeRepository.findOneOrFail({ where: {
                        registrationCode: regCode,
                        used: false
                }});
                resolve(registrationCode);
            } catch (error) {
                reject(error);
            }
        });
    }

    createUser(username: string, email: string, password: string, baseSecret: string, firstName: string, lastName: string) : Promise<User> {
        const user = this.userRepository.create();
        user.username = username;
        user.firstName = firstName;
        user.lastName = lastName;
        user.emailAddress = email;
        user.baseSecret = baseSecret;
        user.password = password;
        return this.userRepository.save(user);
    }

    deactivateRegistrationCode(regCode: string, userId: number): Promise<UpdateResult> {
        return this.registrationCodeRepository.update({ registrationCode: regCode }, { used: true , userId});
    }

}

export { Database, TypeOrm };