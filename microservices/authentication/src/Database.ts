import { LessThan, Repository } from 'typeorm';
import { User } from './entities/User';
import { Session } from './entities/Session';

interface Database {

    deleteSession(sessionToken: string): Promise<boolean>;
    deleteSessionById(id: number): Promise<boolean>;
    deleteExpiredSessionsByUsername(username: string): Promise<number>;

    findSessionByTokenOrFail(token: string): Promise<Session>;
    findSessionsByUsername(username: string): Promise<Session[]>;

    findUserByUsernameOrFail(username: string): Promise<User>;
    findUserByUsernameAndPassword(username: string, password: string): Promise<User>;

    updateUser(username: string, sessionExpiration: number): Promise<boolean>;

    saveSession(sessionToken: string, username: string, createdAt: Date, expireAt: Date, preAuth?: boolean): Promise<boolean>;

    /**
     * @deprecated Use deleteSession and then saveSession instead.
     */
    updateSession(currentSessionToken: string, newSessionToken: string, username: string, expiration: number): Promise<boolean>;

}

class TypeOrm implements Database {

    constructor(
        private sessionRepository: Repository<Session>,
        private userRepository: Repository<User>
    ) {
    }

    deleteSession(sessionToken: string): Promise<boolean> {
        return new Promise<boolean>(async (resolve, reject) => {
            try {
                const session = await this.findSessionByTokenOrFail(sessionToken);
                const deleteResult = await this.sessionRepository.delete({ id: session.id });
                if (deleteResult.affected !== 1) throw new Error('Session ' + session.id + ' could not be deleted');
                resolve(true);
            } catch (error) {
                // It is not an error if the Session was not found, because the function is designed to delete the session.
                if (error.name === 'EntityNotFound') resolve(true);
                reject(error);
            }
        });
    }

    deleteSessionById(id: number): Promise<boolean> {
        return new Promise(async (resolve, reject) => {
            try {
                await this.sessionRepository.delete({ id });
                resolve(true);
            } catch (error) {
                reject(error);
            }
        });
    }

    deleteExpiredSessionsByUsername(username: string): Promise<number> {
        return new Promise(async (resolve, reject) => {
           try {
               const user = await this.findUserByUsernameOrFail(username);

               const deleteResult = await this.sessionRepository.delete({
                   user: user,
                   expiration: LessThan(new Date())
               });
               resolve(deleteResult.affected);
           } catch (error) {
               reject(error);
           }
        });
    }

    findSessionByTokenOrFail(token: string): Promise<Session> {
        return this.sessionRepository.findOneOrFail({ where: { token: token } });
    }

    findSessionsByUsername(username: string): Promise<Session[]> {
        return new Promise(async (resolve, reject) => {
            try {
                const user = await this.findUserByUsernameOrFail(username);
                const sessions = await this.sessionRepository.find({
                    where: { user: user },
                    order: { createdAt: 'DESC' }
                });
                resolve(sessions);
            } catch (error) {
                reject(error);
            }
        });
    }

    findUserByUsernameOrFail(username: string): Promise<User> {
        return this.userRepository.findOneByOrFail({ username });
    }

    findUserByUsernameAndPassword(username: string, password: string): Promise<User> {
        return new Promise(async (resolve, reject) => {
            try {
                resolve(await this.userRepository.findOneByOrFail({ username: username, password: password }));
            } catch (error) {
                reject(error);
            }
        });
    }

    updateUser(username: string, sessionExpiration: number): Promise<boolean> {
        return new Promise(async (resolve, reject) => {
            try {
                const user = await this.findUserByUsernameOrFail(username);
                if (user.sessionExpiration === sessionExpiration) resolve(false);
                user.sessionExpiration = sessionExpiration;
                const savedUser = await this.userRepository.save(user);
                if (savedUser.sessionExpiration !== sessionExpiration) throw new Error('User ' + user.id + ' could not be updated');
                resolve(true);
            } catch (error) {
                reject(error);
            }
        });
    }

    saveSession(sessionToken: string, username: string, createdAt: Date, expireAt: Date, preAuth?: boolean): Promise<boolean> {
        return new Promise(async (resolve, reject) => {
            try {
                const session = new Session();
                session.token = sessionToken;
                session.user = await this.findUserByUsernameOrFail(username);
                session.createdAt = createdAt;
                if (preAuth !== undefined) session.preAuth = preAuth;
                session.expiration = expireAt;
                await this.sessionRepository.save(session);
                resolve(true);
            } catch (error) {
                reject(error);
            }
        });
    }

    updateSession(currentSessionToken: string, newSessionToken: string, username: string, expiration: number): Promise<boolean> {
        return new Promise(async (resolve, reject) => {
            try {
                await this.deleteSession(currentSessionToken);
                // TODO: Fix, Currently has no usages
                // await this.saveSession(newSessionToken, username, new Date(), expiration);
                resolve(true);
            } catch (error) {
                reject(error);
            }
        });
    }

}

export { Database, TypeOrm };