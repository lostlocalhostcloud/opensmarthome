import { createConnection } from 'typeorm';
import { auth } from './endpoints/Auth';
import express from 'express';
import { user } from './endpoints/User';
import { DBConnectionHolder, RedisSingleton } from './types';
import { User } from './entities/User';
import { TypeOrm } from './Database';
import { Session } from './entities/Session';
import { health } from './endpoints/Health';
import { versionEndpoint } from './endpoints/VersionEndpoint';
import { configureSharedApiResources } from 'osh-microservice-commons';
import { VERSION } from './version';

require('dotenv').config();
const app = require('express')();
const http = require('http').Server(app);

RedisSingleton.getRedis();

app.use(express.json());

// Set Service Header
app.use(function (req, res, next) {
    res.setHeader('Service', process.env.HOSTNAME ? process.env.HOSTNAME : 'auth-service');
    next();
});

app.use('/health', health);
app.use('/version', versionEndpoint);

const port = process.env.PORT ? process.env.PORT : 3000;
http.listen(port, () => {
    return console.log(`server is listening on ${port}`);
});

createConnection().then(async connection => {

    const sessionRepository = connection.getRepository(Session);
    const userRepository = connection.getRepository(User);
    DBConnectionHolder.setConn(new TypeOrm(
        sessionRepository,
        userRepository
    ));
    configureSharedApiResources(sessionRepository, VERSION);


    const apiPrefix = '/api/v1';
    app.use(apiPrefix + '/auth', auth);
    app.use(apiPrefix + '/user', user);



}).catch(error => console.log(error));
