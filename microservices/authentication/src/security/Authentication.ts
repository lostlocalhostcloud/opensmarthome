import { User } from '../entities/User';
import { authenticator } from 'otplib';
import { DBConnectionHolder, RedisSingleton } from '../types';

const sha512 = require('js-sha512');

interface UserAuthDetails {
    credentialsCorrect: boolean;
    otpEnabled: boolean;
    sessionExpiration?: number;
}

async function verifyCredentialsAndGetUser(username: string, password: string): Promise<UserAuthDetails> {
    return DBConnectionHolder.getConn().findUserByUsernameAndPassword(username, sha512(password + process.env.PASSWORD_SALT)).then((user) => {
        if (user) {
            const sessionExpiration = user.sessionExpiration ? user.sessionExpiration : Number.parseInt(process.env.JWT_TOKEN_EXPIRATION);
            if (user.baseSecret) {
                return {
                    credentialsCorrect: true,
                    otpEnabled: true,
                    sessionExpiration: sessionExpiration
                };
            }
            return {
                credentialsCorrect: true,
                otpEnabled: false,
                sessionExpiration: sessionExpiration
            };
        }
    }).catch(() => {
        return {
            credentialsCorrect: false,
            otpEnabled: false
        };
    });
}

function verifyOTPToken(username: string, otpToken: string): Promise<boolean> {
    return DBConnectionHolder.getConn().findUserByUsernameOrFail(username).then((user) => {
        if (user) {
            return verifyOTP(otpToken, user);
        }
    }).catch(() => {
        return false;
    });
}

function hasUserOTPEnabled(username: string): Promise<boolean> {
    return DBConnectionHolder.getConn().findUserByUsernameOrFail(username).then((user) => {
        return !!user.baseSecret;
    });
}

function verifyOTP(otpToken: string, user: User): boolean {
    const redisDB = RedisSingleton.getRedis();
    redisDB.get(otpToken).then(data => {
        if (data !== null)
            return false;
    }).catch(error => {
        console.error(error);
        return false;
    });
    redisDB.set(otpToken, user.id.toString(), { EX: Number.parseInt(process.env.REDIS_OTP_TOKEN_EXPIRATION) });
    return authenticator.check(otpToken, user.baseSecret);
}

export {verifyCredentialsAndGetUser, hasUserOTPEnabled, verifyOTP, verifyOTPToken};