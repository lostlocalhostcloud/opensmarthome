import { JwtPayload } from 'jsonwebtoken';

const jwt = require('jsonwebtoken');

export interface JwtTokenAndPayload {
    token: string;
    payload: JwtPayload;
}

export function generateToken(username: string, expiration: number, preAuth?: boolean): JwtTokenAndPayload {
    const token = jwt.sign({ username: username }, preAuth ? process.env.JWT_PRE_AUTH_TOKEN_SECRET : process.env.JWT_TOKEN_SECRET, { expiresIn: expiration });
    return {
        token: token,
        payload: jwt.decode(token)
    };
}