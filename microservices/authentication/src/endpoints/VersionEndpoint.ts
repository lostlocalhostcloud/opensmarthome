import { Router } from 'express';
import { VERSION } from '../version';

export const versionEndpoint = Router();

versionEndpoint.get('/', async (req, res) => {
    return res.status(200).send(VERSION);
});
