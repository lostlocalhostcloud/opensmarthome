import { Router } from 'express';
import { DBConnectionHolder } from '../types';
import { isAuthorized, isPreAuthorized } from 'osh-microservice-commons';

export const user = Router();

interface Session {
    id: number,
    createdAt: string,
    expireAt: string,
    current: boolean
}

user.get('/info', isAuthorized(), (req, res) => {
    return res.status(200).json({
        'username': res.locals.decodedJwt.username,
        'loginExpiration': res.locals.decodedJwt.exp
    });
});

user.get('/pre-authorized', isPreAuthorized(), (req, res) => {
    return res.status(200).send('OK');
});

user.get('/sessions', isAuthorized(), async (req, res) => {
    const username = res.locals.decodedJwt.username;

    try {
        await DBConnectionHolder.getConn().deleteExpiredSessionsByUsername(username);
    } catch (error) {
        console.error(`Could not delete expired Sessions of User ${username}`);
        console.error(error);
    }

    const sessions: Array<Session> = [];
    try {
        (await DBConnectionHolder.getConn().findSessionsByUsername(res.locals.decodedJwt.username)).forEach((session) => {
            sessions.push({
                id: session.id,
                createdAt: formatDateShort(session.createdAt),
                expireAt: new Date(session.expiration).toLocaleString(process.env.LOCALE, {
                    weekday: 'short',
                    year: 'numeric',
                    month: 'long',
                    day: 'numeric',
                    hour: '2-digit',
                    minute: '2-digit',
                    timeZone: process.env.TIMEZONE
                }),
                current: session.token === res.locals.authorizationToken
            });
        });
        return res.status(200).json(sessions);
    } catch (error) {
        console.error(error);
        return res.status(404).send('The requested User was not found');
    }
});

user.post('/delete-session', isAuthorized(), async (req, res) => {
    let sessionId = req.body.sessionId;
    if (!sessionId) return res.status(500).send('No sessionId passed');
    sessionId = Number(req.body.sessionId);
    if (Number.isNaN(sessionId)) return res.status(406).send('The given sessionId is not a Number');
    try {
        for (const session of (await DBConnectionHolder.getConn().findSessionsByUsername(res.locals.decodedJwt.username))) {
            if (session.id === sessionId) await DBConnectionHolder.getConn().deleteSessionById(session.id);
        }
        return res.status(200).send('OK');
    } catch (error) {
        return res.status(404).send('The requested User was not found');
    }
});

interface Expiration {
    name: string;
    days: number;
}

const expirations: Expiration[] = [
    {
        name: '1 day',
        days: 1 // 86400
    },
    {
        name: '1 week',
        days: 7 // 604800
    },
    {
        name: '1 month', // 2592000
        days: 30
    },
    {
        name: '3 months',
        days: 90 // 7776000
    },
    {
        name: '6 months',
        days: 180 // 15552000
    }
];

user.get('/session-expirations', isAuthorized(), (req, res) => {
    return res.status(200).json(expirations);
});

user.get('/user-settings', isAuthorized(), async (req, res) => {
    const user = await DBConnectionHolder.getConn().findUserByUsernameOrFail(res.locals.decodedJwt.username);

    if (!user) return res.status(500).send('Internal Server Error');

    try {
        return res.status(200).json({
            'username': user.username,
            'emailAddress': user.emailAddress,
            'sessionExpiration': expirations.filter(expiration => expiration.days === (user.sessionExpiration ? user.sessionExpiration : Number.parseInt(process.env.JWT_TOKEN_EXPIRATION)) / 86400)[0].name,
            'secondFactorEnabled': !!user.baseSecret
        });
    } catch (error) {
        console.error(error);
        return res.status(500).send('Internal Server Error');
    }
});

user.post('/user-settings', isAuthorized(), async (req, res) => {
    let sessionExpiration = req.body.sessionExpiration;
    if (!sessionExpiration) return res.status(500).send('No sessionExpiration passed');
    try {
        sessionExpiration = expirations.filter(expiration => expiration.name === sessionExpiration)[0];
        if (sessionExpiration === undefined) return res.status(406).send('The given sessionExpiration is not supported');
        sessionExpiration = sessionExpiration.days * 86400;
        const userUpdated = await DBConnectionHolder.getConn().updateUser(res.locals.decodedJwt.username, sessionExpiration);
        if (userUpdated) return res.status(200).send('OK');
        return res.status(200).send('No changes');
    } catch (error) {
        console.error(error);
        return res.status(500).send('Internal Server Error');
    }
});

function formatDateShort(date: Date): string {
    return new Date(date).toLocaleString(process.env.LOCALE, {
        dateStyle: 'short',
        timeStyle: 'short',
        timeZone: process.env.TIMEZONE
    });
}
