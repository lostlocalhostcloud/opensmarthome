import { Router } from 'express';
import { generateToken, JwtTokenAndPayload } from '../security/jwt/JwtManager';
import {
    hasUserOTPEnabled,
    verifyCredentialsAndGetUser,
    verifyOTP,
    verifyOTPToken
} from '../security/Authentication';
import { DBConnectionHolder } from '../types';
import { isAuthorized, isPreAuthorized } from 'osh-microservice-commons';

const cookie = require('cookie');

export const auth = Router();

async function setAuthCookieAndSaveSession<ResBody, LocalsObj>(preAuth: boolean, res: any, authorizationToken: JwtTokenAndPayload, expiration: number, username: string): Promise<void> {
    res.setHeader('Set-Cookie', cookie.serialize(preAuth ? 'pre_authorization' : 'authorization', authorizationToken.token, {
        httpOnly: true,
        path: '/',
        maxAge: preAuth ? process.env.JWT_PRE_AUTH_TOKEN_EXPIRATION : expiration
    }));
    await DBConnectionHolder.getConn().saveSession(authorizationToken.token, username, new Date(authorizationToken.payload.iat * 1000), new Date(authorizationToken.payload.exp * 1000), preAuth ? preAuth : undefined);
}

async function authorize(username: string, res: any, expiration: number, currentAuthorizationToken?: string): Promise<void> {
    await setAuthCookieAndSaveSession(false, res, generateToken(username, expiration), expiration, username);

    if (currentAuthorizationToken) await DBConnectionHolder.getConn().deleteSession(currentAuthorizationToken);

    const preAuthorizationToken = res.locals.preAuthorizationToken;
    if (preAuthorizationToken) {
        res.clearCookie('pre_authorization');
        await DBConnectionHolder.getConn().deleteSession(preAuthorizationToken);
    }
}

auth.post('/login', async (req, res) => {
    const username = req.body.username;
    const password = req.body.password;
    const otpToken = req.body.otpToken;
    if (!username || !password) return res.status(500).send('No credentials passed');

    const userAuthDetails = await verifyCredentialsAndGetUser(username, password);
    if (userAuthDetails.credentialsCorrect) {
        if (userAuthDetails.otpEnabled) {
            if (otpToken) {
                if (await verifyOTPToken(username, otpToken)) {
                    try {
                        await authorize(username, res, userAuthDetails.sessionExpiration);
                        return res.status(200).send('OK');
                    } catch (error) {
                        console.error(error);
                        return res.status(500).send('Internal Server Error');
                    }
                }
            } else {
                const authorizationToken = generateToken(username, Number.parseInt(process.env.JWT_PRE_AUTH_TOKEN_EXPIRATION), true);
                try {
                    await setAuthCookieAndSaveSession(true, res, authorizationToken, Number.parseInt(process.env.JWT_PRE_AUTH_TOKEN_EXPIRATION), username);
                    return res.status(200).send('2FA-required');
                } catch (error) {
                    console.error(error);
                    return res.status(500).send('Internal Server Error');
                }
            }
        } else {
            try {
                await authorize(username, res, userAuthDetails.sessionExpiration);
                return res.status(200).send('OK');
            } catch (error) {
                return res.status(500).send('Internal Server Error');
            }
        }
    }

    return res.status(401).send('Unauthorized');
});

auth.get('/logout', isAuthorized(), async (req, res) => {
    try {
        res.clearCookie('authorization');
        await DBConnectionHolder.getConn().deleteSession(res.locals.authorizationToken);
        return res.status(200).send('OK');
    } catch (error) {
        return res.status(500).send('Internal Server Error');
    }
});

auth.post('/otp-verify', isPreAuthorized(), async (req, res) => {
    const otpToken = req.body.otpToken;

    if (!otpToken) return res.status(500).send('No OTP Token passed');

    const username = res.locals.decodedJwt.username;

    if (!username) return res.status(500).send('JWT not valid');

    const userSessionExpiration = (await DBConnectionHolder.getConn().findUserByUsernameOrFail(username)).sessionExpiration;
    const sessionExpiration = userSessionExpiration ? userSessionExpiration : Number.parseInt(process.env.JWT_TOKEN_EXPIRATION);

    if (sessionExpiration && await verifyOTPToken(username, otpToken)) {
        try {
            await authorize(username, res, sessionExpiration);
            return res.status(200).send('OK');
        } catch (error) {
            return res.status(500).send('Internal Server Error');
        }
    }

    return res.status(401).send('OTP mismatch');
});

auth.post('/renew', isAuthorized(), async (req, res) => {
    const otpToken = req.body.otpToken;
    if (!otpToken) return res.status(500).send('OTP required');

    const username = res.locals.decodedJwt.username;
    const currentAuthorizationToken = res.locals.authorizationToken;
    if (!await hasUserOTPEnabled(username)) return res.status(403).send('OTP need to be enabled');

    const user = await DBConnectionHolder.getConn().findUserByUsernameOrFail(username);
    if (verifyOTP(otpToken, user)) {
        try {
            const sessionExpiration = user.sessionExpiration ? user.sessionExpiration : Number.parseInt(process.env.JWT_TOKEN_EXPIRATION);
            await authorize(username, res, sessionExpiration, currentAuthorizationToken);
            return res.status(200).send('OK');
        } catch (error) {
            console.error(error);
            return res.status(500).send('Internal Server Error');
        }
    } else {
        return res.status(401).send('OTP mismatch');
    }
});