import { createConnection } from 'typeorm';
import express from 'express';
import { DBConnectionHolder } from './types';
import { User } from './entities/User';
import { TypeOrm } from './Database';
import { Session } from './entities/Session';
import { health } from './endpoints/Health';
import { versionEndpoint } from './endpoints/VersionEndpoint';
import { file } from './endpoints/File';
import { FileCollection } from './entities/FileCollection';
import { FileEntity } from './entities/FileEntity';
import { UserFileCollection } from './entities/UserFileCollection';
import nocache from 'nocache';
import { FileViewed } from './entities/FileViewed';
import { configureSharedApiResources } from 'osh-microservice-commons';
import { VERSION } from './version';

require('dotenv').config();
const app = require('express')();
const http = require('http').Server(app);

app.use(express.json());
app.use(nocache());

// Set Service Header
app.use(function (req, res, next) {
    res.setHeader('Service', process.env.HOSTNAME ? process.env.HOSTNAME : 'file-management-service');
    next();
});

app.use('/health', health);
app.use('/version', versionEndpoint);

const port = process.env.PORT ? process.env.PORT : 3000;
http.listen(port, () => {
    return console.log(`server is listening on ${port}`);
});

createConnection().then(async connection => {

    const fileCollectionRepository = connection.getRepository(FileCollection);
    const fileRepository = connection.getRepository(FileEntity);
    const fileViewedRepository = connection.getRepository(FileViewed);
    const sessionRepository = connection.getRepository(Session);
    const userRepository = connection.getRepository(User);
    const userFileCollectionRepository = connection.getRepository(UserFileCollection);
    DBConnectionHolder.setConn(new TypeOrm(
        fileCollectionRepository,
        fileRepository,
        fileViewedRepository,
        sessionRepository,
        userRepository,
        userFileCollectionRepository
    ));
    configureSharedApiResources(sessionRepository, VERSION);

    const apiPrefix = '/api/v1';
    app.use(apiPrefix + '/file', file);

}).catch(error => console.log(error));
