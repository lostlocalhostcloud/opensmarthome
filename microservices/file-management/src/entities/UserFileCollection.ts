import { Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { User } from './User';
import { FileCollection } from './FileCollection';

@Entity()
export class UserFileCollection {

    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(() => FileCollection, { nullable: false, eager: true })
    @JoinColumn()
    collection: FileCollection;

    @ManyToOne(() => User, { nullable: false, eager: true })
    @JoinColumn()
    user: User;

}
