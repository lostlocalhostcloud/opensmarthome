import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { User } from './User';

@Entity()
export class FileCollection {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({ nullable: false })
    name: string;

    @ManyToOne(() => User, { nullable: true, eager: true })
    @JoinColumn({ name: 'createdByUserId' })
    user: User;

}
