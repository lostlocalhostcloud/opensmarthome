import { Column, Entity, JoinColumn, OneToOne, PrimaryColumn, PrimaryGeneratedColumn } from 'typeorm';
import { FileCollection } from './FileCollection';

@Entity({ name: 'file' })
export class FileEntity {

    @PrimaryColumn('uuid')
    id: string;

    @Column({ nullable: false, unique: true })
    name: string;

    @OneToOne(() => FileCollection, { nullable: true, eager: true })
    @JoinColumn({ name: 'collectionId' })
    collection: FileCollection;

    @Column({ nullable: false })
    contentType: string;

    @Column({ type: 'timestamptz' })
    timestamp: Date;

    @Column({ nullable: true })
    resolution: string;

}
