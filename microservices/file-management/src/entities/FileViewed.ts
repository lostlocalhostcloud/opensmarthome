import { Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { User } from './User';
import { FileEntity } from './FileEntity';

@Entity()
export class FileViewed {

    @PrimaryGeneratedColumn()
    id: number;

    @OneToOne(() => FileEntity, { nullable: false, eager: true })
    @JoinColumn()
    file: FileEntity;

    @OneToOne(() => User, { nullable: false, eager: true })
    @JoinColumn()
    user: User;

}
