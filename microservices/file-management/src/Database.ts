import { EntityNotFoundError, Repository } from 'typeorm';
import { User } from './entities/User';
import { Session } from './entities/Session';
import { FileCollection } from './entities/FileCollection';
import { FileEntity } from './entities/FileEntity';
import { UserFileCollection } from './entities/UserFileCollection';
import { FileViewed } from './entities/FileViewed';
import { FileCollectionToResponse, FileEntityWithViewedMarking } from './types';

interface Database {

    findUserByUsernameOrFail(username: string): Promise<User>;

    addUserToCollection(collectionId: number, username: string): Promise<void>;
    deleteUsersFromCollections(collectionId: number): Promise<void>;
    findFileCollectionByIdOrFail(collectionId: number): Promise<FileCollection>;
    findFileCollectionsByUserId(userId: number): Promise<FileCollectionToResponse[]>;
    findFiles(): Promise<FileEntity[]>;
    findFilesByCollectionIdAndUserId(collectionId: number, userId: number): Promise<FileEntityWithViewedMarking[]>;
    findFileByIdAndUserId(fileId: string, userId: number): Promise<FileEntity>;
    existsFileByNameOrFail(fileName: string): Promise<boolean>;
    existsFileCollectionByIdAndUserIdOrFail(collectionId: number, userId: number): Promise<boolean>;
    existsFileViewedByFileIdAndUserId(fileId: string, userId: number): Promise<boolean>;
    saveCollection(name: string, user: User): Promise<number>;
    saveFileViewedByFileIdAndUserId(file: FileEntity, user: User): Promise<void>;
    saveFile(file: FileEntity): Promise<void>;

}

class TypeOrm implements Database {

    constructor(
        private fileCollectionRepository: Repository<FileCollection>,
        private fileRepository: Repository<FileEntity>,
        private fileViewedRepository: Repository<FileViewed>,
        private sessionRepository: Repository<Session>,
        private userRepository: Repository<User>,
        private userFileCollectionRepository: Repository<UserFileCollection>
    ) {
    }

    findFileCollectionByIdOrFail(collectionId: number): Promise<FileCollection> {
        return this.fileCollectionRepository.findOneByOrFail({ id: collectionId });
    }

    addUserToCollection(collectionId: number, username: string): Promise<void> {
        return new Promise(async (resolve, reject) => {
           try {
               const collection = await this.findFileCollectionByIdOrFail(collectionId);
               const user = await this.findUserByUsernameOrFail(username);
               const userFileCollection = new UserFileCollection();
               userFileCollection.collection = collection;
               userFileCollection.user = user;
               await this.userFileCollectionRepository.save(userFileCollection);
               resolve();
           } catch (error) {
               if (error instanceof EntityNotFoundError) {
                   if (error.message.includes('FileCollection')) {
                       reject(`No collection with the ID ${collectionId} found`);
                   } else if (error.message.includes('User')) {
                       reject(`No user with the username ${username} found`);
                   }
               }
               reject(error);
           }
        });
    }

    deleteUsersFromCollections(collectionId: number): Promise<void> {
        return new Promise(async (resolve, reject) => {
            try {
                const collection = await this.findFileCollectionByIdOrFail(collectionId);

                await this.userFileCollectionRepository.delete({ collection: { id: collection.id } });
                resolve();
            } catch (error) {
                if (error instanceof EntityNotFoundError) {
                    if (error.message.includes('FileCollection')) {
                        reject(`No collection with the ID ${collectionId} found`);
                    }
                }
                reject(error);
            }
        });
    }

    findFilesByCollectionIdAndUserId(collectionId: number, userId: number): Promise<FileEntityWithViewedMarking[]> {
        return new Promise(async (resolve, reject) => {
            const response: FileEntityWithViewedMarking[] = [];
            const files = await this.fileRepository.find({
                where: {
                    collection: {
                        id: collectionId
                    }
                },
                order: {
                    name: 'ASC'
                }
            });
            for (const file of files) {
                response.push({
                    fileEntity: file,
                    viewed: await this.existsFileViewedByFileIdAndUserId(file.id, userId)
                });
            }
            resolve(response);
        });
    }

    findUserByUsernameOrFail(username: string): Promise<User> {
        return this.userRepository.findOneByOrFail({ username });
    }

    findFileCollectionsByUserId(userId: number): Promise<FileCollectionToResponse[]> {
        return new Promise(async (resolve, reject) => {
            const sharedCollections = await this.userFileCollectionRepository.find({
                where: {
                    user: {
                        id: userId
                    }
                },
                order: {
                    id: 'DESC'
                }
            });
            const fileCollectionsToResponse: FileCollectionToResponse[] = [];
            for (const sharedCollection of sharedCollections) {
                const sharedWith = (await this.userFileCollectionRepository
                    .findBy({
                        collection: {
                            id: sharedCollection.collection.id
                        }
                    }))
                    .map(entity => entity.user.username);
                fileCollectionsToResponse.push({
                    collection: await this.fileCollectionRepository.findOneBy({id: sharedCollection.collection.id}),
                    sharedWith: sharedWith
                });
            }
            resolve(fileCollectionsToResponse);
        });
    }

    findFiles(): Promise<FileEntity[]> {
        return this.fileRepository.find({
            select: ['name']
        });
    }

    findFileByIdAndUserId(fileId: string, userId: number): Promise<FileEntity> {
        return new Promise(async (resolve, reject) => {
            const file = await this.fileRepository.findOneBy({
                id: fileId
            });
            if (!file) {
                reject();
                return;
            }
            if (file.collection) {
                const permission = await this.userFileCollectionRepository.findOneBy({
                    collection: {
                        id: file.collection.id
                    },
                    user: {
                        id: userId
                    }
                });
                if (!permission) {
                    reject();
                    return;
                }
            } else {
                const user = await this.userRepository.findOneBy({
                    id: userId
                });
                if (!user.noCollectionsFoundImageId || user.noCollectionsFoundImageId !== fileId) {
                    reject();
                    return;
                }
            }
            resolve(file);
        });
    }

    existsFileByNameOrFail(fileName: string): Promise<boolean> {
        return this.fileRepository.existsBy({ name: fileName });
    }

    existsFileCollectionByIdAndUserIdOrFail(collectionId: number, userId: number): Promise<boolean> {
        return this.userFileCollectionRepository.existsBy({
            collection: {
                id: collectionId
            },
            user: {
                id: userId
            }
        });
    }

    existsFileViewedByFileIdAndUserId(fileId: string, userId: number): Promise<boolean> {
        return Promise.resolve(this.fileViewedRepository.existsBy({
            file: {
                id: fileId
            },
            user: {
                id: userId
            }
        }));
    }

    saveCollection(name: string, user: User): Promise<number> {
        return new Promise(async (resolve, reject) => {
            try {
                if (await this.fileCollectionRepository.existsBy({ name })) {
                    reject(`File collection with name ${name} already exists`);
                    return;
                }
                const collection = new FileCollection();
                collection.name = name;
                collection.user = user;
                const fileCollection = await this.fileCollectionRepository.save(collection);
                resolve(fileCollection.id);
            } catch (error) {
                reject(error);
            }
        });
    }

    saveFileViewedByFileIdAndUserId(file: FileEntity, user: User): Promise<void> {
        return new Promise(async (resolve, reject) => {
            try {
                if (await this.existsFileViewedByFileIdAndUserId(file.id, user.id)) {
                    resolve();
                }
                const fileViewed = new FileViewed();
                fileViewed.file = file;
                fileViewed.user = user;
                await this.fileViewedRepository.save(fileViewed);
                resolve();
            } catch (error) {
                reject(error);
            }
        });
    }

    saveFile(file: FileEntity): Promise<void> {
        return new Promise(async (resolve, reject) => {
            try {
                await this.fileRepository.save(file);
                resolve();
            } catch (error) {
                reject(error);
            }
        });
    }

}

export { Database, TypeOrm };