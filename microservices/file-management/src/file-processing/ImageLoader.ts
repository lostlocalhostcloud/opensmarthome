import fs from 'node:fs';
import sharp from 'sharp';
import { FileEntity } from '../entities/FileEntity';
import { v4 as uuidv4 } from 'uuid';
import { DBConnectionHolder } from '../types';
import { EntityNotFoundError, QueryFailedError } from 'typeorm';

const exif = require('exif-reader');

const FILES_PATH = 'files';

export async function loadImage(fileName: string, collectionId: number): Promise<void> {
    unifyFileNames();
    const inputPath = `${FILES_PATH}/${fileName}`;
    if (!fs.existsSync(inputPath)) {
        throw new Error(`File ${fileName} does not exist`);
    } else if (!fileName.endsWith('.jpg')) {
        throw new Error(`File ${fileName} is not a jpg file`);
    }
    if (await DBConnectionHolder.getConn().existsFileByNameOrFail(fileName)) {
        throw new Error(`File with name ${fileName} exists`);
    }
    await createPreviewImage(fileName);

    let collection = null;
    try {
        collection = await DBConnectionHolder.getConn().findFileCollectionByIdOrFail(collectionId);
    } catch (error) {
        if (error instanceof EntityNotFoundError) {
            throw new Error(`Collection with id ${collectionId} does not exist`);
        }
        throw error;
    }

    const metadata = await sharp(inputPath).metadata();
    const file = new FileEntity();
    file.id = uuidv4();
    file.name = fileName;
    file.collection = collection;
    file.contentType = 'image/jpeg';
    let timestamp = exif(metadata.exif).Photo.DateTimeOriginal;
    if (!timestamp) {
        const stat = fs.statSync(inputPath);
        if (stat.birthtime) timestamp = stat.birthtime;
    }
    file.timestamp = new Date(timestamp.getTime() + timestamp.getTimezoneOffset() * 60000);
    file.resolution = `${metadata.width}x${metadata.height}`;
    try {
        await DBConnectionHolder.getConn().saveFile(file);
    } catch (error) {
        if (error instanceof QueryFailedError) {
            console.error(error.driverError);
            return;
        }
        console.error(error);
    }
}

export function unifyFileNames(): void {
    let counter = 0;
    fs.readdirSync(FILES_PATH).forEach(fileName => {
        if (fileName.endsWith('.JPG')) {
            const shortFileName = fileName.replace('.JPG', '');
            fs.renameSync(`${FILES_PATH}/${fileName}`, `${FILES_PATH}/${shortFileName}.jpg`);
            counter++;
        }
    });
    if (counter > 0) console.info(`Unified ${counter} file names`);
}

export async function createPreviewImage(fileName: string, maxWidth?: number, maxHeight?: number): Promise<void> {
    const inputPath = `${FILES_PATH}/${fileName}`;
    if (!fs.existsSync(inputPath)) {
        throw new Error(`File ${fileName} does not exist`);
    } else if (!fileName.endsWith('.jpg')) {
        throw new Error(`File ${fileName} is not a jpg file`);
    }

    const inputPathParts = inputPath.split('.jpg');
    const outputPath = inputPathParts[0] + '_Preview.jpg';
    if (fs.existsSync(outputPath)) fs.rmSync(outputPath);

    maxWidth = maxWidth ?? 854;
    maxHeight = maxHeight ?? 480;

    try {
        const metadata = await sharp(inputPath).metadata();
        let width = maxWidth;
        let height = maxHeight;
        if (metadata.width && metadata.height && metadata.height > metadata.width) {
            width = maxHeight;
            height = maxWidth;
        }
        await sharp(inputPath)
            .jpeg({ mozjpeg: true })
            .resize({
                width,
                height,
                fit: 'inside',
                withoutEnlargement: true
            }).toFile(outputPath);
    } catch (error) {
        console.error(error);
    }
}
