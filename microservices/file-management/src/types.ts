import { Database } from './Database';
import { FileEntity } from './entities/FileEntity';
import { FileCollection } from './entities/FileCollection';

class DBConnectionHolder {
    private db: Database;
    private static instance: DBConnectionHolder;

    constructor(db: Database) {
        this.db = db;
    }

    public static setConn(db: Database): void {
        this.instance = new DBConnectionHolder(db);
    }

    public static getConn(): Database {
        // TODO: fail save
        return this.instance.db;
    }
}

class FileCollectionResponse {
    private readonly id: number;
    private readonly name: string;
    private readonly creator: string;
    private readonly sharedWith: string[];

    constructor(id: number, name: string, creator: string, sharedWith: string[]) {
        this.id = id;
        this.name = name;
        this.creator = creator;
        this.sharedWith = sharedWith;
    }
}

class FileResponse {
    private readonly id: string;
    private readonly name: string;
    private readonly timestamp: string;
    private readonly contentType: string;
    private readonly resolution: string;
    private readonly viewed: boolean;

    constructor(id: string, name: string, timestamp: string, contentType: string, resolution: string, viewed: boolean) {
        this.id = id;
        this.name = name;
        this.timestamp = timestamp;
        this.contentType = contentType;
        this.resolution = resolution;
        this.viewed = viewed;
    }
}

interface FileEntityWithViewedMarking {
    fileEntity: FileEntity,
    viewed: boolean
}

interface FileCollectionToResponse {
    collection: FileCollection;
    sharedWith: string[];
}

export { DBConnectionHolder, FileCollectionResponse, FileResponse, FileEntityWithViewedMarking, FileCollectionToResponse };