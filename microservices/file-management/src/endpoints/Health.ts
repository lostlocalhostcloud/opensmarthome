import { Router } from 'express';
import { getConnection } from 'typeorm';

interface HealthStatus {
    name: string;
    status: string;
    error?: {
        code: number;
        message: string;
    }
}

export const health = Router();

health.get('/', async (req, res) => {
    let databaseHealth: HealthStatus;

    try {
        databaseHealth = await checkDatabase();
    } catch (error) {
        databaseHealth = error;
    }

    if (!databaseHealth.error) {
        return res.status(200).json({
            status: 'all systems up'
        });
    }
    return res.status(500).json({
        status: 'not all systems up',
        services: [databaseHealth]
    });
});

health.get('/database', (req, res) => {
    checkDatabase().then((databaseHealth) => {
        return res.status(200).json({
            status: databaseHealth.status
        });
    }).catch((databaseHealthError) => {
        return res.status(500).json({
            status: databaseHealthError.status,
            error: {
                code: databaseHealthError.error.code,
                message: databaseHealthError.error.message
            }
        });
    });
});

function checkDatabase(): Promise<HealthStatus> {
    return new Promise(async (resolve, reject) => {
        try {
            await getConnection().query('select 1');
            resolve({
                name: 'Database',
                status: 'database connection Ok'
            });
        } catch (error) {
            console.log(error);
            reject({
                name: 'Database',
                status: 'can\'t connect to database',
                error: {
                    code: error.code,
                    message: error.message
                }
            });
        }
    });
}
