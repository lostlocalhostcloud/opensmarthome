import { Router } from 'express';
import { isAuthorized } from 'osh-microservice-commons';
import { DBConnectionHolder, FileCollectionResponse, FileResponse } from '../types';
import path from 'node:path';
import fs from 'node:fs';
import { validate as uuidValidate } from 'uuid';
import { createPreviewImage, loadImage } from '../file-processing/ImageLoader';

export const file = Router();

file.get('/:file_id/full', isAuthorized(), async (req, res) => {
    await fetchFile(req, res, false, false);
});

file.get('/:file_id/preview', isAuthorized(), async (req, res) => {
    await fetchFile(req, res, true, false);
});

file.get('/:file_id/download', isAuthorized(), async (req, res) => {
    await fetchFile(req, res, false, true);
});

file.get('/collections', isAuthorized(), async (req, res) => {
    try {
        const user = await DBConnectionHolder.getConn().findUserByUsernameOrFail(res.locals.decodedJwt.username);
        const collections = await DBConnectionHolder.getConn().findFileCollectionsByUserId(user.id);

        const collectionsToResponse: FileCollectionResponse[] = [];
        for (const collection of collections) {
            let sharedWith = collection.sharedWith;
            if (user.id === collection.collection.user.id) {
                sharedWith = collection.sharedWith.filter(username => username !== collection.collection.user.username);
            }
            collectionsToResponse.push(new FileCollectionResponse(
                collection.collection.id,
                collection.collection.name,
                collection.collection.user.username,
                sharedWith
            ));
        }
        const response = {
            collections: collectionsToResponse,
            noCollectionsFoundImageId: user.noCollectionsFoundImageId
        };
        return res.status(200).json(response);
    } catch (error) {
        console.error(error);
    }
});

file.get('/collections/:collection_id/files', isAuthorized(), async (req, res) => {
    const collectionIdParam = req.params.collection_id;

    try {
        const collectionId = Number(collectionIdParam);

        const user = await DBConnectionHolder.getConn().findUserByUsernameOrFail(res.locals.decodedJwt.username);
        const hasAccess = await DBConnectionHolder.getConn().existsFileCollectionByIdAndUserIdOrFail(collectionId, user.id);
        if (!hasAccess) return res.status(403).send('Forbidden');

        const files = await DBConnectionHolder.getConn().findFilesByCollectionIdAndUserId(collectionId, user.id);
        const mappedFiles = files.map(file => new FileResponse(
                file.fileEntity.id,
                file.fileEntity.name,
                formatDateShort(file.fileEntity.timestamp),
                file.fileEntity.contentType,
                file.fileEntity.resolution,
                file.viewed
        ));

        return res.status(200).json(mappedFiles);
    } catch (error) {
        console.error(error);
        return res.status(500).send('Internal Server Error');
    }
});

file.post('/regenerate-previews', isAuthorized(), async (req, res) => {
    const files = await DBConnectionHolder.getConn().findFiles();
    let processed = 0;
    let errors = 0;
    let width = req.body.width;
    let height = req.body.height;
    if (width && height) {
        if (typeof width !== 'number' || typeof height !== 'number') {
            return res.status(406).send('Invalid data');
        }
        width = Number(width);
        height = Number(height);
    }
    for (const file of files) {
        try {
            if (width && height) await createPreviewImage(file.name, width, height); else await createPreviewImage(file.name);
            processed++;
        } catch (error) {
            console.error(error.message);
            errors++;
        }
    }
    return res.status(200).send(`Processed ${processed} previews with ${errors} errors`);
});

file.post('/collection', isAuthorized(), async (req, res) => {
    const collectionName = req.body.collectionName;
    if (!collectionName) {
        return res.status(400).send('Missing collectionName');
    }

    const user = await DBConnectionHolder.getConn().findUserByUsernameOrFail(res.locals.decodedJwt.username);
    try {
        const collectionId = await DBConnectionHolder.getConn().saveCollection(collectionName, user);
        await DBConnectionHolder.getConn().addUserToCollection(collectionId, user.username);
        return res.status(201).send('OK');
    } catch (error) {
        if (typeof error === 'string' && error === `File collection with name ${collectionName} already exists`) {
            return res.status(406).send(error);
        }
        console.error(error);
        return res.status(500).send('Internal Server Error');
    }
});

file.post('/file', isAuthorized(), async (req, res) => {
    const fileName = req.body.fileName;
    const collectionId = req.body.collectionId;
    if (!fileName) {
        return res.status(400).send('Missing fileName');
    } else if (!collectionId) {
        return res.status(400).send('Missing collectionId');
    }

    if (typeof fileName !== 'string') {
        return res.status(406).send('Invalid file name');
    }
    const fileNameParts = fileName.split('.');
    if (fileNameParts.length === 1) {
        return res.status(406).send('Invalid file name');
    }
    if (typeof collectionId !== 'number') {
        return res.status(406).send('Invalid collection id');
    }
    if (fileNameParts[1].toLowerCase() === 'jpg') {
        try {
            await loadImage(fileName, collectionId);
        } catch (error) {
            if (error.message && (error.message === `File with name ${fileName} exists` || error.message === `Collection with id ${collectionId} does not exist`)) {
                return res.status(406).send(error.message);
            }
            console.error(error);
            return res.status(500).send('Internal Server Error');
        }
    }

    return res.status(201).send('OK');
});

file.put('/collection/:collection_id/users', isAuthorized(), async (req, res) => {
    const collectionId = Number(req.params.collection_id);
    const users: string[] = req.body.users;
    if (!collectionId || isNaN(Number(collectionId))) {
        return res.status(400).send('Missing or invalid collectionId');
    }

    try {
        await DBConnectionHolder.getConn().deleteUsersFromCollections(collectionId);
    } catch (error) {
        if (typeof error === 'string') return res.status(406).send(error);
        console.error(error);
        return res.status(500).send('Internal Server Error');
    }

    for (const username of users) {
        try {
            await DBConnectionHolder.getConn().addUserToCollection(collectionId, username);
        } catch (error) {
            if (typeof error === 'string') return res.status(406).send(error);
            console.error(error);
            return res.status(500).send('Internal Server Error');
        }
    }
    return res.status(200).send('OK');
});

async function fetchFile(req: any, res: any, preview: boolean, download: boolean): Promise<void> {
    const fileId = req.params.file_id;
    if (!fileId || !uuidValidate(fileId)) return res.status(400).send('Invalid File ID');

    try {
        const user = await DBConnectionHolder.getConn().findUserByUsernameOrFail(res.locals.decodedJwt.username);
        try {
            const file = await DBConnectionHolder.getConn().findFileByIdAndUserId(fileId, user.id);

            if (!preview) {
                try {
                    await DBConnectionHolder.getConn().saveFileViewedByFileIdAndUserId(file, user);
                } catch (error) {
                    console.error(error);
                }
            }

            let fileName = file.name;
            if (preview) {
                const splitFileName = file.name.split('.jpg');
                fileName = splitFileName[0] + '_Preview.jpg';
            }
            if (!fs.existsSync(path.join('files/' + fileName))) {
                return res.status(404).send('File not found');
            }
            if (file.contentType === 'image/jpeg') {
                res.setHeader('Content-Type', 'image/jpeg');
            }
            if (download) {
                res.download('files/' + fileName, (error) => {
                    if (error) {
                        console.error(error);
                    }
                    return;
                });
            } else {
                res.setHeader('Content-Disposition', 'filename=' + fileName);
                res.status(200).sendFile(fileName, { root: path.join('files') });
            }
        } catch (error) {
            return res.status(403).send('Forbidden');
        }
    } catch (error) {
        console.error(error);
        return res.status(500).send('Internal Server Error');
    }
}

function formatDateShort(date: Date): string {
    return new Date(date).toLocaleString(process.env.LOCALE, {
        dateStyle: 'short',
        timeStyle: 'short',
        timeZone: process.env.TIMEZONE
    });
}
