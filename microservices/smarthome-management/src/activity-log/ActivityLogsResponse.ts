import { ActivityLogEntry } from './ActivityLogEntry';

export interface ActivityLogsResponse {
    smartHomeName: string;
    enabled: boolean;
    overallSize: number;
    activityLogs: ActivityLogEntry[];
}
