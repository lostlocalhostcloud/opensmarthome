import { ActivityLog } from '../entities/ActivityLog';
import { ActivityLogsResponse } from './ActivityLogsResponse';

export class ActivityLogEntry {
    private readonly deviceName: string;
    private readonly deviceType: string;
    private readonly username: string;
    private readonly type: string;
    private readonly state: string;
    private readonly timestamp: number;
    private readonly formattedTimestamp: string;
    private readonly formattedTimestampShort: string;

    constructor(deviceName: string,
                deviceType: string,
                username: string,
                type: string,
                state: string,
                timestamp: number,
                formattedTimestamp: string,
                formattedTimestampShort: string) {
        this.deviceName = deviceName;
        this.deviceType = deviceType;
        this.username = username;
        this.type = type;
        this.state = state;
        this.timestamp = timestamp;
        this.formattedTimestamp = formattedTimestamp;
        this.formattedTimestampShort = formattedTimestampShort;
    }
    
    public static mapEntityToDto(entity: ActivityLog): ActivityLogEntry {
        const timestamp = new Date(entity.timestamp).toLocaleString(process.env.LOCALE, {
            weekday: 'short',
            year: 'numeric',
            month: 'long',
            day: 'numeric',
            hour: '2-digit',
            minute: '2-digit',
            timeZone: process.env.TIMEZONE
        });
        const timestampShort = new Date(entity.timestamp).toLocaleString(process.env.LOCALE, {
            dateStyle: 'short',
            timeStyle: 'short',
            timeZone: process.env.TIMEZONE
        });
        return new ActivityLogEntry(entity.device.name,
            entity.device.type.name,
            entity.user?.username,
            entity.type.name,
            entity.state,
            entity.timestamp.getTime(),
            timestamp,
            timestampShort);
    }

    public static convertToResponse(smartHomeName: string, enabled: boolean, overallSize: number, activityLogs: ActivityLogEntry[]): ActivityLogsResponse {
        return {
            smartHomeName,
            enabled,
            overallSize,
            activityLogs
        };
    }
}
