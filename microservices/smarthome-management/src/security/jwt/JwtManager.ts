const jwt = require('jsonwebtoken');

export function verifyJwt(authorizationCookie: string): Promise<any> {
    return new Promise(((resolve, reject) => {
        jwt.verify(authorizationCookie, process.env.JWT_TOKEN_SECRET, (error: any, decoded: any) => {
            if (error) reject(error);
            resolve(decoded);
        });
    }));
}