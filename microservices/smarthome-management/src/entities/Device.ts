import {Column, Entity, JoinColumn, ManyToOne, PrimaryColumn} from 'typeorm';
import {DeviceType} from './DeviceType';
import {Gateway} from './Gateway';

@Entity()
export class Device {

    @PrimaryColumn('uuid')
    id: string;

    @Column()
    name: string;

    @ManyToOne(() => DeviceType, { nullable: false, eager: true })
    @JoinColumn()
    type: DeviceType;

    @Column({name: 'typeId'})
    typeId: string;


    @ManyToOne(() => Gateway, { nullable: false, eager: true })
    @JoinColumn()
    gateway: Gateway;

    @Column({ nullable: false })
    cloudTriggerable: boolean;

    @Column({ nullable: false })
    hasSensor: boolean;

}
