import { Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { User } from './User';
import { SmartHome } from './SmartHome';

@Entity()
export class UserSmartHome {

    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(() => User, { nullable: false, eager: true })
    @JoinColumn()
    user: User;

    @ManyToOne(() => SmartHome, { nullable: false, eager: true })
    @JoinColumn()
    smartHome: SmartHome;

}
