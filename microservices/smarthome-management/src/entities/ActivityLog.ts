import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Device } from './Device';
import { User } from './User';
import { ActivityLogType } from './ActivityLogType';
import { SmartHome } from './SmartHome';

@Entity()
export class ActivityLog {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: 'timestamptz' })
    timestamp: Date;

    @ManyToOne(() => Device, { nullable: false, eager: true })
    @JoinColumn()
    device: Device;

    @ManyToOne(() => SmartHome, { nullable: false, eager: true })
    @JoinColumn()
    smartHome: SmartHome;

    @ManyToOne(() => User, { nullable: true, eager: true })
    @JoinColumn()
    user: User;

    @ManyToOne(() => ActivityLogType, { nullable: false, eager: true })
    @JoinColumn()
    type: ActivityLogType;

    @Column({ nullable: true })
    state: string;

}
