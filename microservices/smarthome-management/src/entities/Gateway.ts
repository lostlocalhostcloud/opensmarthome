import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { SmartHome } from './SmartHome';

@Entity()
export class Gateway {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column({ unique: true })
    key: string;

    @ManyToOne(() => SmartHome, { nullable: false, eager: true})
    @JoinColumn()
    smartHome: SmartHome;

}
