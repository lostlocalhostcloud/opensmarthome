import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { User } from './User';

@Entity()
export class Session {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({ unique: true })
    token: string;

    @ManyToOne(() => User, { nullable: false })
    @JoinColumn()
    user: User;

    @Column({ name: 'userId' })
    userId: number;

    @Column()
    createdAt: string;

}
