import {Column, Entity, PrimaryGeneratedColumn} from 'typeorm';

@Entity()
export class DeviceType {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({ nullable: false })
    name: string;

}
