import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class ActivityLogType {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({ nullable: false })
    name: string;

}
