import { createConnection } from 'typeorm';
import express from 'express';
import { DBConnectionHolder, GatewayHolder } from './types';
import { User } from './entities/User';
import { UserSmartHome } from './entities/UserSmartHome';
import { TypeOrm } from './Database';
import { SmartHome } from './entities/SmartHome';
import { smartHomeManager } from './endpoints/SmartHomeManagement';
import { Session } from './entities/Session';
import { Socket } from 'socket.io';
import { Device } from './entities/Device';
import { DeviceType } from './entities/DeviceType';
import { Gateway } from './entities/Gateway';
import { Type } from './gateprotocol/protocol';
import { verifyJwt } from './security/jwt/JwtManager';
import { deviceManager } from './endpoints/DeviceManagement';
import { health } from './endpoints/Health';
import { ActivityLog } from './entities/ActivityLog';
import { versionEndpoint } from './endpoints/VersionEndpoint';
import { configureSharedApiResources } from 'osh-microservice-commons';
import { VERSION } from './version';

require('dotenv').config();
const app = require('express')();
const cookie = require('cookie');
const http = require('http').Server(app);
const { Server } = require('socket.io');


app.use(express.json());

// Set Service Header
app.use(function (req, res, next) {
    res.setHeader('Service', process.env.HOSTNAME ? process.env.HOSTNAME : 'smarthome-mngmt-service');
    next();
});

app.use('/health', health);
app.use('/version', versionEndpoint);

const port = process.env.PORT ? process.env.PORT : 3000;
http.listen(port, () => {
    return console.log(`server is listening on ${port}`);
});

createConnection().then(async connection => {

    const io = new Server(http);

    const activityLogRepository = connection.getRepository(ActivityLog);
    const deviceRepository = connection.getRepository(Device);
    const deviceTypeRepository = connection.getRepository(DeviceType);
    const gatewayRepository = connection.getRepository(Gateway);
    const sessionRepository = connection.getRepository(Session);
    const smartHomeRepository = connection.getRepository(SmartHome);
    const userRepository = connection.getRepository(User);
    const userSmartHomeRepository = connection.getRepository(UserSmartHome);
    DBConnectionHolder.setConn(new TypeOrm(
        activityLogRepository,
        deviceRepository,
        deviceTypeRepository,
        gatewayRepository,
        sessionRepository,
        smartHomeRepository,
        userRepository,
        userSmartHomeRepository
    ));
    configureSharedApiResources(sessionRepository, VERSION);

    app.use(express.json());

    const apiPrefix = '/api/v1';
    const smartHomesPrefix = apiPrefix + '/smarthomes';
    app.use(smartHomesPrefix, deviceManager);
    app.use(smartHomesPrefix, smartHomeManager);

    io.on('connection', async (socket: Socket) => {
        const req = socket.client.request;

        let gateway: Gateway;

        const authorization = cookie.parse(req.headers.cookie || '').authorization;
        if (('authorization' in req.headers)) {
            try {
                gateway = await gatewayRepository.findOneByOrFail({ key: req.headers.authorization });
                console.log(`Gateway ${gateway.name} of ${gateway.smartHome.name} is Connected with socketId: ${socket.id}`);
                GatewayHolder.setGateConnection(`${gateway.smartHome.id}-${gateway.id}`, socket);
                try {
                    // Emit Sensor State change to Frontend
                    (await GatewayHolder.getGateConn(`${gateway.smartHome.id}-${gateway.id}`)).on(Type.SensorEvent, (data) => {
                        DBConnectionHolder.getConn().writeActivityLog(new Date(data._meta.timestamp), String(data.deviceId), null, 1, String(data.sensorState))
                            .then()
                            .catch((error) => console.error(`Error saving SensorEvent to ActivityLog: ${error}`));
                        io.to(String(gateway.smartHome.id)).emit(Type.SensorEvent, data);
                    });
                    (await GatewayHolder.getGateConn(`${gateway.smartHome.id}-${gateway.id}`)).on('healthcheck', (data, callback) => {
                        console.debug(`Successful healthcheck with Gateway ${gateway.name} of ${gateway.smartHome.name}`);
                        callback({
                            _meta: {
                                apiVersion: 'v1',
                                name: 'healthcheck',
                                type: 'CALLBACK',
                                timestamp: Date.now()
                            },
                            status: 'ok'
                        });
                    });
                } catch (e) {
                    return;
                }
            } catch (EntityNotFoundError) {
                socket.disconnect();
                return;
            }
        } else if (authorization) {
            try {
                const username = (await verifyJwt(authorization)).username;
                if (username) {
                    const user = await userRepository.findOneByOrFail({ username });
                    const smartHomes = await userSmartHomeRepository.find({ where: { user: { id: user.id } } });
                    smartHomes.forEach((smartHome) => {
                        socket.join(String(smartHome.smartHome.id));
                    });
                }
            } catch (e) {
                socket.disconnect();
                return;
            }
        } else {
            socket.disconnect();
            return;
        }
    });

}).catch(error => console.log(error));