interface Meta {
    api_version: string;
    name: string;
    type: string;
    timestamp: number;
}
class Type {
    public static IOCreateType = 'IOCreate';
    public static IOUpdateType = 'IOUpdate';
    public static IODeleteType = 'IODelete';
    public static DataRequest = 'DataRequest';
    public static IOEntryKinds = {
        Sensor: 'SENSOR',
        Trigger: 'TRIGGER',
        Action: 'ACTION',
        Device: 'DEVICE',
    };
    public static DeviceCreateType = 'DeviceCreate';
    public static DeviceUpdateType = 'DeviceUpdate';
    public static DeviceDeleteType = 'DeviceDelete';
    public static StatusResponse = 'StatusResponse';
    public static DataResponse = 'DataResponse';
    public static Status = {
        OK: 'OK',
        FAIL: 'FAIL',
    };
    public static CloudTrigger = 'CloudTrigger';
    public static SensorEvent = 'SensorEvent';
    public static SensorStates = 'SensorStates';
}

interface IOCreate {
    kind: string;
    name: string;
    config: any;
    typeId: number;
    deviceId: string;
}

interface IOUpdate {
    kind: string;
    id: string;
    name?: string;
    config?: any;
    typeId?: number;
}

interface IODelete {
    kind: string;
    id: string;
}

interface DataRequest {
    kind: string;
    deviceId?: string;
}

interface DeviceCreate {
    name: string;
    typeId: number;
    actions?: IOCreate[];
    sensors?: IOCreate[];
    triggers?: IOCreate[];
}

interface DeviceUpdate {
    id: string;
    name: string;
    typeId: number;
}

interface DeviceDelete {
    id: string;
}

interface CloudTrigger {
    deviceId: string;
}

interface SensorEvent {
    deviceId: string;
    sensorState: any;
}

interface SensorStates {
    deviceSensorStates: {
        deviceId: string;
        sensorState: any;
    }[];
}

interface Event {
    _meta: Meta;
    payload: IOCreate | IODelete | IOUpdate | DeviceCreate | DeviceUpdate | DeviceDelete | CloudTrigger | SensorEvent | SensorStates | DataRequest | Response;
}

interface Response {
    status: any;
    id?: string | number;
}

export {
    Event, Response, Meta, IOCreate, IOUpdate, IODelete, DeviceCreate, DeviceUpdate, DeviceDelete, CloudTrigger, SensorEvent, SensorStates, Type
};