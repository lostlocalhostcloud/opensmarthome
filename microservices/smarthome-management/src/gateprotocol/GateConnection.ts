import { Event, IOCreate, Meta, Type } from './protocol';
import { Socket } from 'socket.io';

export class GateConnection {

    constructor(private socket: Socket,
                private gatewayId: string,
                private smarthomeId: string) {

    }

    public cloudTrigger(deviceId: string): Promise<Event> {
        return new Promise<Event>(((resolve, reject) => {
            const type = Type.CloudTrigger + '-' + deviceId;
            this.emitEvent(type, {
                _meta: this.genMeta(type),
                deviceId
            }, true).then(response => {
                if (response.status == Type.Status.OK) {
                    resolve(response);
                }
                reject(response);
            }).catch(error => reject(error));
        }));
    }

    public sensorStates(deviceIds: string[]): Promise<Event> {
        return new Promise<Event>(((resolve, reject) => {
            const type =  Type.SensorStates;
            this.emitEvent(type, {
                _meta: this.genMeta(type),
                deviceIds: deviceIds
            }, true).then(response => {
                if (response.status == Type.Status.OK) {
                    resolve(response);
                }
                reject(response);
            }).catch(error => reject(error));
        }));
    }

    public deleteDeviceTrigger(id: string): Promise<Event> {
        return this.deleteIOEntry(id);
    }

    public editDeviceTrigger(id: number, name?: string, config?: any, typeId?: number): Promise<Event> {
        return this.updateIOEntry(id, name, config, typeId);
    }

    public addDeviceTrigger(name: string, config: any, typeId: number, deviceId: string): Promise<Event> {
        return this.createIOEntry(name, config, typeId, deviceId);
    }

    public deleteDeviceSensor(id: string): Promise<Event> {
        return this.deleteIOEntry(id);
    }

    public editDeviceSensor(id: number, name: string, config: any, typeId: number): Promise<Event> {
        return this.updateIOEntry(id, name, config, typeId);
    }

    public addDeviceSensor(name: string, config: any, typeId: number, deviceId: string): Promise<Event> {
        return this.createIOEntry(name, config, typeId, deviceId);
    }

    public deleteDeviceAction(id: string): Promise<Event> {
        return this.deleteIOEntry(id);
    }

    public editDeviceAction(id: number, name: string, config: any, typeId: number): Promise<Event> {
        return this.updateIOEntry(id, name, config, typeId);
    }

    public addDeviceAction(name: string, config: any, typeId: number, deviceId: string): Promise<Event> {
        return this.createIOEntry(name, config, typeId, deviceId);
    }

    public addDevice(name: string, typeId: number,
                     actions?: IOCreate[], sensors?: IOCreate[], triggers?: IOCreate[]): Promise<Event> {

        const promise = new Promise<Event>((resolve, reject) => {
            const type = Type.DeviceCreateType;
            return this.socket.emit(type,{
                _meta: this.genMeta(type),
                payload: {
                    name,
                    typeId,
                    actions,
                    sensors,
                    triggers
                }
            },
                this.convertData(resolve, reject)
            );
        });
        return promise;


    }

    public editDevice(id: string, name: string, typeId: number): Promise<Event> {
        const promise = new Promise<Event>((resolve, reject) => {
            const type = Type.DeviceUpdateType;
            this.socket.emit(type,{
                _meta: this.genMeta(type),
                payload: {
                    id,
                    name,
                    typeId
                }
            },
                this.convertData(resolve, reject)
            );
        });
        return promise;
    }

    public deleteDevice(id: string): Promise<Event> {
        const promise = new Promise<Event>((resolve, reject) => {
            const type = Type.DeviceDeleteType;
            this.socket.emit(type,{
                _meta: this.genMeta(type),
                payload: {
                    id,
                }
            },
                this.convertData(resolve, reject)
            );
        });

        return promise;
    }

    private genMeta(type: string): Meta {
        return {
            api_version: 'v1',
            name: 'string',
            type: type,
            timestamp: Date.now()
        };
    }

    private updateIOEntry(id: number, name: string, config: any, typeId: number): Promise<Event> {
        const promise = new Promise<Event>((resolve, reject) => {
            const type = Type.IOUpdateType;
            return this.socket.emit(type, {
                _meta: this.genMeta(type),
                payload: {
                    id,
                    name,
                    config,
                    typeId
                }
            },
                this.convertData(resolve, reject)
            );
        });
        return promise;
    }

    private createIOEntry(name: string, config: string, typeId: number, deviceId: string): Promise<Event> {
        const promise = new Promise<Event>((resolve, reject) => {
            const type = Type.IOCreateType;
            this.socket.emit(type, {
                _meta: this.genMeta(type),
                payload: {
                    name,
                    config,
                    typeId,
                    deviceId
                }
            },
                this.convertData(resolve, reject)
            );
        });
        return promise;
    }

    private deleteIOEntry(id: string,): Promise<Event> {
        const promise = new Promise<Event>(((resolve, reject) => {
            const type = Type.IOUpdateType;
            this.socket.emit(type, {
                _meta: this.genMeta(type),
                payload: {
                    id
                }
            },
                this.convertData(resolve, reject)
            );
        }));
        return promise;

    }

    private getDevices(IOEntryKind: string): Promise<Event> {
        const promise = new Promise<Event>((resolve, reject) => {
            const type = Type.DataRequest;
            this.emitEvent(type,{
                _meta: this.genMeta(type),
                payload: {
                    kind: IOEntryKind,
                }
            },
                // FIXME
                // this.convertData(resolve, reject)
            );
        });
        return promise;
    }

    private getIOEntries(IOEntryKind: string, deviceId: string): Promise<Event> {
        const promise = new Promise<Event>((resolve, reject) => {
            const type = Type.DataRequest;
            this.emitEvent(type,{
                    _meta: this.genMeta(type),
                    payload: {
                        kind: IOEntryKind,
                        deviceId,
                    }
                },
                // FIXME
                // this.convertData(resolve, reject)
            );
        });
        return promise;
    }

    private convertData(resolve: any, reject: any) {
        return (dataRaw) => {
            try {
                const data: Event = dataRaw as Event;
                resolve(data);
            } catch (e) {
                reject(e);
            }
        };
    }

    public emitEvent(event: string, data: any, withResponse?: boolean): Promise<any> {
        const timeout = process.env.EVENT_TIMEOUT ? Number.parseInt(process.env.EVENT_TIMEOUT) : 10000;
        if (withResponse === true) {
            return new Promise((resolve, reject) => {
                this.socket.volatile.timeout(timeout).emit(event, data, (err, response) => {
                    if (err) {
                        reject(err);
                    }
                    resolve(response);
                });
            });
        }
        return new Promise((resolve, reject) => {
            this.socket.volatile.timeout(timeout).emit(event, data, (err) => {
                if (err) {
                    reject(err);
                }
                resolve(undefined);
            });
        });
    }

    public on(event: string, callback: any): this {
        this.socket.on(event, callback);
        return this;
    }



}