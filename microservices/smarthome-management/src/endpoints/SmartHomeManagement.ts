import { Router } from 'express';
import { isAuthorized } from 'osh-microservice-commons';
import { DBConnectionHolder } from '../types';
import { ActivityLogEntry } from '../activity-log/ActivityLogEntry';

export const smartHomeManager = Router();

smartHomeManager.get('/', isAuthorized(), (req, res) => {
    DBConnectionHolder.getConn().findSmartHomesByUsername(res.locals.decodedJwt.username).then((smartHomes) => {
        return res.status(200).json(smartHomes);
    }).catch(() => {
        return res.status(500).send('Internal Server Error');
    });
});

smartHomeManager.get('/:smarthome_id/activity-log', isAuthorized(), (req, res) => {
    let typeId: number;
    switch (String(req.query.type)) {
        case 'SENSOR':
            typeId = 1;
            break;
        case 'TRIGGER':
            typeId = 2;
            break;
    }
    let limit: number;
    let skip: number;
    if (req.query.page !== undefined && req.query.limit !== undefined) {
        const page = Number(req.query.page);
        limit = Number(req.query.limit);
        skip = (limit * page) - limit;
    }
    const smartHomeId = Number(req.params.smarthome_id);
    DBConnectionHolder.getConn().findActivityLogsBySmartHomeIdAndType(smartHomeId, typeId, limit, skip).then((result) => {
        DBConnectionHolder.getConn().findSmartHomeByIdOrFail(smartHomeId).then((smartHome) => {
            const activityLogs = result.activityLogs.map(entry => ActivityLogEntry.mapEntityToDto(entry));
            return res.status(200).json(ActivityLogEntry.convertToResponse(smartHome.name, smartHome.activityLogEnabled, result.count, activityLogs));
        }).catch((error) => {
            return res.status(500).send(error.stack);
        });
    }).catch((error) => {
        return res.status(500).send(error.stack);
    });
});
