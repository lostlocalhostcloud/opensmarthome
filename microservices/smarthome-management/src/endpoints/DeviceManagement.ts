import { Router } from 'express';
import { isAuthorized } from 'osh-microservice-commons';
import { DBConnectionHolder, GatewayHolder } from '../types';

class DeviceResponse {
    id: string;
    name: string;

    constructor(id: string, name: string) {
        this.id = id;
        this.name = name;
    }
}

export const deviceManager = Router();

deviceManager.get('/:smarthome_id/device/:device_id', isAuthorized(), (req, res) => {
    const smartHomeId = req.params.smarthome_id;
    const deviceId = req.params.device_id;
    DBConnectionHolder.getConn().findSmartHomeByIdAndUsernameOrFail(Number.parseInt(smartHomeId), res.locals.decodedJwt.username).then(() => {
        DBConnectionHolder.getConn().findDeviceByIdOrFail(deviceId).then((device) => {
            return res.status(200).json(new DeviceResponse(device.id, device.name));
        }).catch(() => {
            return res.status(500).send('Internal Server Error');
        });
    }).catch(() => {
        return res.status(401).send('No access rights to this SmartHome');
    });
});

deviceManager.get('/:smarthome_id/devices/:device_id/trigger', isAuthorized(), (req, res) => {
    DBConnectionHolder.getConn().findDeviceByIdAndCloudTriggerable(req.params.device_id, true).then((device) => {
        if (req.params.smarthome_id === String(device.gateway.smartHome.id)) {
            DBConnectionHolder.getConn().findUserByUsernameOrFail(res.locals.decodedJwt.username).then((user) => {
                DBConnectionHolder.getConn().findUserSmartHomeByUserIdAndSmartHomeIdOrFail(user.id, device.gateway.smartHome.id).then(() => {
                    GatewayHolder.getGateConn(`${req.params.smarthome_id}-${device.gateway.id}`).then((conn) => {
                        conn.cloudTrigger(req.params.device_id).then(() => {
                            DBConnectionHolder.getConn().writeActivityLog(new Date(), device.id, user.id, 2)
                                .then()
                                .catch((error) => {
                                    console.error(error);
                                });
                            return res.status(200).send('OK');
                        }).catch(() => {
                            return res.status(500).send('Device could not be triggered');
                        });
                    }).catch(() => {
                        return res.status(500).send('Gateway not connected');
                    });
                }).catch(() => {
                    return res.status(401).send('This User is not authorized to trigger this device');
                });
            });
        } else {
            return res.status(401).send('This Device is not assigned to the requested SmartHome');
        }
    }).catch(() => {
        return res.status(500).send('Device does not exists or not Cloud triggerable');
    });
});

deviceManager.get('/:smarthome_id/devices/sensor_states', isAuthorized(), async (req, res) => {
    const smartHomeId = req.params.smarthome_id;
    const devices = await DBConnectionHolder.getConn().findDevicesBySmartHomeId(Number.parseInt(smartHomeId));
    const response = {
        devices: [],
        offlineGateways: []
    };
    DBConnectionHolder.getConn().findGatewaysBySmartHomeId(Number.parseInt(smartHomeId)).then(async (gateways) => {
        for (const gateway of gateways) {
            const foundDeviceIds = [];
            for (const device of devices) {
                if (gateway.id === device.gateway.id) {
                    foundDeviceIds.push(device.id);
                }
            }
            if (foundDeviceIds.length > 0) {
                try {
                    const conn = await GatewayHolder.getGateConn(`${smartHomeId}-${gateway.id}`);
                    try {
                        const sensorStatesResponse = await conn.sensorStates(foundDeviceIds);
                        console.log(sensorStatesResponse);
                        // eslint-disable-next-line
                        // @ts-ignore
                        sensorStatesResponse.deviceSensorStates.forEach((deviceSensorState) => {
                            console.log(deviceSensorState);
                            response.devices.push({ id: deviceSensorState.id, sensorState: deviceSensorState.sensorState });
                        });
                    } catch (error) {
                        console.log(error);
                        return res.status(500).send('Device Ids could not be found');
                    }
                } catch (error) {
                    console.log(error);
                    response.offlineGateways.push({ gatewayId: gateway.id, devices: foundDeviceIds });
                }
            }
        }
    }).catch((error) => {
        console.log(error);
        return res.status(500).send('Internal Server Error');
    }).finally(() => {
        if (response.offlineGateways.length > 0) {
            return res.status(207).json(response);
        }
        return res.status(200).json(response);
    });
});

deviceManager.get('/:smarthome_id/devices', isAuthorized(), (req, res) => {
    const smartHomeId = req.params.smarthome_id;
    DBConnectionHolder.getConn().findSmartHomeByIdAndUsernameOrFail(Number.parseInt(smartHomeId), res.locals.decodedJwt.username).then((smartHome) => {
        DBConnectionHolder.getConn().findDevicesBySmartHomeId(smartHome.id).then((foundDevices) => {
            const devices = [];
            foundDevices.forEach((device) => {
                if (device.cloudTriggerable) {
                    devices.push({
                        id: device.id,
                        name: device.name,
                        online: GatewayHolder.isOnline(smartHomeId + '-' + device.gateway.id),
                        typeId: device.typeId,
                        hasSensor: device.hasSensor
                    });
                }
            });
            return res.status(200).json(devices);
        }).catch(() => {
            return res.status(500).send('Internal Server Error');
        });
    }).catch(() => {
        return res.status(401).send('No access rights to this SmartHome');
    });
});