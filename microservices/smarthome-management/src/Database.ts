import { FindManyOptions, Repository } from 'typeorm';
import { SmartHome } from './entities/SmartHome';
import { User } from './entities/User';
import { UserSmartHome } from './entities/UserSmartHome';
import { Session } from './entities/Session';
import { Device } from './entities/Device';
import { Gateway } from './entities/Gateway';
import { DeviceType } from './entities/DeviceType';
import { ActivityLog } from './entities/ActivityLog';

interface Database {

    findDeviceByIdOrFail(id: string): Promise<Device>;
    findDeviceByIdAndCloudTriggerable(id: string, cloudTriggerable: boolean): Promise<Device>;
    findDevicesByGatewayId(gatewayId: number): Promise<Device[]>;
    findDevicesBySmartHomeId(smartHomeId: number): Promise<Device[]>;

    findGatewaysBySmartHomeId(smartHomeId: number): Promise<Gateway[]>;

    findSessionsByUsername(username: string): Promise<Session[]>;
    findSmartHomesByUsername(username: string): Promise<SmartHome[]>;
    findUserByUsernameOrFail(username: string): Promise<User>;

    findSmartHomeByIdOrFail(id: number): Promise<SmartHome>;
    findSmartHomeByIdAndUsernameOrFail(id: number, username: string): Promise<SmartHome>;

    findUserById(userId: number): Promise<User>;
    findUserSmartHomesByUserId(userId: number): Promise<UserSmartHome[]>;
    findUserSmartHomeByUserIdAndSmartHomeIdOrFail(userId: number, smartHomeId: number): Promise<UserSmartHome>;

    // ActivityLog
    writeActivityLog(timestamp: Date, deviceId: string, userId: number, typeId: number, state?: string): Promise<void>;
    findActivityLogsBySmartHomeIdAndType(smartHomeId: number, typeId?: number, take?: number, skip?: number): Promise<{ activityLogs: ActivityLog[]; count: number; }>;

}

class TypeOrm implements Database {

    constructor(
        private activityLogRepository: Repository<ActivityLog>,
        private deviceRepository: Repository<Device>,
        private deviceTypeRepository: Repository<DeviceType>,
        private gatewayRepository: Repository<Gateway>,
        private sessionRepository: Repository<Session>,
        private smartHomeRepository: Repository<SmartHome>,
        private userRepository: Repository<User>,
        private userSmartHomeRepository: Repository<UserSmartHome>
    ) {
    }

    writeActivityLog(timestamp: Date, deviceId: string, userId: number, typeId: number, state?: string): Promise<void> {
        return new Promise(async (resolve, reject) => {
            try {
                const device = await this.findDeviceByIdOrFail(deviceId);
                const smartHome = await this.findSmartHomeByIdOrFail(device.gateway.smartHome.id);
                if (smartHome.activityLogEnabled) {
                    const activityLog = new ActivityLog();
                    activityLog.timestamp = timestamp;
                    activityLog.device = device;
                    activityLog.smartHome = smartHome;
                    activityLog.user = userId !== null ? await this.findUserById(userId) : null;
                    activityLog.type = await this.deviceTypeRepository.findOneByOrFail({ id: typeId });
                    activityLog.state = state;
                    await this.activityLogRepository.save(activityLog);
                }
                resolve();
            }  catch (e) {
                reject(e);
            }
        });
    }

    findActivityLogsBySmartHomeIdAndType(smartHomeId: number, typeId?: number, take?: number, skip?: number): Promise<{ activityLogs: ActivityLog[]; count: number; }> {
        return new Promise(async (resolve, reject) => {
            try {
                const options: FindManyOptions<ActivityLog> = {
                    take,
                    skip,
                    where: typeId ? {
                        smartHome: { id: smartHomeId },
                        type: { id: typeId }
                    } : {
                        smartHome: { id: smartHomeId }
                    },
                    order: {
                        timestamp: 'DESC'
                    }
                };
                const [activityLogs, count] = await this.activityLogRepository.findAndCount(options);
                resolve({ activityLogs, count });
            } catch (e) {
                reject(e);
            }
        });
    }

    findDeviceByIdOrFail(id: string): Promise<Device> {
        return this.deviceRepository.findOneByOrFail({ id });
    }

    findDeviceByIdAndCloudTriggerable(id: string, cloudTriggerable: boolean): Promise<Device> {
        return new Promise(async (resolve, reject) => {
            try {
                const device = await this.findDeviceByIdOrFail(id);
                if (device.cloudTriggerable === cloudTriggerable) {
                    resolve(device);
                }
                throw new Error('Device is not Cloud triggerable');
            } catch (e) {
                reject(e);
            }
        });
    }

    findDevicesByGatewayId(gatewayId: number): Promise<Device[]> {
        return this.deviceRepository.find({ where: { gateway: { id: gatewayId } } });
    }

    findDevicesBySmartHomeId(smartHomeId: number): Promise<Device[]> {
        return new Promise(async (resolve, reject) => {
            try {
                const devices = [];
                for (const gateway of (await this.findGatewaysBySmartHomeId(smartHomeId))) {
                    devices.push(...(await this.findDevicesByGatewayId(gateway.id)));
                }
                resolve(devices);
            } catch (e) {
                reject(e);
            }
        });
    }

    findGatewaysBySmartHomeId(smartHomeId: number): Promise<Gateway[]> {
        return this.gatewayRepository.find({ where: { smartHome: { id: smartHomeId } } });
    }

    findSessionsByUsername(username: string): Promise<Session[]> {
        return new Promise(async (resolve, reject) => {
            try {
                const user = await this.findUserByUsernameOrFail(username);
                const sessions = await this.sessionRepository.find({ where: { user: user } });
                resolve(sessions);
            } catch (error) {
                reject(error);
            }
        });
    }

    findSmartHomeByIdOrFail(id: number): Promise<SmartHome> {
        return this.smartHomeRepository.findOneByOrFail({ id });
    }

    findSmartHomeByIdAndUsernameOrFail(id: number, username: string): Promise<SmartHome> {
        return new Promise(async (resolve, reject) => {
            try {
                const user = await this.findUserByUsernameOrFail(username);
                const userSmartHome = await this.findUserSmartHomeByUserIdAndSmartHomeIdOrFail(user.id, id);
                resolve(await this.smartHomeRepository.findOneByOrFail({ id: userSmartHome.smartHome.id }));
            } catch (e) {
                reject(e);
            }
        });
    }

    findSmartHomesByUsername(username: string): Promise<SmartHome[]> {
        return new Promise(async (resolve, reject) => {
            try {
                const user = await this.findUserByUsernameOrFail(username);
                const smartHomes = [];
                for (const userSmartHome of (await this.findUserSmartHomesByUserId(user.id))) {
                    smartHomes.push(await this.findSmartHomeByIdOrFail(userSmartHome.smartHome.id));
                }
                resolve(smartHomes);
            } catch (e) {
                reject(e);
            }
        });
    }

    findUserByUsernameOrFail(username: string): Promise<User> {
        return this.userRepository.findOneByOrFail({ username });
    }

    findUserById(userId: number): Promise<User> {
        return new Promise(async (resolve, reject) => {
           try {
               resolve(await this.userRepository.findOneBy({ id: userId }));
           } catch (e) {
               reject(e);
           }
        });
    }

    findUserSmartHomesByUserId(userId: number): Promise<UserSmartHome[]> {
        return new Promise(async (resolve, reject) => {
            try {
                resolve(await this.userSmartHomeRepository.find({ where: { user: { id: userId } } }));
            } catch (e) {
                reject(e);
            }
        });
    }

    findUserSmartHomeByUserIdAndSmartHomeIdOrFail(userId: number, smartHomeId: number): Promise<UserSmartHome> {
        return this.userSmartHomeRepository.findOneOrFail({ where: { user: { id: userId }, smartHome: { id: smartHomeId } } });
    }

}

export { Database, TypeOrm };