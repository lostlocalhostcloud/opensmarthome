import { Database } from './Database';
import { GateConnection } from './gateprotocol/GateConnection';
import { Socket } from 'socket.io';

class DBConnectionHolder {
    private db: Database;
    private static instance: DBConnectionHolder;

    constructor(db: Database) {
        this.db = db;
    }

    public static setConn(db: Database): void {
        this.instance = new DBConnectionHolder(db);
    }

    public static getConn(): Database {
        // TODO: fail save
        return this.instance.db;
    }
}

interface IClients {
    [indexer: string]: Socket;
}

class GatewayHolder {
    private clientsByID: IClients = {};
    private static instance: GatewayHolder;

    constructor(clientsByID: IClients) {
        this.clientsByID = clientsByID;
    }

    public static setGateConnection(gatewayId: string, socket: Socket): void {
        if (this.instance === undefined) {
            this.instance = new GatewayHolder({});
        }
        this.instance.clientsByID[gatewayId] = socket;
        this.instance.clientsByID[gatewayId].on('disconnect', () => {
            this.instance.clientsByID[gatewayId] = undefined;
        });
    }

    public static getGateConn(gatewayId: string): Promise<GateConnection> {
        return new Promise<GateConnection>((resolve, reject) => {
            if (this.instance === undefined) {
                reject('Singleton not initialized');
                return;
            }
            if (this.instance.clientsByID[gatewayId] === undefined) {
                reject('Gateway not connected');
                return;
            }
            resolve(new GateConnection(this.instance.clientsByID[gatewayId], '', ''));
        });
    }

    public static isOnline(gatewayId: string): boolean {
        if (this.instance === undefined) {
            return false;
        }
        if (this.instance.clientsByID[gatewayId] === undefined) {
            return false;
        }
        return true;
    }
}

export { DBConnectionHolder, GatewayHolder };