import { EventTypes, SocketEvent } from './SocketEvent';
import { Type } from './Type';

export class SensorEvent extends SocketEvent {
    deviceId: string;
    sensorState: any;

    constructor(deviceId: string, sensorState: any) {
        super({ apiVersion: 'v1', name: Type.SensorEvent, type: EventTypes.EVENT, timestamp: Date.now() });
        this.deviceId = deviceId;
        this.sensorState = sensorState;
    }
}
