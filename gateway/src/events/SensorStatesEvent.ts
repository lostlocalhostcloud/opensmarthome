import { EventTypes, SocketEvent } from './SocketEvent';
import { Type } from './Type';

export class SensorStatesEvent extends SocketEvent {
    deviceIds: string[];

    constructor(deviceIds: string[]) {
        super({ apiVersion: 'v1', name: Type.SensorStates, type: EventTypes.EVENT, timestamp: Date.now() });
        this.deviceIds = deviceIds;
    }
}

export class SensorStatesEventResponse extends SocketEvent {
    deviceSensorStates: {deviceId: string, sensorState: any}[];
    status: string;

    constructor(deviceSensorStates: {deviceId: string, sensorState: any}[], status: string) {
        super({ apiVersion: 'v1', name: Type.SensorStates, type: EventTypes.RESPONSE, timestamp: Date.now() });
        this.deviceSensorStates = deviceSensorStates;
        this.status = status;
    }
}
