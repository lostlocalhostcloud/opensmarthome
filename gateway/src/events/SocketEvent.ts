export abstract class SocketEvent {
    _meta: {
        apiVersion: string;
        name: string;
        type: string;
        timestamp: number;
    };

    protected constructor(meta: { apiVersion: string; name: string; type: string; timestamp: number; }) {
        this._meta = meta;
    }
}

export enum Events {
    HEALTHCHECK = 'healthcheck',
}

export enum EventTypes {
    EVENT = 'event',
    RESPONSE = 'response',
}