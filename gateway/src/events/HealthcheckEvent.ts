import { Events, EventTypes, SocketEvent } from './SocketEvent';

export class HealthcheckEvent extends SocketEvent {
    constructor() {
        super({ apiVersion: 'v1', name: Events.HEALTHCHECK, type: EventTypes.EVENT, timestamp: Date.now() });
    }
}

export class HealthcheckEventResponse extends SocketEvent {
    status: string;

    constructor(status: string) {
        super({ apiVersion: 'v1', name: Events.HEALTHCHECK, type: EventTypes.RESPONSE, timestamp: Date.now() });
        this.status = status;
    }
}
