export class Type {
    public static CloudTrigger = 'CloudTrigger';
    public static SensorEvent = 'SensorEvent';
    public static SensorStates = 'SensorStates';
}
