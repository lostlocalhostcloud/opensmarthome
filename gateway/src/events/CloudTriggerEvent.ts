import { EventTypes, SocketEvent } from './SocketEvent';
import { Type } from './Type';

export class CloudTriggerEvent extends SocketEvent {
    deviceId: string;

    constructor(event: CloudTriggerEvent) {
        super(event._meta);
        this.deviceId = event.deviceId;
    }
}

export class CloudTriggerResponseEvent extends SocketEvent {
    deviceId: string;
    status: string;

    constructor(deviceId: string, status: string) {
        super({ apiVersion: 'v1', name: Type.CloudTrigger, type: EventTypes.RESPONSE, timestamp: Date.now() });
        this.deviceId = deviceId;
        this.status = status;
    }
}
