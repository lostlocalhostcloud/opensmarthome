import { ISensor } from '../types';
import { DisabledGpio, EnabledGpio, IGpio } from '../helpers/GPIOHelper';

export class GPIOSensor implements ISensor<boolean> {

    private sensor: IGpio;

    read(): boolean {
        return GPIOSensor.getSensorValue(this.sensor.readSync(), this.inverted);
    }

    constructor(
        private gpioEnabled: boolean,
        private deviceId: string,
        private pin: number,
        private inverted: boolean,
    ) {
    }

    setup(onEvent: (deviceId: string, data: boolean) => void): boolean {
        this.sensor = this.gpioEnabled ? new EnabledGpio(this.pin, 'in', 'both', { debounceTimeout: 150 }) : new DisabledGpio();
        console.log('Setup pin: ' + this.pin);
        this.sensor.watch((error, value) => {
            if (error) {
                throw error;
            }

            onEvent(this.deviceId, GPIOSensor.getSensorValue(value, this.inverted));
        });
        return true;
    }

    private static getSensorValue(value: number, inverted: boolean): boolean {
        return inverted ? value === 0 : value === 1;
    }

}