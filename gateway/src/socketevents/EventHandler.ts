import { SocketSingleton } from '../singletons/SocketSingleton';
import { Device } from '../database/entity/Device';
import { Action } from '../database/entity/Action';
import { Sensor } from '../database/entity/Sensor';
import { Trigger } from '../database/entity/Trigger';
import { Database } from '../Database';
import { v4 as uuidv4 } from 'uuid';
import { DeviceCreate, IOCreate, IODelete, IOUpdate, Type } from '../gateprotocol/protocol';
import { DBConnectionSingleton } from '../singletons/DBConnectionSingleton';
import { GateConnection } from '../gateprotocol/GateConnection';

export class EventHandler {

    private static database: Database;

    public static initEventHandler(database: Database) {
        EventHandler.database = database;

        this.onDeviceCreate(database);

        this.onDeviceDelete();

        this.onDeviceUpdate(database);

        this.DataRequest();

        this.onCreateIOEntry();

        this.onUpdateIOEntry();

        this.onDeleteIOEntry();
    }

    private static onCreateIOEntry() {
        SocketSingleton.getInstance().setupListener(Type.IOCreateType, async (event, cb) => {
            const IOEntryType = this.MapKindToEntity(event);
            if (IOEntryType) {
                await EventHandler.createIOEntry(cb, IOEntryType, event);
                return;
            }
            cb(GateConnection.genUpdateResponse(false));
            return;
        });
    }

    private static onUpdateIOEntry() {
        SocketSingleton.getInstance().setupListener(Type.IOUpdateType, async (event, cb) => {
            const IOEntryType = this.MapKindToEntity(event);
            if (IOEntryType) {
                await EventHandler.updateIOEntry(cb, IOEntryType, event);
                return;
            }
            cb(GateConnection.genUpdateResponse(false));
            return;
        });
    }

    private static onDeleteIOEntry() {
        SocketSingleton.getInstance().setupListener(Type.IODeleteType, async (event, cb) => {
            const IOEntryType = this.MapKindToEntity(event);
            if (IOEntryType) {
                await EventHandler.deleteIOEntry(cb, Sensor, event);
                return;
            }
            cb(GateConnection.genDeletedResponse(false));
            return;
        });
    }

    private static DataRequest() {
        SocketSingleton.getInstance().setupListener(Type.DataRequest, async (event, cb) => {
            console.log(event);
            await EventHandler.findIOEntryByDeviceID(cb, Trigger, event.payload.id);
        });
    }

    private static onDeviceUpdate(database: Database) {
        SocketSingleton.getInstance().setupListener(Type.DeviceUpdateType, async (event, cb) => {
            try {
                const device = await database.findDeviceById(event.payload.id);
                device.name = event.name;
                device.typeId = event.typeId;
                database.saveDevice(device);
            } catch (e) {
                cb(GateConnection.genDeletedResponse(false));
                return;
            }
            cb(GateConnection.genDeletedResponse(false));
            return;
        });
    }

    private static onDeviceDelete() {
        SocketSingleton.getInstance().setupListener(Type.DeviceDeleteType, async (event, cb) => {
            try {
                await DBConnectionSingleton.getConn().deleteDeviceByID(event.payload.id);
                // TODO: Create an Manager for trigger action sensor etc for dynamic load and unload
            } catch (e) {
                cb(GateConnection.genDeletedResponse(false));
                return;
            }
            cb(GateConnection.genDeletedResponse(true));
            return;
        });
    }

    private static onDeviceCreate(database: Database) {
        SocketSingleton.getInstance().setupListener(Type.DeviceCreateType, async (event, cb) => {
            let deviceId = undefined;
            try {
                const data: DeviceCreate = event.payload;
                const device = new Device();
                device.id = uuidv4();
                device.name = data.name;
                device.typeId = data.typeId;
                deviceId = (await database.saveDevice(device)).id;

                if (event.payload.actions)
                    await EventHandler.createIOEntry(event.payload.actions, Action, deviceId);

                if (event.payload.sensors)
                    await EventHandler.createIOEntry(event.payload.sensors, Sensor, deviceId);

                if (event.payload.triggers)
                    await EventHandler.createIOEntry(event.payload.triggers, Trigger, deviceId);

                // await initializeIOEntriesFromDatabase();
            } catch (e) {
                console.error(e);
                cb(GateConnection.genCreationResponse());
                return;
            }
            cb(GateConnection.genCreationResponse(deviceId));
        });
    }

    private static MapKindToEntity(data: IOCreate | IOUpdate | IODelete) {
        let IOEntryType = undefined;
        switch (data.kind) {
            case Type.IOEntryKinds.Action: {
                IOEntryType = Action;
                break;
            }
            case Type.IOEntryKinds.Trigger: {
                IOEntryType = Trigger;
                break;
            }
            case Type.IOEntryKinds.Sensor: {
                IOEntryType = Sensor;
                break;
            }
        }
        return IOEntryType;
    }

    private static async createIOEntry(ioEntryData: any, ioEntryType: any, deviceId: string) {
        for (const currentIOEntryData of ioEntryData) {
            const current = new ioEntryType();
            current.device = await this.database.findDeviceById(deviceId);
            current.name = currentIOEntryData.name;
            current.config = currentIOEntryData.config;
            current.type = await this.database.findTypeById(currentIOEntryData.typeId);
            await this.database.saveEntity(current,ioEntryType);
        }
    }

    private static async findIOEntryByDeviceID(cb, IOEntry: any, deviceId) {
        try {
            const data = await this.database.findEntityByCondition({deviceId: deviceId}, IOEntry);
            cb(data);
        } catch (e) {
            cb('fail');
            return;
        }
    }

    private static async updateIOEntry(cb, IOEntry: any, payload) {
        try {
            const Io: any = await this.database.findEntityById(payload.id, IOEntry);
            Io.name = payload.name ? payload.name : Io.name;
            Io.config = payload.config ? payload.config : Io.config;
            Io.typeId = payload.typeId ? payload.typeId : Io.typeId;
            Io.deviceId = payload.deviceId ? payload.deviceId : Io.deviceId;
            this.database.saveEntity(Io, IOEntry);
        } catch (e) {
            cb(GateConnection.genUpdateResponse(false));
            return;
        }
        cb(GateConnection.genUpdateResponse(true));
        return;
    }

    private static async deleteIOEntry(cb, IOEntry: any, id) {
        try {
            await this.database.deleteEntity({id: id}, IOEntry);
        } catch (e) {
            cb(GateConnection.genDeletedResponse(false));
            return;
        }
        cb(GateConnection.genDeletedResponse(true));
        return;
    }

}