import { SaveOptions } from 'typeorm/repository/SaveOptions';
import { DeleteResult } from 'typeorm/query-builder/result/DeleteResult';
import { AppDataSource } from './database/data-source';
import { Device } from './database/entity/Device';
import { ActionAndSensorAndTriggerType } from './database/entity/ActionAndSensorAndTriggerType';
import { Trigger } from './database/entity/Trigger';
import { Action } from './database/entity/Action';
import { Sensor } from './database/entity/Sensor';

interface Database {

    findDeviceById(id: string);
    findTriggerById(id: number);
    findActionById(id: number);
    findSensorById(id: number);
    findTypeById(id: number);

    findActionsByOptionalDeviceId(deviceId?: string);
    findSensorsByOptionalDeviceId(deviceId?: string);
    findTriggersByOptionalDeviceId(deviceId?: string);

    findEntityById(id: any, entityType: any);
    findEntityByCondition(conidtion: any, entityType: any);
    findOneEntityByConditionOrFail(condition: any, entityType: any);

    healthcheck();

    saveDevice(device: Device, options?: SaveOptions);
    saveEntity(data: any, entityType: any);

    deleteDeviceByID(id: string);
    deleteEntity(data: any, entityType: any);
    deleteTriggerByDeviceId(deviceId: string);
    deleteActionByDeviceId(deviceId: string);
    deleteSensorByDeviceId(deviceId: string);

}

class TypeOrm implements Database {

    constructor() {
    }

    findEntityById(id: any, entityType: any): Promise<unknown> {
        return AppDataSource.getRepository(entityType).findOneByOrFail({ id });
    }

    findEntityByCondition(condition: any, entityType: any): Promise<unknown> {
        return AppDataSource.getRepository(entityType).find(condition);
    }

    findOneEntityByConditionOrFail(condition: any, entityType: any): Promise<unknown> {
        return AppDataSource.getRepository(entityType).findOneOrFail(condition);
    }

    findActionsByOptionalDeviceId(deviceId?: string): Promise<Action[]> {
        return AppDataSource.getRepository(Action).find(deviceId ? { where: { deviceId: deviceId } } : {});
    }

    findSensorsByOptionalDeviceId(deviceId?: string) {
        return AppDataSource.getRepository(Sensor).find(deviceId ? { where: { deviceId: deviceId } } : {});
    }

    findTriggersByOptionalDeviceId(deviceId?: string) {
        return AppDataSource.getRepository(Trigger).find(deviceId ? { where: { deviceId: deviceId } } : {});
    }

    findDeviceById(id: string): Promise<Device> {
        return AppDataSource.getRepository(Device).findOneByOrFail({ id });
    }

    findTriggerById(id: number): Promise<Trigger> {
        return AppDataSource.getRepository(Trigger).findOneByOrFail({ id });
    }

    findActionById(id: number): Promise<Action> {
        return AppDataSource.getRepository(Action).findOneByOrFail({ id });
    }

    findSensorById(id: number): Promise<Sensor> {
        return AppDataSource.getRepository(Sensor).findOneByOrFail({ id });
    }

    findTypeById(id: number): Promise<ActionAndSensorAndTriggerType> {
        return AppDataSource.getRepository(ActionAndSensorAndTriggerType).findOneByOrFail({ id });
    }

    healthcheck(): Promise<boolean> {
        return AppDataSource.query('select 1;');
    }

    public saveDevice(device: Device, options?: SaveOptions) {
        return AppDataSource.getRepository(Device).save(device, options);
    }

    saveEntity(data: any, entityType: any): Promise<(unknown)[]> {
        return AppDataSource.getRepository(entityType).save(data);
    }

    async deleteDeviceByID(id: string): Promise<DeleteResult> {
        await this.deleteActionByDeviceId(id);
        await this.deleteSensorByDeviceId(id);
        await this.deleteTriggerByDeviceId(id);
        return await AppDataSource.getRepository(Device).delete({ id });
    }

    deleteEntity(data: any, entityType: any): Promise<DeleteResult> {
        return AppDataSource.getRepository(entityType).delete(data);
    }

    deleteActionByDeviceId(deviceId: string): Promise<DeleteResult> {
        return AppDataSource.getRepository(Action).delete({ deviceId: deviceId });
    }

    deleteSensorByDeviceId(deviceId: string): Promise<DeleteResult> {
        return AppDataSource.getRepository(Sensor).delete({ deviceId: deviceId });
    }

    deleteTriggerByDeviceId(deviceId: string): Promise<DeleteResult> {
        return AppDataSource.getRepository(Trigger).delete({ deviceId: deviceId });
    }

}

export { Database, TypeOrm };