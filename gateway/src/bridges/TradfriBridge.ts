import {IBridge} from '../types';
import {TradfriClient} from 'node-tradfri-client';

export class TradfriBridge implements IBridge {

    private _onConnect: () => void;
    private _onDisconnect: () => void;
    private client: TradfriClient;

    constructor(
        private hostname: string,
        private identity: string,
        private psk: string,
        public id: number
    ) {
    }

    connect(): boolean {
        this.client = new TradfriClient(this.hostname);
        this.client.connect(this.identity, this.psk).then(() => this._onConnect());
        return false;
    }

    disconnect(): boolean {
        return false;
    }

    onDisconnected(onDisconnectedCallback: () => void): void {
        this._onDisconnect = onDisconnectedCallback;
    }

    onConnected(onConnectedCallback: () => void): void {
        this._onConnect = onConnectedCallback;
    }

}