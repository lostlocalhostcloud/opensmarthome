import { Database } from '../Database';

export class DBConnectionSingleton {
    private db: Database;
    private static instance: DBConnectionSingleton;

    constructor(db: Database) {
        this.db = db;
    }

    public static setConn(db: Database): void {
        this.instance = new DBConnectionSingleton(db);
    }

    public static getConn(): Database {
        // TODO: fail save
        return this.instance.db;
    }
}