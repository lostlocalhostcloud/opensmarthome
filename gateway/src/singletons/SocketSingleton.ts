import { io, Socket } from 'socket.io-client';
import { stopGateway } from '../index';
import { HealthcheckEvent, HealthcheckEventResponse } from '../events/HealthcheckEvent';
import { Events } from '../events/SocketEvent';

export class SocketSingleton {

    private static instance: SocketSingleton;

    private socket: Socket;

    private constructor() {
        this.createInstance();
    }

    public static getInstance(): SocketSingleton {
        if (!SocketSingleton.instance) {
            SocketSingleton.instance = new SocketSingleton();
        }

        return SocketSingleton.instance;
    }

    private createInstance(): void {
        if (!process.env.CLOUD_URL) {
            console.error('Required ENV CLOUD_URL is not set.');
            stopGateway(1);
        }
        this.socket = io(process.env.CLOUD_URL, process.env.CLOUD_KEY ? {
            extraHeaders: {
                Authorization: process.env.CLOUD_KEY
            }
        } : {});

        this.socket.on('connect_error', () => {
            // TODO: Less logs if not connected
            console.warn(`Cannot connect to ${process.env.CLOUD_URL}`);
        });

        setInterval(async () => {
            try {
                const response: HealthcheckEventResponse = await this.emitEvent(Events.HEALTHCHECK, new HealthcheckEvent(), true);
                if (response.status === 'ok') {
                    // TODO: Dont log always
                    // console.debug(`Successful healthcheck with Cloud ${process.env.CLOUD_URL}`);
                } else {
                    throw new Error('Healthcheck failed. Response is not expected.');
                }
            } catch (error) {
                console.error(error);
                console.info(`Socket connection status: ${this.socket.connected}`);
            }
        },  process.env.HEALTHCHECK_INTERVAL ? Number.parseInt(process.env.HEALTHCHECK_INTERVAL) : 60000);
    }

    public static deleteInstance(): void {
        if (SocketSingleton.instance) {
            SocketSingleton.getInstance().disconnect();
        }
    }

    private disconnect(): void {
        this.socket.disconnect();
    }

    public emitEvent(event: string, data: any, withResponse?: boolean): Promise<any> {
        const timeout = process.env.EVENT_TIMEOUT ? Number.parseInt(process.env.EVENT_TIMEOUT) : 10000;
        if (withResponse === true) {
            return new Promise((resolve, reject) => {
                this.socket.volatile.timeout(timeout).emit(event, data, (err, response) => {
                    if (err) {
                        reject(err);
                    }
                    resolve(response);
                });
            });
        }
        return new Promise((resolve, reject) => {
            this.socket.volatile.emit(event, data, (err) => {
                if (err) {
                    reject(err);
                }
                resolve(undefined);
            });
        });
    }

    public setupListener(event: string, onEvent: (data: any, callback: Function) => void): void {
        this.socket.on(event, (data, callback) => {
            onEvent(data, callback);
        });
    }

    public isCloudConnected(): boolean {
        return this.socket.connected;
    }

}