export enum DBTables {
    ACTION = 'action',
    ACTION_AND_SENSOR_AND_TRIGGER_TYPE = 'action_and_sensor_and_trigger_type',
    BRIDGE = 'bridge',
    BRIDGE_TYPE = 'bridge_type',
    DEVICE_TYPE = 'device_type',
    DEVICE = 'device',
    SENSOR = 'sensor',
    TRIGGER = 'trigger'
}