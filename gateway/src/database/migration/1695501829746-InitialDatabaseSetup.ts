import { MigrationInterface, QueryRunner, Table, TableColumnOptions, TableForeignKey } from 'typeorm';
import { DBTables } from '../DBTables';

export class InitialDatabaseSetup1695501829746 implements MigrationInterface {

    private static ACTION_AND_SENSOR_AND_TRIGGER_COLUMNS: TableColumnOptions[] = [
        {
            name: 'id',
            type: 'integer',
            isPrimary: true,
            isGenerated: true,
            generationStrategy: 'increment'
        },
        {
            name: 'name',
            type: 'varchar'
        },
        {
            name: 'device_id',
            type: 'blob'
        },
        {
            name: 'config',
            type: 'text'
        },
        {
            name: 'type_id',
            type: 'integer'
        }
    ];

    public async up(queryRunner: QueryRunner): Promise<void> {
        console.info('MIGRATION: InitialDatabaseSetup started.');
        await queryRunner.createTable(
            new Table({
                name: DBTables.BRIDGE_TYPE,
                columns: [
                    {
                        name: 'id',
                        type: 'integer',
                        isPrimary: true,
                        isGenerated: true,
                        generationStrategy: 'increment'
                    },
                    {
                        name: 'name',
                        type: 'varchar'
                    }
                ]
            })
        );
        await queryRunner.createTable(
            new Table({
                name: DBTables.BRIDGE,
                columns: [
                    {
                        name: 'id',
                        type: 'integer',
                        isPrimary: true,
                        isGenerated: true,
                        generationStrategy: 'increment'
                    },
                    {
                        name: 'type_id',
                        type: 'integer'
                    },
                    {
                        name: 'name',
                        type: 'varchar'
                    },
                    {
                        name: 'host',
                        type: 'varchar'
                    },
                    {
                        name: 'credentials',
                        type: 'varchar'
                    },
                    {
                        name: 'config',
                        type: 'varchar'
                    }
                ]
            })
        );
        await queryRunner.createTable(
            new Table({
                name: DBTables.ACTION_AND_SENSOR_AND_TRIGGER_TYPE,
                columns: [
                    {
                        name: 'id',
                        type: 'integer',
                        isPrimary: true,
                        isGenerated: true,
                        generationStrategy: 'increment'
                    },
                    {
                        name: 'name',
                        type: 'varchar'
                    }
                ]
            })
        );
        await queryRunner.createTable(
            new Table({
                name: DBTables.DEVICE_TYPE,
                columns: [
                    {
                        name: 'id',
                        type: 'blob',
                        isPrimary: true,
                        isGenerated: true,
                    },
                    {
                        name: 'name',
                        type: 'varchar',
                    }
                ]
            })
        );
        await queryRunner.createTable(
            new Table({
                name: DBTables.DEVICE,
                columns: [
                    {
                        name: 'id',
                        type: 'blob',
                        isPrimary: true
                    },
                    {
                        name: 'name',
                        type: 'varchar'
                    },
                    {
                        name: 'type_id',
                        type: 'integer'
                    }
                ]
            })
        );
        await queryRunner.createTable(
            new Table({
                name: DBTables.ACTION,
                columns: InitialDatabaseSetup1695501829746.ACTION_AND_SENSOR_AND_TRIGGER_COLUMNS
            })
        );
        await queryRunner.createTable(
            new Table({
                name: DBTables.SENSOR,
                columns: InitialDatabaseSetup1695501829746.ACTION_AND_SENSOR_AND_TRIGGER_COLUMNS
            })
        );
        await queryRunner.createTable(
            new Table({
                name: DBTables.TRIGGER,
                columns: InitialDatabaseSetup1695501829746.ACTION_AND_SENSOR_AND_TRIGGER_COLUMNS
            })
        );
        await queryRunner.createForeignKeys(DBTables.BRIDGE, [
            new TableForeignKey({
                columnNames: ['type_id'],
                referencedColumnNames: ['id'],
                referencedTableName: DBTables.BRIDGE_TYPE
            })
        ]);
        await queryRunner.createForeignKeys(DBTables.DEVICE, [
            new TableForeignKey({
                columnNames: ['type_id'],
                referencedColumnNames: ['id'],
                referencedTableName: DBTables.DEVICE_TYPE
            })
        ]);
        await queryRunner.createForeignKeys(DBTables.ACTION, [
            new TableForeignKey({
            columnNames: ['device_id'],
            referencedColumnNames: ['id'],
            referencedTableName: DBTables.DEVICE
            }),
            new TableForeignKey({
                columnNames: ['type_id'],
                referencedColumnNames: ['id'],
                referencedTableName: DBTables.ACTION_AND_SENSOR_AND_TRIGGER_TYPE
            })
        ]);
        await queryRunner.createForeignKeys(DBTables.SENSOR, [
            new TableForeignKey({
                columnNames: ['device_id'],
                referencedColumnNames: ['id'],
                referencedTableName: DBTables.DEVICE
            }),
            new TableForeignKey({
                columnNames: ['type_id'],
                referencedColumnNames: ['id'],
                referencedTableName: DBTables.ACTION_AND_SENSOR_AND_TRIGGER_TYPE
            })
        ]);
        await queryRunner.createForeignKeys(DBTables.TRIGGER, [
            new TableForeignKey({
                columnNames: ['device_id'],
                referencedColumnNames: ['id'],
                referencedTableName: DBTables.DEVICE
            }),
            new TableForeignKey({
                columnNames: ['type_id'],
                referencedColumnNames: ['id'],
                referencedTableName: DBTables.ACTION_AND_SENSOR_AND_TRIGGER_TYPE
            })
        ]);
        await queryRunner.query('INSERT INTO device_type (id, name) VALUES (1, \'Switch\'), (2, \'Garagedoor\')');
        await queryRunner.query('INSERT INTO action_and_sensor_and_trigger_type (id, name) VALUES (1, \'cloud\'), (2, \'gpio\'), (3, \'http\');');
        console.info('MIGRATION: InitialDatabaseSetup finished.');
    }

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
