import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { BridgeType } from './BridgeType';

@Entity()
export class Bridge {

    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(() => BridgeType, { nullable: false })
    @JoinColumn()
    type: BridgeType;

    @Column('varchar',{ nullable: false, unique: true })
    name: string;

    @Column('varchar',{ nullable: false })
    host: string;

    @Column({ type: 'json', nullable: false })
    credentials: JSON;

    @Column({ type: 'json', nullable: false })
    config: JSON;

}