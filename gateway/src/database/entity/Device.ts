import { Column, Entity, JoinColumn, ManyToOne, PrimaryColumn } from 'typeorm';
import { DBTables } from '../DBTables';
import { DeviceType } from './DeviceType';

@Entity(DBTables.DEVICE)
export class Device {

    @PrimaryColumn('uuid')
    id: string;

    @Column('varchar',{ nullable: false })
    name: string;

    @Column('int', { name: 'type_id' })
    typeId: number;

    @ManyToOne(() => DeviceType, { nullable: false, eager: true })
    @JoinColumn({ name: 'type_id' })
    type: DeviceType;

}
