import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class BridgeType {

    @PrimaryGeneratedColumn()
    id: number;

    @Column('varchar', { nullable: false })
    name: string;

}
