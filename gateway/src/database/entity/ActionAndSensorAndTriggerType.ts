import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { DBTables } from '../DBTables';

@Entity(DBTables.ACTION_AND_SENSOR_AND_TRIGGER_TYPE)
export class ActionAndSensorAndTriggerType {

    @PrimaryGeneratedColumn()
    id: number;

    @Column('varchar', { nullable: false })
    name: string;

}
