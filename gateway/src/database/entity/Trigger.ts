import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { ActionAndSensorAndTriggerType } from './ActionAndSensorAndTriggerType';
import { Device } from './Device';

@Entity()
export class Trigger {

    @PrimaryGeneratedColumn()
    id: number;

    @Column('varchar', { nullable: false })
    name: string;

    @Column('uuid', { name: 'device_id' })
    deviceId: string;

    @ManyToOne(() => Device, { nullable: false, eager: true })
    @JoinColumn({ name: 'device_id' })
    device: Device;

    @ManyToOne(() => ActionAndSensorAndTriggerType, { nullable: false, eager: true })
    @JoinColumn({ name: 'type_id' })
    type: ActionAndSensorAndTriggerType;

    @Column({ type: 'json' })
    config: JSON;

}
