import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Device } from './Device';
import { ActionAndSensorAndTriggerType } from './ActionAndSensorAndTriggerType';
import { DBTables } from '../DBTables';

@Entity(DBTables.ACTION)
export class Action {

    @PrimaryGeneratedColumn()
    id: number;

    @Column('varchar', { nullable: false })
    name: string;

    @Column('uuid',{ name: 'device_id' })
    deviceId: string;

    @ManyToOne(() => Device, { nullable: false, eager: true })
    @JoinColumn({ name: 'device_id' })
    device: Device;

    @ManyToOne(() => ActionAndSensorAndTriggerType, { nullable: false, eager: true })
    @JoinColumn({ name: 'type_id' })
    type: ActionAndSensorAndTriggerType;

    @Column({ type: 'json', nullable: false })
    config: JSON;

}
