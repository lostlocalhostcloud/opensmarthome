import 'reflect-metadata';
import { DataSource } from 'typeorm';

export const AppDataSource = new DataSource({
    type: 'sqlite',
    database: 'osh-gateway.sqlite',
    synchronize: false,
    logging: false,
    entities: ['./src/database/entity/**/*.ts', './database/entity/**/*.js'],
    migrations: ['./src/database/migration/**/*.ts', './database/migration/**/*.js'],
    migrationsRun: true,
    subscribers: [],
});
