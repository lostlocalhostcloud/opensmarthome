import { TradfriBridge } from './bridges/TradfriBridge';

interface IHashActions {
    [action: string]: IAction[];
}

interface IHashSensors {
    [sensor: string]: ISensor<any>[];
}

interface IHashTradfriBridge {
    [bridgeId: number]: TradfriBridge;
}

interface IHashCookies {
    [key: string] : string;
}

interface IHashString {
    [key: string] : string;
}

interface IAction {
    setup(): boolean;

    execute(): boolean;
}

interface ITrigger {
    setup(onEvent: (deviceId: string) => void): boolean;
}

interface ISensor<T> {
    setup(onEvent: (deviceId: string, data: T) => void): boolean;

    read(): T;
}

interface IBridge {

    id: number

    onDisconnected(onDisconnectedCallback: () => void): void;

    onConnected(onConnectedCallback: () => void): void

    connect(): boolean;

    disconnect(): boolean;

}

abstract class TradfriDevice {
    private bridgeId: number;
}

class TradfriBridgesSingleton {
    private tradfriBridges: IHashTradfriBridge = [];
    private static instance: TradfriBridgesSingleton;

    public static getTradfriBridgeById(id: number): TradfriBridge {
        return this.instance.tradfriBridges[id];
    }

    public static AddTradfriBridgeById(bridge: TradfriBridge): TradfriBridge {
        return this.instance.tradfriBridges[bridge.id] = bridge;
    }
}

export {TradfriDevice, IBridge, IHashSensors,
    ISensor, ITrigger, IAction, IHashActions, IHashCookies, IHashString};
