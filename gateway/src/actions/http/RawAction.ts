import { HttpAction } from '../HttpAction';
import { Headers } from 'node-fetch';
import { IHashCookies } from '../../types';

export class RawAction extends HttpAction {



    constructor(
        private _deviceId: string,
        private _url: string,
        private _headers: Headers,
        private _method: string,
        private _cookies: IHashCookies,
        private _redirect: boolean,
        private rawBody: ArrayBuffer | ArrayBufferView | NodeJS.ReadableStream | string | undefined,
        private contentType: string,
    ) {
        super(_deviceId, _url, _headers, _method, _cookies,_redirect);
        if(!this.headers){
            this.headers = new Headers();
        }
    }

    setup(): boolean {
        if (super.setup()) {
            this.appendToHeaders('Content-Type', this.contentType);

            if (this.rawBody){
                // FIXME: Broken because of dependency update
                // super.options.body = this.rawBody;
            }
            return true;
        }
        return false;
    }

    execute(): boolean {
        this.setup();
        return super.execute();
    }
}