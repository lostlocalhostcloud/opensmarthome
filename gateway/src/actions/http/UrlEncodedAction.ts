import { HttpAction } from '../HttpAction';
import { Headers } from 'node-fetch';
import { IHashCookies, IHashString } from '../../types';
import { URLSearchParams } from 'url';


export class UrlEncodedAction extends HttpAction {

    constructor(
        private _deviceId: string,
        private _url: string,
        private _headers: Headers,
        private _method: string,
        private _cookies: IHashCookies,
        private _redirect: boolean,
        private fromUrlencoded: IHashString,
    ) {
        super(_deviceId, _url, _headers, _method, _cookies,_redirect);
    }

    setup(): boolean {
        if (super.setup()) {
            this.appendToHeaders('Content-Type', 'application/x-www-form-urlencoded');
            const urlencoded = new URLSearchParams();

            for (const formKey in this.fromUrlencoded) {
                urlencoded.append(formKey, this.fromUrlencoded[formKey]);
            }
            this.options.body = urlencoded;
            return true;
        }
        return false;
    }

    execute(): boolean {
        this.setup();
        return super.execute();
    }
}