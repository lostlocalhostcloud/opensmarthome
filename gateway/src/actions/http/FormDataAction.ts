import { HttpAction } from '../HttpAction';
import { Headers } from 'node-fetch';
import { IHashCookies, IHashString } from '../../types';
import FormData from 'form-data';

export class FormDataAction extends HttpAction {

    constructor(
        private _deviceId: string,
        private _url: string,
        private _headers: Headers,
        private _method: string,
        private _cookies: IHashCookies,
        private _redirect: boolean,
        private Form: IHashString
    ) {
        super(_deviceId, _url, _headers, _method, _cookies,_redirect);
        if(!this.headers){
            this.headers = new Headers();
        }
    }

    setup(): boolean {
        if (super.setup()) {
            const formData = new FormData();

            for (const formKey in this.Form) {
                formData.append(formKey, this.Form[formKey]);
            }
            this.options.body = formData;
            return true;
        }
        return false;
    }

    execute(): boolean {
        this.setup();
        return super.execute();
    }
}