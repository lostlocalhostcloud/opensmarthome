import { IAction, IHashCookies } from '../types';
import fetch, { Headers, RequestInit } from 'node-fetch';

export abstract class HttpAction implements IAction {

    protected options: RequestInit;

    constructor(
        private deviceId: string,
        private url: string,
        protected headers: Headers,
        private method: string,
        private cookies: IHashCookies,
        private redirect: boolean,
    ) {
        if (!this.headers) {
            this.headers = new Headers();
        }
    }

    execute(): boolean {
        console.log('HttpAction on Device:' + this.deviceId);
        this.fetch();
        return true;
    }

    setup(): boolean {
        if (this.cookies && Object.keys(this.cookies).length > 0) {
            let cookiesHeaderValue = '';
            for (const cookieKey of Object.keys(this.cookies)) {
                cookiesHeaderValue += cookieKey + '=' + this.cookies[cookieKey] + '; ';
            }
            if (cookiesHeaderValue.length > 4) {
                this.headers.append('Cookie', cookiesHeaderValue);
            } else {
                console.error('DeviceId:' + this.deviceId + '; setup HTTP Action failed: because of invalid cookie');
                return false;
            }
        }


        this.options = {
            headers: this.headers,
            method: this.method,
            redirect: this.redirect ? 'follow' : 'error'
        };
        return true;
    }

    public fetch() {
        return fetch(this.url, this.options).catch(error => console.error('DeviceId:' + this.deviceId + '; HTTP Action failed:', error));
    }

    protected appendToHeaders(key: string, value: string) {
        if (this.options.headers instanceof Headers) {
            this.options.headers.append(key, value);
        }
    }
}