import { IAction } from '../types';
import { DisabledGpio, IGpio, EnabledGpio } from '../helpers/GPIOHelper';

export class GPIOAction implements IAction {

    private action: IGpio;
    private state = 0;

    constructor(
        private gpioEnabled: boolean,
        private deviceId: string,
        private pin: number,
        private inverted: boolean,
        private stateType: string,
    ) {
    }

    execute(): boolean {
        console.log('GPIOAction on pin: ' + this.pin);
        switch (this.stateType) {
            case 'stateful':
                switch (this.state) {
                    case 0:
                        this.action.write(this.inverted ? 0 : 1);
                        this.state = 1;
                        break;
                    case 1:
                        this.action.write(this.inverted ? 1 : 0);
                        this.state = 0;
                        break;
                    default:
                        return false;
                }
                break;
            case 'stateless':
                this.action.write(this.inverted ? 0 : 1);
                setTimeout(() => {
                    this.action.write(this.inverted ? 1 : 0);
                }, 500);
                break;
            default:
                return false;
        }
        return true;
    }

    setup(): boolean {
        this.action = this.gpioEnabled ? new EnabledGpio(this.pin, this.inverted ? 'high' : 'out') : new DisabledGpio();
        console.log('Setup pin: ' + this.pin);
        return true;
    }

}