import { Event, IOCreate, Meta, Type } from './protocol';
// @ts-ignore
import { Emitter } from 'component-emitter';

export class GateConnection {

    constructor(private socket: Emitter,
                private gatewayId: string,
                private smarthomeId: string) {

    }

    public static genCreationResponse(id?: string): Event {
        return {
            _meta: GateConnection.genMeta(Type.StatusResponse),
            payload: {
                status: id ? Type.Status.OK : Type.Status.FAIL,
                id
            }
        };
    }

    public static genDeletedResponse(deleted: boolean): Event {
        return {
            _meta: GateConnection.genMeta(Type.StatusResponse),
            payload: {
                status: deleted ? Type.Status.OK : Type.Status.FAIL,
            }
        };
    }

    public genDataResponse(deleted: boolean): Event {
        return {
            _meta: GateConnection.genMeta(Type.StatusResponse),
            payload: {
                status: deleted ? Type.Status.OK : Type.Status.FAIL,
            }
        };
    }

    public static genUpdateResponse(update: boolean): Event {
        return {
            _meta: GateConnection.genMeta(Type.StatusResponse),
            payload: {
                status: update ? Type.Status.OK : Type.Status.FAIL,
            }
        };
    }

    public verifyEvent(event: Event, type: string) : boolean {
        const expect = GateConnection.genMeta(type);
        if (event._meta) {
            if (event._meta.type == expect.type && event._meta.api_version == expect.api_version){
                return true;
            }
            return  false;
        }
        return false;
    }

    public deleteDeviceTrigger(id: string): Promise<Event> {
        return this.deleteIOEntry(id);
    }

    public editDeviceTrigger(id: number, name?: string, config?: any, typeId?: number): Promise<Event> {
        return this.updateIOEntry(id, name, config, typeId);
    }

    public addDeviceTrigger(name: string, config: any, typeId: number, deviceId: string): Promise<Event> {
        return this.createIOEntry(name, config, typeId, deviceId);
    }

    public deleteDeviceSensor(id: string): Promise<Event> {
        return this.deleteIOEntry(id);
    }

    public editDeviceSensor(id: number, name: string, config: any, typeId: number): Promise<Event> {
        return this.updateIOEntry(id, name, config, typeId);
    }

    public addDeviceSensor(name: string, config: any, typeId: number, deviceId: string): Promise<Event> {
        return this.createIOEntry(name, config, typeId, deviceId);
    }

    public deleteDeviceAction(id: string): Promise<Event> {
        return this.deleteIOEntry(id);
    }

    public editDeviceAction(id: number, name: string, config: any, typeId: number): Promise<Event> {
        return this.updateIOEntry(id, name, config, typeId);
    }

    public addDeviceAction(name: string, config: any, typeId: number, deviceId: string): Promise<Event> {
        return this.createIOEntry(name, config, typeId, deviceId);
    }

    public addDevice(name: string, typeId: number,
                     actions?: IOCreate[], sensors?: IOCreate[], triggers?: IOCreate[]): Promise<Event> {

        const promise = new Promise<Event>((resolve, reject) => {
            const type = Type.DeviceDeleteType;
            return this.socket.emit(type,{
                _meta: GateConnection.genMeta(type),
                payload: {
                    name,
                    typeId,
                    actions,
                    sensors,
                    triggers
                }
            },
                this.convertData(resolve, reject)
            );
        });
        return promise;
    }

    public editDevice(id: string, name: string, typeId: number): Promise<Event> {
        const promise = new Promise<Event>((resolve, reject) => {
            const type = Type.DeviceDeleteType;
            this.socket.emit(type,{
                _meta: GateConnection.genMeta(type),
                payload: {
                    id,
                    name,
                    typeId
                }
            },
                this.convertData(resolve, reject)
            );
        });
        return promise;
    }

    public deleteDevice(id: string): Promise<Event> {
        const promise = new Promise<Event>((resolve, reject) => {
            const type = Type.DeviceDeleteType;
            this.socket.emit(type,{
                _meta: GateConnection.genMeta(type),
                payload: {
                    id,
                }
            },
                this.convertData(resolve, reject)
            );
        });

        return promise;
    }

    private static genMeta(type: string): Meta {
        return {
            type: type,
            api_version: 'v1'
        };
    }

    private updateIOEntry(id: number, name: string, config: any, typeId: number): Promise<Event> {
        const promise = new Promise<Event>((resolve, reject) => {
            const type = Type.IOUpdateType;
            return this.socket.emit(type, {
                _meta: GateConnection.genMeta(type),
                payload: {
                    id,
                    name,
                    config,
                    typeId
                }
            },
                this.convertData(resolve, reject)
            );
        });
        return promise;
    }

    private createIOEntry(name: string, config: string, typeId: number, deviceId: string): Promise<Event> {
        const promise = new Promise<Event>((resolve, reject) => {
            const type = Type.IOCreateType;
            this.socket.emit(type, {
                _meta: GateConnection.genMeta(type),
                payload: {
                    name,
                    config,
                    typeId,
                    deviceId
                }
            },
                this.convertData(resolve, reject)
            );
        });
        return promise;
    }

    private deleteIOEntry(id: string,): Promise<Event> {
        const promise = new Promise<Event>(((resolve, reject) => {
            const type = Type.IOUpdateType;
            this.socket.emit(type, {
                _meta: GateConnection.genMeta(type),
                payload: {
                    id
                }
            },
                this.convertData(resolve, reject)
            );
        }));
        return promise;

    }

    private getDevices(IOEntryKind: string): Promise<Event> {
        const promise = new Promise<Event>((resolve, reject) => {
            const type = Type.DataRequest;
            this.emitEvent(type,{
                _meta: GateConnection.genMeta(type),
                payload: {
                    kind: IOEntryKind,
                }
            },
                this.convertData(resolve, reject)
            );
        });
        return promise;
    }

    private getIOEntries(IOEntryKind: string, deviceId: string): Promise<Event> {
        const promise = new Promise<Event>((resolve, reject) => {
            const type = Type.DataRequest;
            this.emitEvent(type,{
                    _meta: GateConnection.genMeta(type),
                    payload: {
                        kind: IOEntryKind,
                        deviceId,
                    }
                },
                this.convertData(resolve, reject)
            );
        });
        return promise;
    }

    private convertData(resolve: any, reject: any) {
        return (dataRaw) => {
            try {
                const data: Event = dataRaw as Event;
                resolve(data);
            } catch (e) {
                reject(e);
            }
        };
    }

    public emitEvent(event: string, data: any, callback?: any): Emitter<string> {
        if(callback) {
            return this.socket.emit(event, data, callback);
        }
        return this.socket.emit(event, data);
    }

    public on(event: string, callback: (event, cb) => Promise<void>): this {

        const verifyHook = (event_data, cb) => {
            if (this.verifyEvent(event_data, event)) {
                callback(event_data, cb);
            } else {
                console.error('malformed event data or api version incompatible:\n');
                console.error(event_data);
            }
        };

        this.socket.on(event, verifyHook);
        return this;
    }

}