import { BinaryValue, Direction, Edge, Gpio, Options, ValueCallback } from 'onoff';

interface IGpio {

    readSync(): BinaryValue;
    write(value: BinaryValue, callback: (err: Error | null | undefined) => void): void;
    write(value: BinaryValue): Promise<void>;
    writeSync(value: BinaryValue): void;
    watch(callback: ValueCallback): void;

}

class EnabledGpio implements IGpio {

    gpio: Gpio;

    constructor(gpio: number, direction: Direction, edge?: Edge, options?: Options) {
        this.gpio = new Gpio(gpio, direction, edge, options);
    }

    readSync(): BinaryValue {
        return this.gpio.readSync();
    }

    write(value: BinaryValue, callback: (err: (Error | null | undefined)) => void): void;

    write(value: BinaryValue): Promise<void>;

    write(value: BinaryValue, callback?: (err: (Error | null | undefined)) => void): void | Promise<void> {
        return this.gpio.write(value, callback);
    }

    writeSync(value: BinaryValue): void {
        return this.gpio.writeSync(value);
    }

    watch(callback: ValueCallback): void {
        return this.gpio.watch(callback);
    }

}

class DisabledGpio implements IGpio {

    readSync(): BinaryValue {
        return null;
    }

    write(value: BinaryValue, callback: (err: (Error | null | undefined)) => void): void;

    write(value: BinaryValue): Promise<void>;

    write(value: BinaryValue, callback?: (err: (Error | null | undefined)) => void): void | Promise<void> {
        return new Promise((resolve, reject) => resolve());
    }

    writeSync(value: BinaryValue): void {
        return null;
    }

    watch(callback: ValueCallback): void {
        return null;
    }

}

export { IGpio, EnabledGpio, DisabledGpio };