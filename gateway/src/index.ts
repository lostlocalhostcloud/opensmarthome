import 'reflect-metadata';
import { Device } from './database/entity/Device';
import { Trigger } from './database/entity/Trigger';
import { Action } from './database/entity/Action';
import { actionFactory, sensorFactory, triggerFactory } from './MegaFactory';
import { IHashActions, IHashSensors, ITrigger } from './types';
import { Sensor } from './database/entity/Sensor';
import { TypeOrm } from './Database';
import { EventHandler } from './socketevents/EventHandler';
import { Type } from './gateprotocol/protocol';
import { health } from './endpoints/Health';
import { AppDataSource } from './database/data-source';
import { DBConnectionSingleton } from './singletons/DBConnectionSingleton';
import { SocketSingleton } from './singletons/SocketSingleton';
import { SensorEvent } from './events/SensorEvent';
import { SensorStatesEvent, SensorStatesEventResponse } from './events/SensorStatesEvent';
import { versionEndpoint } from './endpoints/VersionEndpoint';

require('log-timestamp');
// eslint-disable-next-line @typescript-eslint/no-var-requires
require('dotenv').config();
const app = require('express')();

let hashActions: IHashActions = {};

let hashSensors: IHashSensors = {};

let triggers: ITrigger[] = [];

app.use('/health', health);
app.use('/version', versionEndpoint);

const port = process.env.PORT ? process.env.PORT : 3001;

app.listen(port, () => {
    return console.log(`server is listening on ${port}`);
});

export function stopGateway(exitCode?: number): void {
    SocketSingleton.deleteInstance();
    process.exit(exitCode ? exitCode : 0);
}

async function initializeIOEntriesFromDatabase(deviceId?: string) {
    if (!deviceId) {
        hashActions = {};
        hashSensors = {};
        triggers = [];
    }

    (await DBConnectionSingleton.getConn().findActionsByOptionalDeviceId(deviceId)).forEach((a) => {
        hashActions[a.device.id] = [];
        const action = actionFactory(a);
        action.setup();
        hashActions[a.device.id].push(action);
    });

    (await DBConnectionSingleton.getConn().findSensorsByOptionalDeviceId(deviceId)).forEach((s) => {
        hashSensors[s.device.id] = [];
        const sensor = sensorFactory(s);
        sensor.setup((deviceId, data) => {
            console.log(`Event for device: ${deviceId} with data: ${data}`);
            SocketSingleton.getInstance().emitEvent(Type.SensorEvent, new SensorEvent(deviceId, data));
        });
        hashSensors[s.device.id].push(sensor);
    });

    (await DBConnectionSingleton.getConn().findTriggersByOptionalDeviceId(deviceId)).forEach((t) => {
        const trigger = triggerFactory(t, app);
        trigger.setup(((deviceId) => {
            hashActions[deviceId].forEach(a => a.execute());
        }));
        triggers.push(trigger);
    });

    SocketSingleton.getInstance().setupListener(Type.SensorStates, async (event: SensorStatesEvent, cb) => {
        const devices = [];
        try {
            event.deviceIds.forEach((deviceId) => {
                if (hashSensors[deviceId] != undefined) {
                    hashSensors[deviceId].forEach((sensor) => {
                        devices.push({ id: deviceId, sensorState: sensor.read() });
                    });
                }
            });
        } catch (e) {
            console.error(e);
            cb(new SensorStatesEventResponse([], Type.Status.FAIL));
            return;
        }
        cb(new SensorStatesEventResponse(devices, Type.Status.OK));
    });
}

AppDataSource.initialize().then(async () => {
    await AppDataSource.query('UPDATE SQLITE_SEQUENCE SET SEQ=(SELECT MAX(id) from action) WHERE NAME=\'action\';');
    await AppDataSource.query('UPDATE SQLITE_SEQUENCE SET SEQ=(SELECT MAX(id) from action_and_sensor_and_trigger_type) WHERE NAME=\'action_and_sensor_and_trigger_type\';');
    await AppDataSource.query('UPDATE SQLITE_SEQUENCE SET SEQ=(SELECT MAX(id) from bridge) WHERE NAME=\'bridge\';');
    await AppDataSource.query('UPDATE SQLITE_SEQUENCE SET SEQ=(SELECT MAX(id) from bridge_type) WHERE NAME=\'bridge_type\';');
    await AppDataSource.query('UPDATE SQLITE_SEQUENCE SET SEQ=(SELECT MAX(id) from device_type) WHERE NAME=\'device_type\';');
    await AppDataSource.query('UPDATE SQLITE_SEQUENCE SET SEQ=(SELECT MAX(id) from sensor) WHERE NAME=\'sensor\';');
    await AppDataSource.query('UPDATE SQLITE_SEQUENCE SET SEQ=(SELECT MAX(id) from trigger) WHERE NAME=\'trigger\';');

    const deviceRepository = AppDataSource.getRepository(Device);
    const triggerRepository = AppDataSource.getRepository(Trigger);
    const actionRepository = AppDataSource.getRepository(Action);
    const sensorRepository = AppDataSource.getRepository(Sensor);
    DBConnectionSingleton.setConn(new TypeOrm());

    await initializeIOEntriesFromDatabase();

    EventHandler.initEventHandler(DBConnectionSingleton.getConn());

    SocketSingleton.getInstance().setupListener('deleteDevice', async (event, cb) => {
        console.log(event);
        try {
            await actionRepository.delete({deviceId: event});
            await sensorRepository.delete({deviceId: event});
            await triggerRepository.delete({deviceId: event});
            await deviceRepository.delete({ id: event });
            // TODO: Create an Manager for trigger action sensor etc for dynamic load and unload
            await initializeIOEntriesFromDatabase();
        } catch (e) {
            cb('fail');
            return;
        }
        cb('OK');
        return;
    });

    SocketSingleton.getInstance().setupListener('updateDevice', async (event, cb) => {
        console.log(event);
        try {
            const device = await deviceRepository.findOneByOrFail({ id: event.id });
            device.name = event.name;
            device.typeId = event.typeId;
            deviceRepository.save(device);
        } catch (e) {
            cb({status: 'fail'});
            return;
        }
        cb({status: 'ok'});
        return;
    });

    SocketSingleton.getInstance().setupListener('getDeviceTrigger', async (event, cb) => {
        console.log(event);
        await findIoEntryByDeviceID(cb, Trigger, event);
    });

    async function findIoEntryByDeviceID(cb, IOEntry: any, event) {
        try {
            const data = await AppDataSource.getRepository(IOEntry).findBy({deviceId: event});
            cb(data);
        } catch (e) {
            cb('fail');
            return;
        }
    }

    SocketSingleton.getInstance().setupListener('getDeviceTrigger', async (event, cb) => {
        console.log(event);
        cb(await findIoEntryByDeviceID(cb, Trigger, event));
    });

    SocketSingleton.getInstance().setupListener('getDeviceAction', async (event, cb) => {
        console.log(event);
        await findIoEntryByDeviceID(cb, Action, event);
    });

    SocketSingleton.getInstance().setupListener('getDeviceSensor', async (event, cb) => {
        console.log(event);
        await findIoEntryByDeviceID(cb, Sensor, event);
    });

    SocketSingleton.getInstance().setupListener('editDeviceSensor', async (event, cb) => {
        console.log(event);
        await updateIOEntry(cb, Sensor, event);
    });

    SocketSingleton.getInstance().setupListener('editDeviceAction', async (event, cb) => {
        console.log(event);
        await updateIOEntry(cb, Action, event);
    });

    SocketSingleton.getInstance().setupListener('editDeviceTrigger', async (event, cb) => {
        console.log(event);
        await updateIOEntry(cb, Trigger, event);
    });

    SocketSingleton.getInstance().setupListener('deleteDeviceSensor', async (event, cb) => {
        console.log(event);
        await deleteIOEntry(cb, Sensor, event);
    });

    SocketSingleton.getInstance().setupListener('deleteDeviceAction', async (event, cb) => {
        console.log(event);
        await deleteIOEntry(cb, Action, event);
    });

    SocketSingleton.getInstance().setupListener('deleteDeviceTrigger', async (event, cb) => {
        console.log(event);
        await deleteIOEntry(cb, Trigger, event);
    });

    async function updateIOEntry(cb, IOEntry: any, event) {
        try {
            const IORepo = AppDataSource.getRepository(IOEntry);
            const Io: any = await IORepo.findOneByOrFail({id: event.id});
            Io.name = event.name ? event.name : Io.name;
            Io.config = event.config ? event.config : Io.config;
            Io.typeId = event.typeId ? event.typeId : Io.typeId;
            Io.deviceId = event.deviceId ? event.deviceId : Io.deviceId;
            cb( await IORepo.save(Io));
        } catch (e) {
            cb('fail');
            return;
        }
    }

    async function deleteIOEntry(cb, IOEntry: any, event) {
        try {
            const IORepo = AppDataSource.getRepository(IOEntry);
            const Io: any = await IORepo.delete({id: event});
        } catch (e) {
            cb('fail');
            return;
        }
        cb('ok');
    }

    const bodyParser = require('body-parser');
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));

    app.get('/', (req, res) => {
        res.status(200).send('gateway is working!');
    });
}).catch(error => {
    console.error(error);
    stopGateway(1);
});
