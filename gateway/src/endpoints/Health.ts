import { Router } from 'express';
import { SocketSingleton } from '../singletons/SocketSingleton';
import { DBConnectionSingleton } from '../singletons/DBConnectionSingleton';

interface HealthStatus {
    name: string;
    status: string;
    error?: {
        code: number;
        message: string;
    }
}

export const health = Router();

health.get('/', async (req, res) => {
    const cloudHealth = checkCloudConnection();
    try {
        const databaseHealth = await checkDatabase();

        if (!databaseHealth.error && !cloudHealth.status.includes('not')) {
            return res.status(200).json({
                status: 'all systems up'
            });
        }
        return res.status(500).json({
            status: 'not all systems up',
            services: [databaseHealth, cloudHealth]
        });
    } catch (error) {
        return res.status(500).json({
            status: 'not all systems up',
            services: [error, cloudHealth]
        });
    }
});

health.get('/database', (req, res) => {
    checkDatabase().then((databaseHealth) => {
        return res.status(200).json({
            status: databaseHealth.status
        });
    }).catch((databaseHealthError) => {
        return res.status(500).json({
            status: databaseHealthError.status,
            error: {
                code: databaseHealthError.error.code,
                message: databaseHealthError.error.message
            }
        });
    });
});

health.get('/cloud', (req, res) => {
    const cloudHealth = checkCloudConnection();

    if (!cloudHealth.status.includes('not')) {
        return res.status(200).json({
            status: cloudHealth.status
        });
    }
    return res.status(500).json({
        status: cloudHealth.status
    });
});

function checkDatabase(): Promise<HealthStatus> {
    return new Promise((resolve, reject) => {
        DBConnectionSingleton.getConn().healthcheck().then(() => resolve({
            name: 'Database',
            status: 'database connection Ok'
        })).catch((error) => {
            console.error(error);
            reject({
                name: 'Database',
                status: 'can\'t connect to database',
                error: {
                    code: error.code,
                    message: error.message
                }
            });
        });
    });
}

function checkCloudConnection(): HealthStatus {
    return SocketSingleton.getInstance().isCloudConnected()
        ? {
            name: 'CloudConnection',
            status: 'cloud connection Ok'
        } : {
            name: 'CloudConnection',
            status: 'cloud is not connected'
        };
}