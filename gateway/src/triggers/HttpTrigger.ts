import { ITrigger } from '../types';
import { Express } from 'express';

export class HttpTrigger implements ITrigger {

    constructor(
        private deviceId: string,
        private app: Express,
        private url: string,
        private method: string,
    ) {
    }

    private handler(onEvent: (deviceId: string) => void) {
        return async (req, res) => {
            // console.log(`/api/devices/${this.deviceId}/${this.url}:\n`, req);
            onEvent(this.deviceId);
            res.status(200).send('Ok');
        };
    }

    setup(onEvent: (deviceId: string) => void): boolean {
        console.log('Http control for ' + this.deviceId + ' is enabled');
        try {
            switch (this.method.toLowerCase()) {
                case 'get': {
                    this.app.get(`/api/devices/${this.deviceId}/${this.url}`, this.handler(onEvent));
                    break;
                }
                case 'post': {
                    this.app.post(`/api/devices/${this.deviceId}/${this.url}`, this.handler(onEvent));
                    break;
                }
                default: {
                    return false;
                }
            }
        } catch {
            return false;
        }
        console.log(`Registered: /api/devices/${this.deviceId}/${this.url}`);
        return true;
    }

}