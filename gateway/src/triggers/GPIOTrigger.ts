import { ITrigger } from '../types';
import { DisabledGpio, EnabledGpio, IGpio } from '../helpers/GPIOHelper';

export class GPIOTrigger implements ITrigger {

    private trigger: IGpio;

    constructor(
        private gpioEnabled: boolean,
        private deviceId: string,
        private pin: number,
        private inverted: boolean,
    ) {
    }

    setup(onEvent: (deviceId: string) => void): boolean {
        this.trigger = this.gpioEnabled ? new EnabledGpio(this.pin, 'in', this.inverted ? 'falling' : 'rising', { debounceTimeout: 10 }) : new DisabledGpio();
        console.log('GPIO control for ' + this.deviceId + ' on pin ' + this.pin + ' is enabled');
        this.trigger.watch((error) => {
            if (error) {
                throw error;
            }

            onEvent(this.deviceId);
        });
        return true;
    }
}