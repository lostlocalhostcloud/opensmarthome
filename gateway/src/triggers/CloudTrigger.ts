import { ITrigger } from '../types';
import { Type } from '../gateprotocol/protocol';
import { SocketSingleton } from '../singletons/SocketSingleton';
import { CloudTriggerEvent, CloudTriggerResponseEvent } from '../events/CloudTriggerEvent';

export class CloudTrigger implements ITrigger {

    constructor(
        private deviceId: string,
    ) {
    }

    setup(onEvent: (deviceId: string) => void): boolean {
        const permittedTime = process.env.MAX_CLOUD_TRIGGER_DIFFERENCE_IN_SECONDS ? Number(process.env.MAX_CLOUD_TRIGGER_DIFFERENCE_IN_SECONDS) : 10;
        console.log('Cloud control for ' + this.deviceId + ' is enabled');
        SocketSingleton.getInstance().setupListener(Type.CloudTrigger + '-' + this.deviceId, async (event: CloudTriggerEvent, cb) => {
            try {
                event = new CloudTriggerEvent(event);
                if (event.deviceId !== this.deviceId) {
                    throw new Error('Device ID is not identical');
                }
                const timestampSend = new Date(event._meta.timestamp).getTime();
                const calculatedTime = (Date.now() - timestampSend) / 1000;
                // FIXME: Try to find the reason for this Behavior
                if (calculatedTime < 0) {
                    console.warn(`calculatedTime is below zero: ${calculatedTime}`);
                }
                if (calculatedTime <= permittedTime) {
                    onEvent(this.deviceId);
                } else {
                    throw new Error(`CloudTriggerEvent is too old. It took ${calculatedTime} seconds instead of the permitted ${permittedTime} seconds. Timestamp send: ${(timestampSend / 1000).toFixed()}`);
                }
            } catch (e) {
                console.error(e);
                cb(new CloudTriggerResponseEvent(this.deviceId, Type.Status.FAIL));
                return;
            }
            cb(new CloudTriggerResponseEvent(this.deviceId, Type.Status.OK));
        });
        return true;
    }

}