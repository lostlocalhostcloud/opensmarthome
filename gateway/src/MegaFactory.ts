import { Action } from './database/entity/Action';
import { IAction, IHashCookies, ISensor, ITrigger } from './types';
import {GPIOAction} from './actions/GPIOAction';
import { Trigger } from './database/entity/Trigger';
import {GPIOTrigger} from './triggers/GPIOTrigger';
import {CloudTrigger} from './triggers/CloudTrigger';
import { Sensor } from './database/entity/Sensor';
import {GPIOSensor} from './sensors/GPIOSensor';
import { Express } from 'express';
import { HttpTrigger } from './triggers/HttpTrigger';
import { Headers } from 'node-fetch';
import { UrlEncodedAction } from './actions/http/UrlEncodedAction';
import { RawAction } from './actions/http/RawAction';
import { FormDataAction } from './actions/http/FormDataAction';

export function actionFactory(action: Action): IAction {
    switch (action.type.id) {
        case 2: {
            return createGPIOAction(action.device.id, action.config);
        }
        case 3: {
            return createHttpAction(action.device.id, action.config);
        }
    }
    return undefined;
}

function createGPIOAction(deviceId: string, config: any): IAction {
    return new GPIOAction(process.env.GPIO_ENABLED === 'true', deviceId, config.pin, config.inverted, config.stateType);
}

function jsonToHeaders(rawHeaders: any): Headers{
    const headers = new Headers();
    for (const headersKey in rawHeaders) {
        headers.append(headersKey, rawHeaders[headersKey]);
    }
    return headers;
}

function jsonToCookies(rawCookies: any): IHashCookies{
    const hashCookies: IHashCookies = {};
    for (const cookieKey in rawCookies) {
        hashCookies[cookieKey] = rawCookies[cookieKey];
    }
    return hashCookies;
}

function jsonToHashString(rawBody: any): IHashCookies{
    const hashCookies: IHashCookies = {};
    for (const bodyKey in rawBody) {
        hashCookies[bodyKey] = rawBody[bodyKey];
    }
    return hashCookies;
}

function createHttpAction(deviceId: string, config: any): IAction {
    switch (config.type.toLowerCase()) {
        case 'urlencoded': {
            return new UrlEncodedAction(
                deviceId,
                config.url,
                jsonToHeaders(config.headers),
                config.method,
                jsonToCookies(config.cookies),
                config.redirect,
                jsonToHashString(config.body));
        }
        case 'formdata': {
            return new FormDataAction(
                deviceId,
                config.url,
                jsonToHeaders(config.headers),
                config.method,
                jsonToCookies(config.cookies),
                config.redirect,
                jsonToHashString(config.body));
        }
        case 'raw': {
            return new RawAction(
                deviceId,
                config.url,
                jsonToHeaders(config.headers),
                config.method,
                jsonToCookies(config.cookies),
                config.redirect,
                config.body,
                config.contentType);
        }
    }
}

export function triggerFactory(trigger: Trigger, app: Express): ITrigger {
    switch (trigger.type.id) {
        case 1: {
            return createCloudTrigger(trigger.device.id);
        }
        case 2: {
            return createGPIOTrigger(trigger.device.id, trigger.config);
        }
        case 3: {
            return createHttpTrigger(trigger.device.id, trigger.config, app);
        }
    }
    return undefined;
}

function createGPIOTrigger(deviceId: string, config: any): ITrigger {
    return new GPIOTrigger(process.env.GPIO_ENABLED === 'true', deviceId, config.pin, config.inverted);
}

function createHttpTrigger(deviceId: string, config: any, app: Express): ITrigger {
    return new HttpTrigger(deviceId, app, config.url, config.method);
}

function createCloudTrigger(deviceId: string): ITrigger {
    return new CloudTrigger(deviceId);
}

export function sensorFactory(sensor: Sensor): ISensor<unknown> {
    switch (sensor.type.id) {
        case 2: {
            return createGPIOSensor(sensor.device.id, sensor.config);
        }
    }

    return undefined;
}

function createGPIOSensor(deviceId: string, config: any): ISensor<any> {
    return new GPIOSensor(process.env.GPIO_ENABLED === 'true', deviceId, config.pin, config.inverted);
}
