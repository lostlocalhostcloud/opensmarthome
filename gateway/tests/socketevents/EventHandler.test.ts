import "reflect-metadata";
import { EmitterMock } from '../mocks/EmitterMock';
import { DeviceCreate, Event, Type } from '../../src/gateprotocol/protocol';
import { EventHandler } from '../../src/socketevents/EventHandler';
import { DatabaseMock } from '../mocks/DatabaseMock';
import { Device } from '../../src/database/entity/Device';
import { Trigger } from '../../src/database/entity/Trigger';
import { Action } from '../../src/database/entity/Action';

interface IHashDevice {
    [event: string]: Device;
}

interface IHashTrigger {
    [event: string]: Trigger;
}
interface IHashAction {
    [event: string]: Action;
}

test('test onDeviceCreate() of EventHandler', () => {

    const emitter = new EmitterMock(null)
    const databaseMock = new DatabaseMock();
    const deviceHash: IHashDevice = {};
    const triggerHash: IHashTrigger = {};
    const actionHash: IHashAction = {}

    // FIXME
    // SocketSingleton.setEmitter(emitter);
    EventHandler.initEventHandler(databaseMock);

    databaseMock.saveDeviceFn = (device, options) => {
        return new Promise<Device>((resolve) => {
            deviceHash[device.id] = device
            databaseMock.findDeviceByIdFn = (id) => {
                return new Promise<Device>((resolve) => {
                    resolve(deviceHash[id]);
                });
            }
            resolve(device);
        });
    }

    databaseMock.findTypeByIdFn = (id) => {
        return new Promise<any>((resolve) => {
            resolve({
                id,
                name: 'unittest'
            });
        });
    }

    databaseMock.saveEntityFn = (data, entityType) => {
        return new Promise<unknown>((resolve) => {
            switch (entityType) {
                case Type.IOEntryKinds.Action: {
                    actionHash[data.id] = data;
                    break;
                }
                case Type.IOEntryKinds.Sensor: {
                    break;
                }
                case Type.IOEntryKinds.Trigger: {
                    triggerHash[data.id] = data;
                    break;
                }

            }
            resolve(data);
        });

    }

    const eventType = Type.DeviceCreateType;

    const createDevice: DeviceCreate = {
        name: 'UnitTest Device',
        typeId: 0,
        triggers: [{
            config: {data: 'UnitTestMockData'},
            kind: Type.IOEntryKinds.Trigger,
            name: 'mockTrigger',
            typeId: 0,
            deviceId: ''
        }],
        actions: [{
            config: {},
            kind: Type.IOEntryKinds.Action,
            name: 'mockAction',
            typeId: 0,
            deviceId: ''
        }]
    }

    const event: Event = {
        _meta: {
            type: eventType,
            api_version: 'v1'
        },
        payload: createDevice
    }

    const expectdata = (data) => {
        expect({
            "_meta": {
                "type": "StatusResponse",
                "api_version": "v1"
            },
            "payload": {
                "status": "OK",
                "id": data.payload.id
            }
        }).toEqual(data);
        expect({
            "id": data.payload.id,
            "typeId": 0,
            "name": "UnitTest Device"
        }).toEqual(deviceHash[data.payload.id])


    }

    emitter.emitEvent(eventType,event,expectdata)
});