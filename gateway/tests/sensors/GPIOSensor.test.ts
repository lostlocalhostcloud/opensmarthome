import { newActionAndSensorAndTriggerType, newDevice, newDeviceType, newSensor } from '../TestHelperFuntions';
import { sensorFactory } from '../../src/MegaFactory';
import { GPIOSensor } from '../../src/sensors/GPIOSensor';

test('Test that GPIOSensor can be created', () => {
    const sensor = newSensor(
        1,
        'testSensor',
        newDevice(
            'testDev1-UUID',
            newDeviceType(
                2,
                'GPIO'),
            'Test Device 1'),
        newActionAndSensorAndTriggerType(
            2,
            'GPIO'),
        {
            pin: 22,
            inverted: false
        }
    );
    const gpioSensor = new GPIOSensor(false, sensor.device.id, sensor.config.pin, sensor.config.inverted);
    expect(sensorFactory(sensor)).toEqual(gpioSensor);
});

describe('GPIOSensor tests with already existing GPIOSensor', () => {
   let gpioSensor;
   beforeAll(() => {
       gpioSensor = new GPIOSensor(false, 'testDev2-UUID', 22, true);
   });

   test('Test that GPIOSensor can be setup', () => {
      expect(gpioSensor.setup()).toEqual(true);
   });

    test('Test that GPIOSensor can be read', () => {
        expect(gpioSensor.read()).toEqual(false);
    });
});