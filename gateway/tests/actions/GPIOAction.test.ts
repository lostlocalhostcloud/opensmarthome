import { newAction, newActionAndSensorAndTriggerType, newDevice, newDeviceType } from '../TestHelperFuntions';
import { GPIOAction } from '../../src/actions/GPIOAction';
import { actionFactory } from '../../src/MegaFactory';

test('Test that GPIOAction can be created', () => {
    const action = newAction(
        1,
        'testAction',
        newDevice(
            'testDev1-UUID',
            newDeviceType(
                2,
                'GPIO'),
            'Test Device 1'),
        newActionAndSensorAndTriggerType(
            2,
            'GPIO'),
        {
            pin: 42,
            inverted: false,
            stateType: 'stateless'
        }
    );
    const gpioAction = new GPIOAction(false, action.device.id, action.config.pin, action.config.inverted, action.config.stateType);
    expect(actionFactory(action)).toEqual(gpioAction);
});

describe('GPIOAction tests with already existing GPIOAction', () => {
   let gpioAction;
   beforeAll(() => {
       gpioAction = new GPIOAction(false, 'testDev2-UUID', 42, true, 'stateful');
   });

   test('Test that GPIOAction can be setup', () => {
      expect(gpioAction.setup()).toEqual(true);
   });

    test('Test that GPIOAction can be executed', () => {
        expect(gpioAction.execute()).toEqual(true);
    });

    test('Test that GPIOAction with not matching stateType cant be executed', () => {
        const gpioActionWithNotMatchingStateType = new GPIOAction(false, 'testDev3-UUID', 42, true, 'notMatchingStateType')
        expect(gpioActionWithNotMatchingStateType.setup()).toEqual(true);
        expect(gpioActionWithNotMatchingStateType.execute()).toEqual(false);
    });
});