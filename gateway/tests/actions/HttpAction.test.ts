import { RawAction } from '../../src/actions/http/RawAction';
import { Headers } from 'node-fetch';
import { IHashCookies } from '../../src/types';

const headers = new Headers();
headers.append('Cookie','unit-test=unittest-id; auth=dGVzdDp0c2V0; ');
headers.append('Content-Type','text/plain');

const cookies: IHashCookies = {};
cookies['unit-test'] = 'unittest-id';
cookies['auth'] = 'dGVzdDp0c2V0';

test('test setup() of HttpAction with RawAction (adding Cookies)', () => {

    const rawAction = new RawAction(
        'test-RawAction',
        'http://test.exsample.com/test/unit',
        undefined,
        'POST',
        cookies,
        true,
        undefined,
        'text/plain'
    );
    rawAction.setup();
    expect(rawAction['options'].headers).toEqual(headers);
});