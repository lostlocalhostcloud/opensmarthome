import { UrlEncodedAction } from '../../../src/actions/http/UrlEncodedAction';
import { Headers } from 'node-fetch';
import { IHashString } from '../../../src/types';

import { URLSearchParams } from 'url';


const testData: IHashString = {};
testData.password = 'test';
testData.username = 'unittest-test-user';

const resultData = new URLSearchParams();
resultData.append('password','test');
resultData.append('username','unittest-test-user');

const headers = new Headers();
headers.append('test_kind','unit');
headers.append("Content-Type", "application/x-www-form-urlencoded")

test('test setup() of UrlEncoded (adding FormData)', () => {

    const urlEnodedAction = new UrlEncodedAction(
    'test-UrlEncodedAction',
        'http://test.exsample.com/test/unit',
        headers,
        'POST',
        undefined,
        true,
        testData,
        )
    expect(urlEnodedAction.setup());
    expect(urlEnodedAction['options'].body).toEqual(resultData);
});

test('test setup() of UrlEncoded (adding Content-Type)', () => {

    const urlEnodedAction = new UrlEncodedAction(
        'test-FormDataAction',
        'http://test.exsample.com/test/unit',
        headers,
        'POST',
        undefined,
        true,
        testData,
    )
    expect(urlEnodedAction.setup());
    expect(urlEnodedAction['options'].headers).toEqual(headers);
});