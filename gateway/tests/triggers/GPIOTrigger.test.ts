import { newActionAndSensorAndTriggerType, newDevice, newDeviceType, newTrigger } from '../TestHelperFuntions';
import { triggerFactory } from '../../src/MegaFactory';
import { GPIOTrigger } from '../../src/triggers/GPIOTrigger';

test('Test that GPIOTrigger can be created', () => {
    const trigger = newTrigger(
        1,
        'testTrigger',
        newDevice(
            'testDev1-UUID',
            newDeviceType(
                2,
                'GPIO'),
            'Test Device 1'),
        newActionAndSensorAndTriggerType(
            2,
            'GPIO'),
        {
            pin: 27,
            inverted: false
        }
    );
    const gpioTrigger = new GPIOTrigger(false, trigger.device.id, trigger.config.pin, trigger.config.inverted);
    expect(triggerFactory(trigger, undefined)).toEqual(gpioTrigger);
});

describe('GPIOTrigger tests with already existing GPIOTrigger', () => {
   let gpioTrigger;
   beforeAll(() => {
       gpioTrigger = new GPIOTrigger(false, 'testDev2-UUID', 27, true);
   });

   test('Test that GPIOTrigger can be setup', () => {
      expect(gpioTrigger.setup()).toEqual(true);
   });
});