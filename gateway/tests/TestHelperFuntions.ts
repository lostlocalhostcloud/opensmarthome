import { Device } from '../src/database/entity/Device';
import { DeviceType } from '../src/database/entity/DeviceType';
import { Action } from '../src/database/entity/Action';
import { ActionAndSensorAndTriggerType } from '../src/database/entity/ActionAndSensorAndTriggerType';
import { Trigger } from '../src/database/entity/Trigger';
import { Sensor } from '../src/database/entity/Sensor';

function newDevice(id: string, type: DeviceType, name: string): Device {
    return  {
        typeId: type.id,
        id,
        type,
        name
    };
}

function newDeviceType(id: number, name: string): DeviceType {
    return  {
        id,
        name
    };
}

function newAction(id: number, name: string, device: Device, type: ActionAndSensorAndTriggerType, config: any): Action {
    return  {
        deviceId: device.id,
        id,
        name,
        device,
        type,
        config
    };
}

function newTrigger(id: number, name: string, device: Device, type: ActionAndSensorAndTriggerType, config: any): Trigger {
    return  {
        deviceId: device.id,
        name: null,
        id,
        device,
        type,
        config
    };
}

function newSensor(id: number, name: string, device: Device, type: ActionAndSensorAndTriggerType, config: any): Sensor {
    return  {
        deviceId: device.id,
        id,
        name,
        device,
        type,
        config
    };
}

function newActionAndSensorAndTriggerType(id: number, name: string): ActionAndSensorAndTriggerType {
    return {
        id,
        name
    }
}

export {newAction, newTrigger, newSensor, newDeviceType, newDevice, newActionAndSensorAndTriggerType}