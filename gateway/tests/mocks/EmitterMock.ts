// @ts-ignore
import { Emitter }from "component-emitter"
import { IAction } from '../../src/types';

interface IHashEvents {
    [event: string]: Function[];
}

class EmitterMock implements Emitter{

    private eventHolder: IHashEvents = {}

    constructor(
        private reactToEmitCallBack: any,
    ) {
    }

    events: {event: string | symbol, data: any}[] = [];

    emitEvent(event: string, ...args: any[]) {
        if (this.eventHolder[event] != undefined) {
            this.eventHolder[event].forEach(e => {
                e(...args)
            })
        }
    }

    emit(event: string, ...args: any[]): Emitter {
        if (args.length > 1) {
            this.reactToEmitCallBack(args[0], (cb) => {
                args[args.length-1](cb);
            });
        }
        this.events.push({event: event, data: args[0]})
        return this;
    }

    hasListeners(event: string): boolean {
        if (this.eventHolder[event] == undefined) {
            return false;
        }
        return true;
    }

    listeners(event: string): Function[] {
        if (this.eventHolder[event] == undefined) {
            return this.eventHolder[event] = [];
        }
        return this.eventHolder[event];
    }

    off(event?: string, listener?: Function): Emitter {
        return undefined;
    }

    on(event: string, listener: Function): Emitter {
        if (this.eventHolder[event] == undefined) {
            this.eventHolder[event] = [];
        }
        this.eventHolder[event].push(listener);
        return this;
    }

    once(event: string, listener: Function): Emitter {
        return undefined;
    }

}

export { EmitterMock, IHashEvents }