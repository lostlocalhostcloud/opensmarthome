import { Database } from '../../src/Database';
import { Device } from '../../src/database/entity/Device';
import { SaveOptions } from 'typeorm/repository/SaveOptions';

export class DatabaseMock implements Database {

    public findDeviceByIdFn: (id: string) => any;
    public findTriggerByIdFn: (id: number) => any;
    public findActionByIdFn: (id: number) => any;
    public findSensorByIdFn: (id: number) => any;
    public findTypeByIdFn: (id: number) => any;

    public findActionsByOptionalDeviceIdFn: (deviceId?: string) => any;
    public findSensorsByOptionalDeviceIdFn: (deviceId?: string) => any;
    public findTriggersByOptionalDeviceIdFn: (deviceId?: string) => any;

    public findEntityByIdFn: (id: any, entityType: any) => any;
    public findEntityByConditionFn: (condition: any, entityType: any) => any;
    public findOneEntityByConditionOrFailFn: (condition: any, entityType: any) => any;

    public healthcheckFn: () => any;

    public saveDeviceFn: (device: Device, options?: SaveOptions) => any;
    public saveEntityFn: (data: any, entityType: any) => any;

    public deleteDeviceByIDFn: (id: string)  => any;
    public deleteEntityFn: (data: any, entityType: any) => any;
    public deleteTriggerByDeviceIdFn: (deviceId: string) => any;
    public deleteActionByDeviceIdFn: (deviceId: string) => any;
    public deleteSensorByDeviceIdFn: (deviceId: string) => any;
    

    deleteActionByDeviceId(deviceId: string) {
        return this.deleteActionByDeviceIdFn(deviceId);
    }

    deleteDeviceByID(id: string) {
        return this.deleteDeviceByIDFn(id);
    }

    deleteEntity(data: any, entityType: any) {
        return this.deleteEntityFn(data, entityType)
    }

    deleteSensorByDeviceId(deviceId: string) {
        return this.deleteSensorByDeviceIdFn(deviceId);
    }

    deleteTriggerByDeviceId(deviceId: string) {
        return this.deleteTriggerByDeviceIdFn(deviceId);
    }

    findActionById(id: number) {
        return this.findActionByIdFn(id);
    }

    findDeviceById(id: string) {
        return this.findDeviceByIdFn(id);
    }

    findEntityByCondition(conidtion: any, entityType: any) {
        return this.findEntityByConditionFn(conidtion, entityType);
    }

    findEntityById(id: any, entityType: any) {
        return this.findEntityByIdFn(id, entityType);
    }

    findOneEntityByConditionOrFail(condition: any, entityType: any) {
        return this.findOneEntityByConditionOrFailFn(condition, entityType);
    }

    findSensorById(id: number) {
        return this.findSensorByIdFn(id);
    }

    findTriggerById(id: number) {
        return this.findTriggerByIdFn(id);
    }

    findTypeById(id: number) {
        return this.findTypeByIdFn(id);
    }

    saveDevice(device: Device, options?: SaveOptions) {
        return this.saveDeviceFn(device, options)
    }

    saveEntity(data: any, entityType: any) {
        return this.saveEntityFn(data, entityType);
    }

    findActionsByOptionalDeviceId(deviceId?: string) {
        return this.findActionsByOptionalDeviceIdFn(deviceId);
    }

    findSensorsByOptionalDeviceId(deviceId?: string) {
        return this.findSensorsByOptionalDeviceIdFn(deviceId);
    }

    findTriggersByOptionalDeviceId(deviceId?: string) {
        return this.findTriggersByOptionalDeviceIdFn(deviceId);
    }

    healthcheck() {
        return this.healthcheckFn();
    }

}