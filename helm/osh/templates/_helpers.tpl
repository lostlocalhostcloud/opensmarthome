{{/*
Expand the name of the chart.
*/}}
{{- define "osh.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "osh.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "osh.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "osh.labels" -}}
helm.sh/chart: {{ include "osh.chart" . }}
{{ include "osh.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{- define "osh.frontend.labels" -}}
{{ include "osh.labels" . }}
service: "frontend"
{{- end }}

{{- define "osh.authentication.labels" -}}
{{ include "osh.labels" . }}
service: "authentication"
{{- end }}

{{- define "osh.registration.labels" -}}
{{ include "osh.labels" . }}
service: "registration"
{{- end }}

{{- define "osh.smarthomemanagement.labels" -}}
{{ include "osh.labels" . }}
service: "smarthome-management"
{{- end }}

{{/*
Selector labels
*/}}
{{- define "osh.selectorLabels" -}}
app.kubernetes.io/name: {{ include "osh.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{- define "osh.frontend.selectorLabels" -}}
{{ include "osh.selectorLabels" . }}
service: "frontend"
{{- end }}

{{- define "osh.authentication.selectorLabels" -}}
{{ include "osh.selectorLabels" . }}
service: "authentication"
{{- end }}

{{- define "osh.registration.selectorLabels" -}}
{{ include "osh.selectorLabels" . }}
service: "registration"
{{- end }}

{{- define "osh.smarthomemanagement.selectorLabels" -}}
{{ include "osh.selectorLabels" . }}
service: "smarthome-management"
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "osh.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "osh.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}
