## Trigger, Actions And Sensor modification

---
### Create
#### "IOCreate"
```json5
{
  "_meta": {
    "type": "IOCreate",
    "api_version": "v1",
  },
  "id": "asdasds",
  "name" : "",
  "config": {},
  "typeId": 0,
  "deviceId": "asdasda",
  "transactionId": "HASH.RANDOM"
}
```
#### "IOCreated"
```json5
{
  "_meta": {
    "type": "IOCreate",
    "api_version": "v1",
  },
  "status": "ok",
}
```

### Update
#### "IOUpdate" 
```json5
{
    "_meta": {
        "type": "IOCreate",
        "api_version": "v1",
    },
    "id": "asdasds",
    "name" : "",
    "config": {},
    "typeId": 0,
    "deviceId": "asdasda",
    "transactionId": "HASH.RANDOM"
}
```
#### "IOUpdated"
```json5
{
    "_meta": {
        "type": "IOCreate",
        "api_version": "v1",
    },
    "status": "ok"
}
```

### Delete
#### "IODelete" 
```json5
{
    "_meta": {
        "type": "IOCreate",
        "api_version": "v1",
    },
    "id": "asdasds",
    "transactionId": "HASH.RANDOM"
}
```
#### "IODeleted" 
```json5
{
    "_meta": {
        "type": "IOCreate",
        "api_version": "v1",
    },
    "status": "ok"
}
```

## Device modification

---
### Create
#### "DeviceCreate"
```json5
{
    "_meta": {
        "type": "IOCreate",
        "api_version": "v1",
    },
    "name" : "",
    "typeId": 0,
    "actions": [],          // optional
    "sensors": [],          // optional
    "triggers": [],         // optional
    "transactionId": "HASH.RANDOM",
}
```
#### "DeviceCreated" 
```json5
{
    "_meta": {
        "type": "IOCreate",
        "api_version": "v1",
    },
    "status": "ok",
    "id" : "",
}
```

## Update
#### "DeviceUpdate"
```json5
{
    "_meta": {
        "type": "IOCreate",
        "api_version": "v1",
    },
    "name" : "",        // optional
    "typeId": 0,        // optional
    "transactionId": "HASH.RANDOM"
}
```
#### "DeviceUpdated" 
```json5
{
    "_meta": {
        "type": "IOCreate",
        "api_version": "v1",
    },
    "status": "ok",
}
```

## Delete
#### "DeviceDelete"
```json5
{
    "_meta": {
        "type": "IOCreate",
        "api_version": "v1",
    },
    "id" : "",
    "transactionId": "HASH.RANDOM"
}
```
#### "DeviceDeleted"
```json5
{
    "_meta": {
        "type": "IOCreate",
        "api_version": "v1",
    },
    "status": "ok",
}
```