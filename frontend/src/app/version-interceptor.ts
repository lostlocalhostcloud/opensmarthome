import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Observable, tap } from 'rxjs';
import { VERSION } from './version';
import { UserSessionService } from './application-management/user-session.service';
import { AuthService } from './application-management/login/auth.service';

@Injectable()
export class VersionInterceptor implements HttpInterceptor {

  constructor(private authService: AuthService, private userSessionService: UserSessionService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req.clone({
      headers: req.headers.set('OSH-Frontend-Version', VERSION)
    })).pipe(tap(event => {
      if (event instanceof HttpResponse) {
        const oshAppVersion = <string>event.headers.get('OSH-App-Version');
        if (oshAppVersion) {
          this.userSessionService.updateVersion(oshAppVersion);
        }
      }
      this.authService.checkLoginExpiration();
    }));
  }
}
