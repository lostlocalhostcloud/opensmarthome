import { Component, OnInit, ViewChild } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AbstractControl, FormBuilder, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatStepper, MatStepperModule } from '@angular/material/stepper';
import { QRCodeModule } from 'angularx-qrcode';
import { RouterModule } from '@angular/router';
import { BackendService } from '../backend.service';
import { ClipboardModule } from '@angular/cdk/clipboard';

@Component({
  selector: 'app-registration',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatStepperModule,
    QRCodeModule,
    ReactiveFormsModule,
    RouterModule,
    ClipboardModule
  ],
  templateUrl: './registration.component.html',
  styleUrl: './registration.component.scss'
})
export class RegistrationComponent implements OnInit {

  @ViewChild('stepper')
  private stepper: MatStepper | undefined;

  // @ts-ignore
  regCodeFormGroup: FormGroup;
  // @ts-ignore
  userDetailsFormGroup: FormGroup;
  // @ts-ignore
  setupOTPFormGroup: FormGroup;
  // @ts-ignore
  passwordFormGroup: FormGroup;

  // regCode Form
  regCodeVerified = false;
  regCodeErrorMessage: string | null | undefined;
  regCode: string | null | undefined;

  // userDetails Form
  userDetailsVerified = false;
  username: string | undefined;
  emailAddress: string | undefined;
  firstname: string | undefined;
  lastname: string | undefined;

  // setupOtp Form
  otpVerified = false;
  // @ts-ignore
  otpSecret: string;
  otpLink = 'Link is not generated yet.';
  otpDetailsDisplay = 'none';
  verifyOtpErrorMessage: string | null | undefined;
  otpToken: string | null | undefined;

  // password Form
  passwordVerified = false;
  password: string | undefined;
  registerErrorMessage: string | undefined;


  constructor(private formBuilder: FormBuilder,
              private backendService: BackendService) { }

  ngOnInit(): void {
    this.regCodeFormGroup = this.formBuilder.group({
      regCode: ['', Validators.required]
    });
    this.userDetailsFormGroup = this.formBuilder.group({
      username: ['', Validators.required],
      mailAddress: ['', Validators.required],
      optional: ['']
    });
    this.setupOTPFormGroup = this.formBuilder.group({
      otpToken: ['', Validators.required]
    });
    this.passwordFormGroup = this.formBuilder.group({
      password: ['', Validators.required],
      passwordVerify: ['', Validators.required]
    }, {validator: this.passwordConfirming});
  }

  // @ts-ignore
  passwordConfirming(c: AbstractControl): { invalid: boolean } {
    // @ts-ignore
    if (c.get('password').value !== c.get('passwordVerify').value) {
      return {invalid: true};
    }
  }

  verifyRegCode(): void {
    if (this.regCodeFormGroup.valid) {
      this.regCodeErrorMessage = null;
      if (!this.regCodeVerified && this.regCode) {
        this.backendService.regCode(this.regCode).then(response => {
          this.regCodeVerified = true;
          this.otpSecret = response;
          // @ts-ignore
          this.stepper.next();
        }).catch(error => {
          this.regCodeErrorMessage = error.error;
          this.regCode = null;
        });
      } else {
        // @ts-ignore
        this.stepper.next();
      }
    }
  }

  verifyUserDetails(): void {
    if (this.userDetailsFormGroup.valid) {
      if (!this.userDetailsVerified && this.username && this.emailAddress) {
        this.backendService.verifyRegister(this.username, this.emailAddress).then(response => {
          this.userDetailsVerified = true;
          this.otpLink = 'otpauth://totp/OpenSmartHome:' + this.username + '?secret=' + this.otpSecret + '&issuer=OpenSmartHome';
          // @ts-ignore
          this.stepper.next();
        }).catch(error => {
          console.log(error);
        });
      } else {
        // @ts-ignore
        this.stepper.next();
      }
    }
  }

  verifyOTP(): void {
    if (this.setupOTPFormGroup.valid) {
      this.verifyOtpErrorMessage = null;
      if (!this.otpVerified && this.otpToken) {
        this.backendService.verifyOTP(this.otpToken.replace(' ', ''), this.otpSecret).then(response => {
          this.otpVerified = true;
          // @ts-ignore
          this.stepper.next();
        }).catch(error => {
          this.verifyOtpErrorMessage = error.error;
          this.otpToken = null;
        });
      } else {
        // @ts-ignore
        this.stepper.next();
      }
    }
  }

  openOTPLink(): void {
    if (this.otpLink) {
      window.open(this.otpLink);
    }
  }

  register(): void {
    if (this.passwordFormGroup.valid) {
      if (!this.passwordVerified && this.username && this.emailAddress && this.password && this.regCode) {
        this.backendService.register(this.username,
          this.emailAddress,
          this.otpSecret,
          this.regCode,
          this.password,
          this.firstname,
          this.lastname).then(response => {
          // @ts-ignore
          this.stepper.next();
        }).catch(error => {
          this.registerErrorMessage = error.error;
        });
      } else {
        // @ts-ignore
        this.stepper.next();
      }
    }
  }
}
