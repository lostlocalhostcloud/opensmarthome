import { EventEmitter, Injectable } from '@angular/core';
import { UserSessionService } from '../user-session.service';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public authenticated = new BehaviorSubject<boolean>(false);
  public loginExpiresSoon = new EventEmitter<number>();

  constructor(private userSessionService: UserSessionService) { }

  isAuthenticated(preAuth: boolean): Promise<boolean> {
    if (preAuth) {
      return this.userSessionService.isPreAuthorized();
    }

    return this.userSessionService.getUserDetails()
      .then((response: any) => {
        const loginExpiration = response.loginExpiration;
        localStorage.setItem('loginExpiration', loginExpiration);
        this.checkLoginExpiration(loginExpiration);

        this.authenticated.next(true);
        return true;
      })
      .catch(() => {
        localStorage.removeItem('loginExpiration');
        this.authenticated.next(false);
        return false;
      });
  }

  public checkLoginExpiration(loginExpiration?: number): void {
    if (!loginExpiration) {
      if (!this.authenticated.getValue()) return;
      const loginExpirationStorage = localStorage.getItem('loginExpiration');
      if (!loginExpirationStorage) return;
      loginExpiration = Number(loginExpirationStorage);
    }

    const loginExpirationDifferenceInTime = new Date(loginExpiration * 1000).getTime() - new Date().getTime();
    const loginExpirationDifferenceInDays = Number((loginExpirationDifferenceInTime / (1000 * 3600 * 24)).toString().split('.')[0]);

    if (loginExpirationDifferenceInDays <= 7) {
      this.loginExpiresSoon.emit(loginExpirationDifferenceInDays);
    }
  }
}
