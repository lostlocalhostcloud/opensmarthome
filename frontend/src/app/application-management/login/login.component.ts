import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormBuilder, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { Router } from '@angular/router';
import { BackendService } from '../../backend.service';

@Component({
  selector: 'app-login',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    MatButtonModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatProgressBarModule,
    ReactiveFormsModule
  ],
  templateUrl: './login.component.html',
  styleUrl: './login.component.scss'
})
export class LoginComponent {

  form: FormGroup;

  loading = false;
  error: string | null | undefined;


  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private backendService: BackendService
  ) {
    this.form = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  private getFormControls(): any {
    return this.form.controls;
  }

  login(): void {
    this.error = null;

    if (this.form.invalid) {
      return;
    }

    this.loading = true;

    this.backendService.login(this.getFormControls()['username']?.value, this.getFormControls()['password']?.value)
      .then((response) => {
        if (response === '2FA-required') {
          this.router.navigateByUrl('/second-factor');
        } else {
          this.router.navigateByUrl('/');
        }
      })
      .catch(error => this.error = error.statusText)
      .finally(() => {
        this.loading = false;
        this.form.reset();
      });
  }
}
