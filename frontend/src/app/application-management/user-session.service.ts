import { EventEmitter, Injectable } from '@angular/core';
import { BackendService } from '../backend.service';
import { VERSION } from '../version';

@Injectable({
  providedIn: 'root'
})
export class UserSessionService {

  public currentSmartHomeId: number | undefined;

  public versionDifference = new EventEmitter<string>();

  private userDetails: UserDetails | undefined;

  constructor(private backendService: BackendService) { }

  isPreAuthorized(): Promise<boolean> {
    return this.backendService.preAuthorized();
  }

  getUserDetails(): Promise<UserDetails> {
    if (this.userDetails === undefined) {
      return new Promise((resolve, reject) => {
        this.fetchUserDetails(resolve, reject);
      });
    } else {
      return new Promise((resolve) => {
        if (this.userDetails) resolve(this.userDetails);
      });
    }
  }

  private fetchUserDetails(resolve: any, reject: any): void {
    this.backendService.getUserInfo()
      .subscribe(
        (response) => {
          this.userDetails = response;
          resolve(response);
        },
        (error) => {
          reject(error);
        });
  }

  updateVersion(version: string) {
    if (version !== VERSION) this.versionDifference.emit(version);
    else this.versionDifference.emit();
  }
}

interface UserDetails {
  username: string;
  loginExpiration: number;
}
