import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { BackendService } from '../../backend.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-logout',
  standalone: true,
  imports: [
    CommonModule,
    MatProgressBarModule
  ],
  templateUrl: './logout.component.html',
  styleUrl: './logout.component.scss'
})
export class LogoutComponent implements OnInit {

  loading: boolean | undefined;

  logoutFailed: boolean | undefined;

  constructor(private router: Router,
              private backendService: BackendService) { }

  ngOnInit(): void {
    this.loading = true;

    this.backendService.logout().then(() => {
      this.router.navigateByUrl('/').then(() => {
        localStorage.removeItem('loginExpiration');
        window.location.reload();
      });
    }).catch(() => {
      this.logoutFailed = true;
    }).finally(() => {
      this.loading = false;
    });
  }
}
