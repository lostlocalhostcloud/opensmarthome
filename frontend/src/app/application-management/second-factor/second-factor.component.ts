import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormBuilder, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { Router } from '@angular/router';
import { BackendService } from '../../backend.service';

@Component({
  selector: 'app-second-factor',
  standalone: true,
    imports: [
        CommonModule,
        FormsModule,
        MatButtonModule,
        MatCardModule,
        MatFormFieldModule,
        MatInputModule,
        MatProgressBarModule,
        ReactiveFormsModule
    ],
  templateUrl: './second-factor.component.html',
  styleUrl: './second-factor.component.scss'
})
export class SecondFactorComponent {

  form: FormGroup;

  loading = false;
  error: string | null | undefined;

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private backendService: BackendService) {
    this.form = this.formBuilder.group({
      otpToken: ['', Validators.required]
    });
  }

  private getFormControls(): any {
    return this.form.controls;
  }

  verify(): void {
    this.error = null;

    if (this.form.invalid) {
      return;
    }

    this.loading = true;

    this.backendService.otpVerify(this.getFormControls()['otpToken']?.value)
      .then(() => this.router.navigateByUrl('/'))
      .catch(error => this.error = error.error)
      .finally(() => {
        this.loading = false;
        this.form.reset();
      });
  }
}
