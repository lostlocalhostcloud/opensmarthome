import { Component, OnDestroy, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexModule } from '@angular/flex-layout';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { SocketioService } from '../socketio.service';
import { BackendService } from '../backend.service';
import { UserSessionService } from '../application-management/user-session.service';
import { ActivatedRoute, Router } from '@angular/router';

interface DeviceType {
  name: string;
  icon: string;
  sensorTrueText: string;
  sensorFalseText: string;
}

interface Device {
  id: string;
  name: string;
  online: boolean;
  hasSensor: boolean;
  sensorState: boolean;
  type: DeviceType;
  loading: boolean;
}

@Component({
  selector: 'app-dashboard',
  standalone: true,
  imports: [
    CommonModule,
    FlexModule,
    MatButtonModule,
    MatCardModule,
    MatIconModule,
    MatProgressBarModule
  ],
  templateUrl: './dashboard.component.html',
  styleUrl: './dashboard.component.scss'
})
export class DashboardComponent implements OnInit, OnDestroy {

  smartHomeId: number | undefined;

  devices: Array<Device> = [];
  devicesLoading = false;
  devicesErrorMessage: string | null | undefined;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private userSessionService: UserSessionService,
              private backendService: BackendService,
              private socketioService: SocketioService) { }

  ngOnInit(): void {
    const switchDeviceType: DeviceType = {
      name: 'Switch', icon: 'power_input', sensorTrueText: 'On', sensorFalseText: 'Off'
    };
    const garagedoorDeviceType: DeviceType = {
      name: 'Garagedoor', icon: 'directions_car', sensorTrueText: 'Open', sensorFalseText: 'Closed'
    };
    this.route.params.subscribe((params) => {
      if (params['id'] === ':id' || params['id'] === 'undefined') {
        this.router.navigateByUrl('/dashboard/' + this.userSessionService.currentSmartHomeId);
        this.devicesErrorMessage = 'Please choose your Home';
        this.devices = [];
        return;
      }
      this.userSessionService.currentSmartHomeId = params['id'];
      this.smartHomeId = params['id'];
      this.devicesErrorMessage = null;
      this.devices = [];
      this.devicesLoading = true;
      this.backendService.getDevicesBySmartHomeId(params['id']).then((response) => {
        response.forEach((res: any) => {
          const deviceType = res.typeId === 2 ? garagedoorDeviceType : switchDeviceType;
          this.devices.push({
            id: res.id,
            name: res.name,
            online: res.online,
            sensorState: false,
            hasSensor: res.hasSensor,
            type: deviceType,
            loading: false
          });
        });
        this.devicesLoading = false;
      }).then(() => {
        this.backendService.getDeviceSensorStatesBySmartHomeId(params['id']).then((response) => {
          response.devices.forEach((device: any) => {
            this.devices.forEach((frontendDevice) => {
              if (device.id === frontendDevice.id) {
                frontendDevice.sensorState = device.sensorState;
              }
            });
          });
        }).catch((error) => {
          this.devicesErrorMessage = error.error;
          this.devicesLoading = false;
        });
      }).catch((error) => {
        this.devicesErrorMessage = error.error;
        this.devicesLoading = false;
      });
    });
    this.socketioService.setupSensorEventListener((data) => {
      this.devices.forEach((device) => {
        if (device.id === data.deviceId) {
          device.sensorState = data.sensorState;
        }
      });
    });
  }

  ngOnDestroy(): void {
    this.socketioService.disconnectSocket();
  }

  triggerDevice(device: Device): void {
    if (this.smartHomeId) {
      this.devicesErrorMessage = null;
      device.loading = true;
      this.backendService.triggerDevice(this.smartHomeId, device.id).then(() => {
        device.loading = false;
      }).catch((error) => {
        console.log(error);
        this.devicesErrorMessage = error.error;
        device.loading = false;
      });
    }
  }
}
