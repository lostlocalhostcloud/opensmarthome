import { Routes } from '@angular/router';
import { LoginComponent } from './application-management/login/login.component';
import { HomeComponent } from './home/home.component';
import { AuthGuardService } from './application-management/login/auth-guard.service';
import { SecondFactorComponent } from './application-management/second-factor/second-factor.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LogoutComponent } from './application-management/logout/logout.component';
import { UserSettingsComponent } from './user-settings/user-settings.component';
import { ActivityLogComponent } from './activity-log/activity-log.component';
import { FilesComponent } from './files/files.component';

export const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent, canActivate: [AuthGuardService], data: { routeName: 'Home', icon: 'home' } },
  { path: 'dashboard/:id', component: DashboardComponent, canActivate: [AuthGuardService],
    data: { routeName: 'Dashboard', icon: 'dashboard'} },
  { path: 'login', component: LoginComponent },
  { path: 'logout', component: LogoutComponent, canActivate: [AuthGuardService] },
  { path: 'second-factor', component: SecondFactorComponent, canActivate: [AuthGuardService] },
  // TODO: Fix registration later { path: 'registration', component: RegistrationComponent },
  { path: 'user-settings', component: UserSettingsComponent, canActivate: [AuthGuardService],
    data: { routeName: 'User Settings', icon: 'account_circle'} },
  { path: 'activity-log/:id', component: ActivityLogComponent, canActivate: [AuthGuardService],
    data: { routeName: 'Activity Log', icon: 'restore' } },
  { path: 'files', component: FilesComponent, canActivate: [AuthGuardService],
    data: { routeName: 'Files', icon: 'perm_media' } },
  { path: '**', redirectTo: 'home' },
];
