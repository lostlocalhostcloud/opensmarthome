import { Component, OnInit, ViewChild } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatPaginator, MatPaginatorModule, PageEvent } from '@angular/material/paginator';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatSort, MatSortModule } from '@angular/material/sort';
import { MatTableDataSource, MatTableModule } from '@angular/material/table';
import { ActivityLogType } from '../types';
import { BreakpointObserverToolService } from '../tools/breakpoint-observer-tool/breakpoint-observer-tool.service';
import { BreakpointObserver } from '@angular/cdk/layout';
import { BackendService } from '../backend.service';
import { UserSessionService } from '../application-management/user-session.service';
import { ActivatedRoute, Router } from '@angular/router';

interface ActivityLogEntry {
  date: string;
  dateShort: string;
  device: string;
  type: ActivityLogType;
  state: boolean;
  formattedState: string;
  user: string;
}

@Component({
  selector: 'app-activity-log',
  standalone: true,
    imports: [
        CommonModule,
        MatButtonToggleModule,
        MatExpansionModule,
        MatPaginatorModule,
        MatProgressBarModule,
        MatSortModule,
        MatTableModule
    ],
  templateUrl: './activity-log.component.html',
  styleUrl: './activity-log.component.scss'
})
export class ActivityLogComponent extends BreakpointObserverToolService implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator | undefined;
  @ViewChild(MatSort) sort: MatSort | undefined;

  loading: boolean | undefined;

  activityLogsErrorMessage: string | undefined;

  displayedColumns = ['date', 'device', 'type', 'state', 'user'];

  dataSource: MatTableDataSource<ActivityLogEntry> | null | undefined;

  initialPage = 1;
  initialPageSize = 10;

  smartHomeName: string | undefined;

  activityLogsSize = 0;
  activityLogs: Array<ActivityLogEntry> = [];
  activityLogsEnabled: boolean | undefined;

  currentTypeFilter: ActivityLogType | undefined;

  protected readonly activityLogTypeEnum = ActivityLogType;

  constructor(private breakpointObserver: BreakpointObserver,
              private route: ActivatedRoute,
              private router: Router,
              private userSessionService: UserSessionService,
              private backendService: BackendService) {
    super(breakpointObserver);
  }

  ngOnInit(): void {
    super.observeChanges();

    this.route.params.subscribe(async (params) => {
      if (params['id'] === ':id' || params['id'] === 'undefined') {
        this.activityLogsErrorMessage = 'Please choose your Home';
        await this.router.navigateByUrl('/activity-log/' + this.userSessionService.currentSmartHomeId);
        return;
      }
      this.userSessionService.currentSmartHomeId = params['id'];
      this.activityLogsErrorMessage = undefined;

      try {
        await this.fetchActivityLogs(params['id'], this.initialPage, this.initialPageSize);
        // @ts-ignore
        this.dataSource.paginator = this.paginator;
        // @ts-ignore
        this.dataSource.sort = this.sort;
      } catch (error: any) {
        this.dataSource = null;
        this.activityLogsErrorMessage = error.error;
      }
    });
  }

  async applyFilter(filter: string) {
    switch (filter) {
      case 'ALL':
      default:
        this.currentTypeFilter = undefined;
        await this.fetchActivityLogs(
          <number>this.userSessionService.currentSmartHomeId,
          this.initialPage,
          this.initialPageSize
        );
        break;
      case 'SENSOR':
        this.currentTypeFilter = ActivityLogType.SENSOR;
        await this.fetchActivityLogs(
          <number>this.userSessionService.currentSmartHomeId,
          this.initialPage,
          this.initialPageSize,
          this.currentTypeFilter
        );
        break;
      case 'TRIGGER':
        this.currentTypeFilter = ActivityLogType.TRIGGER;
        await this.fetchActivityLogs(
          <number>this.userSessionService.currentSmartHomeId,
          this.initialPage,
          this.initialPageSize,
          this.currentTypeFilter
        );
        break;
    }

    this.paginator?.firstPage();
  }

  async handlePageEvent(pageEvent: PageEvent) {
    await this.fetchActivityLogs(<number>this.userSessionService.currentSmartHomeId,
      pageEvent.pageIndex + 1,
      pageEvent.pageSize,
      this.currentTypeFilter);
  }

  private async fetchActivityLogs(smartHomeId: number, page: number, limit: number, type?: ActivityLogType) {
    this.loading = true;
    // The content must first be cleared so that the mobile page scrolls up again.
    this.activityLogsSize = 0;
    this.activityLogs = [];

    const activityLogs = await this.backendService.getActivityLogs(smartHomeId, page, limit, type);
    this.smartHomeName = activityLogs.smartHomeName;
    this.activityLogsSize = activityLogs.overallSize;

    const activityLogsToDisplay: any[] = [];
    activityLogs.activityLogs.forEach((activityLog: any) => activityLogsToDisplay.push({
      date: activityLog.formattedTimestamp,
      dateShort: activityLog.formattedTimestampShort,
      device: activityLog.deviceName,
      type: activityLog.type,
      state: activityLog.state ? activityLog.state === 'true' : null,
      formattedState: this.getActivityLogState(activityLog.deviceType, activityLog.state),
      user: activityLog.username
    }));

    this.activityLogs = activityLogsToDisplay;
    this.activityLogsEnabled = activityLogs.enabled;
    this.dataSource = new MatTableDataSource(this.activityLogs);
    this.loading = false;
  }

  private getActivityLogState(deviceType: string, state: string): string {
    if (state === '') {
      return state;
    }
    switch (deviceType) {
      case 'Switch':
        return state === 'true' ? 'On' : 'Off';
      case 'Garagedoor':
        return state === 'true' ? 'Open' : 'Closed';
      default:
        return state;
    }
  }
}
