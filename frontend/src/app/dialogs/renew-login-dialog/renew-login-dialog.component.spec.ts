import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RenewLoginDialogComponent } from './renew-login-dialog.component';

describe('TestDialogComponent', () => {
  let component: RenewLoginDialogComponent;
  let fixture: ComponentFixture<RenewLoginDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RenewLoginDialogComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RenewLoginDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
