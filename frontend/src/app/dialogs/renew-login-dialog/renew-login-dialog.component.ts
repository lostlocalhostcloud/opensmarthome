import { AfterViewInit, Component, Inject } from '@angular/core';
import {
  MAT_DIALOG_DATA,
  MatDialogActions,
  MatDialogClose,
  MatDialogContent,
  MatDialogModule,
  MatDialogRef,
  MatDialogTitle
} from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { CommonModule } from '@angular/common';
import { FormBuilder, FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';

@Component({
  selector: 'app-renew-login-dialog',
  standalone: true,
  imports: [
    MatDialogActions,
    MatDialogContent,
    MatButtonModule,
    MatDialogClose,
    MatDialogTitle,
    CommonModule,
    FormsModule,
    MatButtonModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule
  ],
  templateUrl: './renew-login-dialog.component.html',
  styleUrl: './renew-login-dialog.component.scss'
})
export class RenewLoginDialogComponent implements AfterViewInit {

  form: FormGroup;

  submitButtonDisabled: boolean = true;

  constructor(public dialogRef: MatDialogRef<RenewLoginDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: { afterUpdate: boolean; errorMessage: string; otpToken: string },
              private formBuilder: FormBuilder) {
    this.form = this.formBuilder.group({
      otpToken: ['']
    });
  }

  ngAfterViewInit(): void {
    this.form.controls['otpToken'].valueChanges.subscribe((value: string) => {
      this.submitButtonDisabled = value === '' || value === undefined;
    });
  }

  onCancelClick(): void {
    this.dialogRef.close();
  }

  submit(): void {
    this.dialogRef.close(this.form.controls['otpToken'].value);
  }
}
