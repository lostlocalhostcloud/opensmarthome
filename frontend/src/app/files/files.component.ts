import { Component, OnInit } from '@angular/core';
import { MatButton } from '@angular/material/button';
import { MatIcon } from '@angular/material/icon';
import { NgForOf, NgIf } from '@angular/common';
import { BackendService } from '../backend.service';
import { MatProgressBar } from '@angular/material/progress-bar';
import { MatExpansionModule } from '@angular/material/expansion';
import { FileCollection, FileCollectionsResponse, FileResponse } from '../types';

@Component({
  selector: 'app-files',
  standalone: true,
  imports: [
    MatButton,
    MatIcon,
    NgForOf,
    NgIf,
    MatProgressBar,
    MatExpansionModule
  ],
  templateUrl: './files.component.html',
  styleUrl: './files.component.scss'
})
export class FilesComponent implements OnInit {

  loading: boolean | undefined;

  imagePath = '/api/v1/file';

  collections: FileCollection[] = [];

  noCollectionsFoundImageId: string | undefined;

  currentlyOpenedCollectionId: number | undefined;

  constructor(private backendService: BackendService) {
  }

  ngOnInit(): void {
    this.loading = true;

    const lastOpenedCollectionInSession = sessionStorage.getItem('lastOpenedCollection');
    if (lastOpenedCollectionInSession) {
      this.currentlyOpenedCollectionId = Number(lastOpenedCollectionInSession);
    }

    this.backendService.fileCollections().then((response: FileCollectionsResponse) => {
      this.collections = response.collections;
      this.noCollectionsFoundImageId = response.noCollectionsFoundImageId;
    }).catch((error) => {
      console.error(error);
    }).finally(() => {
      this.loading = false;
    });
  }

  fetchFilesForCollection(collection: FileCollection) {
    this.currentlyOpenedCollectionId = collection.id;
    sessionStorage.setItem('lastOpenedCollection', String(this.currentlyOpenedCollectionId));

    if (!collection.files || collection.files.length === 0) {
      collection.files = [];
      this.backendService.filesByCollectionId(collection.id).then((response: FileResponse[]) => {
        collection.files = response;
      }).catch((error) => {
        console.error(error);
      });
    }
  }

  viewImage(file: any, download?: boolean): void {
    file.viewed = true;

    const link = document.createElement('a');
    link.setAttribute('target', '_blank');
    let url = this.imagePath + '/' + file.id;
    if (download) url += '/download'; else url += '/full';
    link.setAttribute('href', url);
    if (download) link.setAttribute('download', file.name);
    document.body.appendChild(link);
    link.click();
    link.remove();
  }
}
