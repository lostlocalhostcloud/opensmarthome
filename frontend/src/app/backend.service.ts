import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ActivityLogType, FileCollectionsResponse, FileResponse } from './types';
import { Observable } from 'rxjs';
import { UserSettings } from './user-settings/user-settings.component';

@Injectable({
  providedIn: 'root'
})
export class BackendService {

  constructor(private http: HttpClient) { }

  login(username: string, password: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.post('/api/v1/auth/login', { username, password }, { responseType: 'text' })
        .subscribe((response) => resolve(response), error => reject(error));
    });
  }

  otpVerify(otpToken: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.post('/api/v1/auth/otp-verify', { otpToken }, { responseType: 'text' })
        .subscribe(response => resolve(response), error => reject(error));
    });
  }

  preAuthorized(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.http.get('/api/v1/user/pre-authorized', { responseType: 'text' })
        .subscribe(response => resolve(true), error => reject(false));
    });
  }

  logout(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get('/api/v1/auth/logout', { responseType: 'text' })
        .subscribe((response) => {
            resolve(response);
          },
          (error) => {
            reject(error);
          });
    });
  }

  renewLogin(otpToken: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.post('/api/v1/auth/renew', { otpToken }, { responseType: 'text' })
        .subscribe((response) => {
            resolve(response);
          },
          (error) => {
            reject(error);
          });
    });
  }

  getUserInfo(): Observable<any> {
    return this.http.get('/api/v1/user/info');
  }

  getDevicesBySmartHomeId(smartHomeId: number): Promise<any> {
    return new Promise(((resolve, reject) => {
      this.http.get('/api/v1/smarthomes/' + smartHomeId + '/devices')
        .subscribe((response) => {
            resolve(response);
          },
          (error) => {
            reject(error);
          });
    }));
  }

  getDeviceSensorStatesBySmartHomeId(smartHomeId: number): Promise<any> {
    return new Promise(((resolve, reject) => {
      this.http.get('/api/v1/smarthomes/' + smartHomeId + '/devices/sensor_states')
        .subscribe((response) => {
            resolve(response);
          },
          (error) => {
            reject(error);
          });
    }));
  }

  getSessions(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get('/api/v1/user/sessions')
        .subscribe((response) => {
            resolve(response);
          },
          (error) => {
            reject(error);
          });
    });
  }

  getActivityLogs(smartHomeId: number, page: number, limit: number, type?: ActivityLogType): Promise<any> {
    return new Promise((resolve, reject) => {
      let params = new HttpParams()
        .set('page', page)
        .set('limit', limit);
      if (type) params = params.set('type', ActivityLogType[type]);
      this.http.get('/api/v1/smarthomes/' + smartHomeId + '/activity-log', {
        params: params
      }).subscribe((response) => {
          resolve(response);
        },
        (error) => {
          reject(error);
        });
    });
  }

  deleteSession(sessionId: number): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.post('/api/v1/user/delete-session', { sessionId }, { responseType: 'text' })
        .subscribe((response) => {
            resolve(response);
          },
          (error) => {
            reject(error);
          });
    });
  }

  getUserSettings(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get('/api/v1/user/user-settings')
        .subscribe((response) => {
            resolve(response);
          },
          (error) => {
            reject(error);
          });
    });
  }

  getSessionExpirations(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get('/api/v1/user/session-expirations')
        .subscribe((response) => {
            resolve(response);
          },
          (error) => {
            reject(error);
          });
    });
  }

  updateUserSettings(userSettings: UserSettings): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.post('/api/v1/user/user-settings', userSettings, { responseType: 'text' })
        .subscribe((response) => {
            resolve(response);
          },
          (error) => {
            reject(error);
          });
    });
  }

  getSmartHomes(): Promise<any> {
    return new Promise(((resolve, reject) => {
      this.http.get('/api/v1/smarthomes')
        .subscribe((response) => {
            resolve(response);
          },
          (error) => {
            reject(error);
          });
    }));
  }

  triggerDevice(smartHomeId: number, deviceId: string): Promise<any> {
    return new Promise(((resolve, reject) => {
      this.http.get('/api/v1/smarthomes/' + smartHomeId + '/devices/' + deviceId + '/trigger', { responseType: 'text' })
        .subscribe((response) => {
            resolve(response);
          },
          (error) => {
            reject(error);
          });
    }));
  }

  regCode(regCode: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.post('/api/v1/reg/regcode', { regCode }, { responseType: 'text' })
        .subscribe((response) => {
            resolve(response);
          },
          (error) => {
            reject(error);
          });
    });
  }

  verifyOTP(otpToken: string, baseSecret: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.post('/api/v1/reg/otp/verify', { otpToken, baseSecret }, { responseType: 'text' })
        .subscribe((response) => {
            resolve(response);
          },
          (error) => {
            reject(error);
          });
    });
  }

  verifyRegister(username: string, emailAddress: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.post('/api/v1/reg/register/verify', { username, emailAddress }, { responseType: 'text' })
        .subscribe((response) => {
            resolve(response);
          },
          (error) => {
            reject(error);
          });
    });
  }

  register(username: string,
           emailAddress: string,
           baseSecret: string,
           regCode: string,
           password: string,
           firstName?: string,
           lastName?: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.post('/api/v1/reg/register', {
        username,
        emailAddress,
        baseSecret,
        regCode,
        password,
        firstName,
        lastName
      }, { responseType: 'text' })
        .subscribe((response) => {
            resolve(response);
          },
          (error) => {
            reject(error);
          });
    });
  }

  fileCollections(): Promise<FileCollectionsResponse> {
    return new Promise((resolve, reject) => {
      this.http.get<FileCollectionsResponse>('/api/v1/file/collections')
        .subscribe((response) => {
            resolve(response);
          }, (error) => {
            reject(error);
        });
    });
  }

  filesByCollectionId(collectionId: number): Promise<FileResponse[]> {
    return new Promise((resolve, reject) => {
      this.http.get<FileResponse[]>('/api/v1/file/collections/' + collectionId + '/files')
        .subscribe((response) => {
            resolve(response);
          }, (error) => {
            reject(error);
          });
    });
  }
}
