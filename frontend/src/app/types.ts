export interface NotificationBarConfig {
  remainingDaysLoginIsValid?: number;
  type: NotificationBarEventType
}

export enum NotificationBarEventType {
  RENEW_LOGIN,
  RENEW_LOGIN_AFTER_SETTINGS_UPDATE,
  VERSION_OUTDATED,
}

export enum ActivityLogType {
  SENSOR = 'SENSOR',
  TRIGGER = 'TRIGGER'
}

export interface FileCollectionsResponse {
  collections: FileCollection[];
  noCollectionsFoundImageId: string;
}

export interface FileCollection {
  id: number;
  name: string;
  creator: string;
  sharedWith: string[];
  files: FileResponse[];
}

export interface FileResponse {
  id: string;
  name: string;
  timestamp: string;
  contentType: string;
  resolution: string;
  viewed: boolean;
}
