import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatOptionModule } from '@angular/material/core';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatSelectModule } from '@angular/material/select';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { BreakpointObserverToolService } from '../tools/breakpoint-observer-tool/breakpoint-observer-tool.service';
import { BreakpointObserver } from '@angular/cdk/layout';
import { BackendService } from '../backend.service';
import { AppComponent } from '../app.component';
import { NotificationBarEventType } from '../types';

export interface Session {
  id: number;
  createdAt: string;
  expireAt: string;
  current: boolean;
}

export interface UserSettings {
  username: string;
  emailAddress: string;
  sessionExpiration: string;
  secondFactorEnabled: boolean;
}

@Component({
  selector: 'app-user-settings',
  standalone: true,
    imports: [
        CommonModule,
        MatButtonModule,
        MatExpansionModule,
        MatFormFieldModule,
        MatOptionModule,
        MatProgressBarModule,
        MatSelectModule,
        MatTableModule,
        MatTabsModule
    ],
  templateUrl: './user-settings.component.html',
  styleUrl: './user-settings.component.scss'
})
export class UserSettingsComponent extends BreakpointObserverToolService implements OnInit {

  activeSessionsErrorMessage: string | null | undefined;

  activeSessionsData: Array<Session> = [];

  displayedColumns: string[] = ['id', 'createdAt', 'expireAt', 'revoke'];

  userSettingsErrorMessage: string | null | undefined;

  userSettings: UserSettings | undefined;

  sessionExpirations: string[] | undefined;

  settingsSaved: boolean | undefined;

  constructor(private backendService: BackendService,
              private appComponent: AppComponent,
              private breakPointObserver: BreakpointObserver) {
    super(breakPointObserver);
  }

  ngOnInit(): void {
    super.observeChanges();
    this.fetchSessionsAndFillTable();

    this.backendService.getUserSettings().then((userSettings) => {
      this.userSettings = userSettings;
      this.backendService.getSessionExpirations().then((sessionExpirations) => {
        this.sessionExpirations = sessionExpirations.map((sessionExpiration: { name: any; }) => sessionExpiration.name);
      }).catch(() => {
        this.userSettingsErrorMessage = 'Session Expirations could not be fetched';
      });
    }).catch((error) => {
      this.userSettingsErrorMessage = error.error;
    });
  }

  saveSettings(): void {
    this.userSettingsErrorMessage = null;

    if (this.userSettings) {
      this.backendService.updateUserSettings(this.userSettings).then((response: any) => {
        this.settingsSaved = true;
        setTimeout(() => this.settingsSaved = false, 2000);
        if (response !== 'No changes') {
          this.appComponent.openNotificationBarEventDialog(NotificationBarEventType.RENEW_LOGIN_AFTER_SETTINGS_UPDATE);
        }
      }).catch((error: any) => {
        this.userSettingsErrorMessage = 'Settings could not be saved - ' + error.error;
      });
    } else {
      this.userSettingsErrorMessage = 'Settings could not be saved. User settings not available.';
    }
  }

  deleteSession(session: Session): void {
    this.backendService.deleteSession(session.id).then(() => {
      this.fetchSessionsAndFillTable();
    }).catch((error) => {
      this.activeSessionsErrorMessage = error.error;
      setTimeout(() => {
        this.activeSessionsErrorMessage = null;
      }, 5000);
    });
  }

  private fetchSessionsAndFillTable(): void {
    this.backendService.getSessions().then((sessions) => {
      const activeSessions: Array<Session> = [];
      sessions.forEach((session: any) => {
        activeSessions.push({
          id: session.id,
          createdAt: session.createdAt,
          expireAt: session.expireAt,
          current: session.current
        });
      });
      this.activeSessionsData = activeSessions;
      this.activeSessionsErrorMessage = null;
    }).catch((error) => {
      if (error.status === 401) {
        window.location.reload();
      } else {
        this.activeSessionsData = [];
        this.activeSessionsErrorMessage = error.error;
      }
    });
  }
}
