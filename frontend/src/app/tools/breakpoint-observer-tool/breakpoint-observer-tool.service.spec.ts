import { TestBed } from '@angular/core/testing';

import { BreakpointObserverToolService } from './breakpoint-observer-tool.service';

describe('BreakpointObserverToolService', () => {
  let service: BreakpointObserverToolService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BreakpointObserverToolService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
