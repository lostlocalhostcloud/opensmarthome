import { Injectable } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';

@Injectable({
  providedIn: 'root'
})
export class BreakpointObserverToolService {

  private _breakpoints: number[] = [
    1,  // Breakpoints.XSmall
    2,  // Breakpoints.Small
    3,  // Breakpoints.Medium
    4,  // Breakpoints.Large
    5   // Breakpoints.XLarge
  ].sort((firstBreakpoint, secondBreakpoint) => {
    if (firstBreakpoint > secondBreakpoint) {
      return 1;
    }

    if (firstBreakpoint < secondBreakpoint) {
      return -1;
    }

    return 0;
  });

  private _currentBreakpoint: number | undefined;

  protected constructor(private _breakpointObserver: BreakpointObserver) { }

  public get breakpoints(): number[] {
    return this._breakpoints;
  }

  public get currentBreakpoint(): number {
    return <number>this._currentBreakpoint;
  }

  protected observeChanges(): void {
    this._breakpointObserver.observe([
      Breakpoints.XSmall,
      Breakpoints.Small,
      Breakpoints.Medium,
      Breakpoints.Large,
      Breakpoints.XLarge,
    ]).subscribe(() => this.breakpointChanged());
  }

  private breakpointChanged(): void {
    if (this._breakpointObserver.isMatched(Breakpoints.XLarge)) {
      this._currentBreakpoint = this._breakpoints[4];
    } else if (this._breakpointObserver.isMatched(Breakpoints.Large)) {
      this._currentBreakpoint = this._breakpoints[3];
    } else if (this._breakpointObserver.isMatched(Breakpoints.Medium)) {
      this._currentBreakpoint = this._breakpoints[2];
    } else if (this._breakpointObserver.isMatched(Breakpoints.Small)) {
      this._currentBreakpoint = this._breakpoints[1];
    } else if (this._breakpointObserver.isMatched(Breakpoints.XSmall)) {
      this._currentBreakpoint = this._breakpoints[0];
    }
  }
}
