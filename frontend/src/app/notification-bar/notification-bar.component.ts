import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatButton } from '@angular/material/button';
import { MatIcon } from '@angular/material/icon';
import { NgForOf, NgIf } from '@angular/common';
import { NotificationBarConfig, NotificationBarEventType } from '../types';

@Component({
  selector: 'app-notification-bar',
  standalone: true,
  imports: [
    MatButton,
    MatIcon,
    NgForOf,
    NgIf
  ],
  templateUrl: './notification-bar.component.html',
  styleUrl: './notification-bar.component.scss'
})
export class NotificationBarComponent implements OnInit {

  action: string | undefined;
  backgroundColor: string | undefined;
  color: string | undefined;
  icon: string | undefined;
  message: string | undefined;

  @Input({ required: true }) config: NotificationBarConfig | undefined;
  @Output() actionExecuted = new EventEmitter<NotificationBarEventType>();

  constructor() {
  }

  ngOnInit(): void {
    if (this.config) {
      switch (this.config.type) {
        case NotificationBarEventType.RENEW_LOGIN:
        case NotificationBarEventType.RENEW_LOGIN_AFTER_SETTINGS_UPDATE:
          this.action = 'Renew Login';
          this.backgroundColor = '#f44336';
          this.icon = 'rotate_right';
          if (this.config.remainingDaysLoginIsValid !== undefined) {
            this.message = this.config.remainingDaysLoginIsValid >= 1
              ? 'Login expires in ' + this.config.remainingDaysLoginIsValid + ' days.'
              : 'Login expires in less than a day.';
          }
          break;
        case NotificationBarEventType.VERSION_OUTDATED:
          this.action = 'Reload';
          this.backgroundColor = '#ffd740';
          this.color = '#000';
          this.icon = 'warning';
          this.message = 'Version outdated. Please reload.';
          break;
      }
    }
  }

  protected executeAction() {
    this.actionExecuted.emit(this.config?.type);
  }
}
