import { Component, OnDestroy, OnInit } from '@angular/core';
import { NavigationStart, Route, Router, RouterModule, RouterOutlet } from '@angular/router';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { map, Observable, shareReplay, Subscription } from 'rxjs';
import { NotificationBarConfig, NotificationBarEventType } from './types';
import { BackendService } from './backend.service';
import { AuthService } from './application-management/login/auth.service';
import { RenewLoginDialogComponent } from './dialogs/renew-login-dialog/renew-login-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { VERSION } from './version';
import { NotificationBarComponent } from './notification-bar/notification-bar.component';
import { UserSessionService } from './application-management/user-session.service';

interface Link {
  icon: string;
  name: string;
  path: string;
  children: {
    name: string;
    path: string;
  }[];
}

interface SmartHome {
  id: number;
  name: string;
}

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, CommonModule, MatButtonModule, MatIconModule, MatListModule, MatMenuModule, MatSidenavModule, MatToolbarModule, RouterModule, NotificationBarComponent],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'frontend';
  version: string | undefined;

  authenticationSubscription = new Subscription();

  authenticated = false;

  navigationLinks: Array<Link> = [];
  notificationBarEvents: NotificationBarConfig[] = [];

  smartHomes: Array<SmartHome> = [];
  smartHomesErrorMessage: string | null | undefined;

  loginRenewMessage: { successful: boolean; message: string } | null | undefined;

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(private breakpointObserver: BreakpointObserver,
              private authService: AuthService,
              private userSessionService: UserSessionService,
              private router: Router,
              private backendService: BackendService,
              private dialog: MatDialog) {}

  ngOnInit(): void {
    this.redirectIfHashRouterUrlIsUsed();
    this.authenticationSubscription = this.authService.authenticated.subscribe((authenticated) => {
      if (authenticated) {
        this.navigationLinks = this.formatLinks(this.router.config);

        this.backendService.getSmartHomes().then((response) => {
          this.smartHomesErrorMessage = null;
          this.smartHomes = [];

          response.forEach((res: any) => {
            this.smartHomes.push({ id: res.id, name: res.name });
          });
        }).catch(() => {
          this.smartHomesErrorMessage = 'Error on loading the smart homes';
        });

        this.authenticated = true;
        const isVersionNumber = VERSION.split('.').length >= 2;
        this.version = isVersionNumber ? 'v' + VERSION : VERSION;
      } else {
        this.navigationLinks = [];
        this.authenticated = false;
        this.version = undefined;
      }
    });
    this.authService.loginExpiresSoon.subscribe((remainingDaysLoginIsValid) => this.pushNotificationBarEvent({
      remainingDaysLoginIsValid,
      type: NotificationBarEventType.RENEW_LOGIN
    }));
    this.userSessionService.versionDifference.subscribe((version) => {
      if (version) {
        this.pushNotificationBarEvent({
          type: NotificationBarEventType.VERSION_OUTDATED
        });
      }
    });
  }

  private pushNotificationBarEvent(config: NotificationBarConfig): void {
    const filteredEvents = this.notificationBarEvents.filter(event => event.type !== config.type);
    this.notificationBarEvents = [config, ...filteredEvents];
  }

  ngOnDestroy(): void {
    this.authenticationSubscription.unsubscribe();
    this.authService.loginExpiresSoon.unsubscribe();
    this.userSessionService.versionDifference.unsubscribe();
  }

  private redirectIfHashRouterUrlIsUsed(): void {
    // https://stackoverflow.com/a/49534503
    this.router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        if (!!event.url && event.url.match(/^\/#/)) {
          this.router.navigate([event.url.replace('/#', '')]);
        }
      }
    });
  }

  private formatLinks(routes: Route[]): Link[] {
    const links: Array<Link> = [];
    for (const route of routes) {
      if (route.data?.['routeName']) {
        const currentPath = '/' + route.path;
        const link: Link = { name: route.data?.['routeName'], path: currentPath, children: [], icon: route.data?.['icon'] ?? '' };
        if (route.children) {
          for (const children of route.children) {
            if (children.data?.['routeName']) {
              link.children.push({ name: children.data?.['routeName'], path: currentPath + '/' + children.path });
            }
          }
        }
        links.push(link);
      }
    }
    return links;
  }

  protected notificationBarActionExecuted(notificationBarEventType: NotificationBarEventType) {
    switch (notificationBarEventType) {
      case NotificationBarEventType.RENEW_LOGIN:
      case NotificationBarEventType.RENEW_LOGIN_AFTER_SETTINGS_UPDATE:
        this.openNotificationBarEventDialog(notificationBarEventType);
        break;
      case NotificationBarEventType.VERSION_OUTDATED:
        window.location.reload();
        break;
    }
  }

  public openNotificationBarEventDialog(notificationBarEventType: NotificationBarEventType): void {
    switch (notificationBarEventType) {
      case NotificationBarEventType.RENEW_LOGIN:
        this.openRenewLoginDialog(false);
        break;
      case NotificationBarEventType.RENEW_LOGIN_AFTER_SETTINGS_UPDATE:
        this.openRenewLoginDialog(true);
        break;
    }
  }

  private openRenewLoginDialog(afterUpdate: boolean, errorMessage?: string): void {
    const dialogRef = this.dialog.open(RenewLoginDialogComponent, {
      width: '250px',
      data: {
        afterUpdate,
        errorMessage
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result !== '' && result !== undefined) {
        this.backendService.renewLogin(result).then(() => {
          this.loginRenewMessage = { successful: true, message: 'Login successfully renewed' };
          // The Session needs to be reloaded, so that the user session service gets the expiration of the new Login
          window.location.reload();
        }).catch((error) => {
          this.openRenewLoginDialog(false, error.error);
        }).finally(() => {
          setTimeout(() => {
            this.loginRenewMessage = null;
          }, 5000);
        });
      }
    });
  }
}
