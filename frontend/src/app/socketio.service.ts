import { Injectable } from '@angular/core';
import { io, Socket } from 'socket.io-client';

@Injectable({
  providedIn: 'root'
})
export class SocketioService {

  private socket: Socket | undefined;

  constructor() { }

  private joinSmartHomeGroup(): void {
    this.socket = io('/', {
      path: '/socket.io'
    });
  }

  public disconnectSocket(): void {
    this.socket?.disconnect();
  }

  public setupSensorEventListener(onEvent: (data: any) => void): void {
    if (!this.socket || !this.socket.connected) {
      this.joinSmartHomeGroup();
    }
    this.socket?.on('SensorEvent', (data: any) => {
      onEvent(data);
    });
  }
}
