#!/bin/bash

BETA_BRANCH="work-on-release"
BETA_TAG="beta"
MICROSERVICES=("authentication" "file-management" "registration" "smarthome-management")
VERSION_FILE=VERSION

if ! test -f ${VERSION_FILE}; then
  echo "ERROR: VERSION file does not exist"
  exit 1
fi

VERSION=$(cat ${VERSION_FILE})
VERSION_ALREADY_EXISTS_MESSAGE="Version ${VERSION} already exist"

if curl -s --fail "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/repository/tags/${VERSION}" > /dev/null; then
  if [[ "${CI_COMMIT_BRANCH}" == "${CI_DEFAULT_BRANCH}" ]]; then
    echo "ERROR: ${VERSION_ALREADY_EXISTS_MESSAGE}"
    exit 1
  fi
  echo "WARNING: ${VERSION_ALREADY_EXISTS_MESSAGE}"
fi

if [[ "${CI_COMMIT_BRANCH}" == "${BETA_BRANCH}" ]]; then
  echo "Pipeline is running on ${BETA_BRANCH}. Set version to ${BETA_TAG}."
  echo "${BETA_TAG}" > $VERSION_FILE
  VERSION="${BETA_TAG}"
elif [[ "${CI_COMMIT_BRANCH}" != "${CI_DEFAULT_BRANCH}" ]]; then
  echo "Pipeline is not running on ${CI_DEFAULT_BRANCH} or ${BETA_BRANCH}. Set version to ${CI_COMMIT_SHORT_SHA} and tags to ${CI_COMMIT_SHA}."
  echo "${CI_COMMIT_SHA}" > $VERSION_FILE
  VERSION="${CI_COMMIT_SHORT_SHA}"
fi

sed -i "s/REPLACE_VERSION_HERE/${VERSION}/g" "frontend/src/app/version.ts"
sed -i "s/REPLACE_VERSION_HERE/${VERSION}/g" "gateway/src/version.ts"
for MICROSERVICE in "${MICROSERVICES[@]}"; do
  sed -i "s/REPLACE_VERSION_HERE/${VERSION}/g" "microservices/${MICROSERVICE}/src/version.ts"
done
