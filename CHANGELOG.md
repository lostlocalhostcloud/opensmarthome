# CHANGELOG

## Release 4.6.0 (2025-01-27)

### Security

- Updated npm Dependencies (npm audit fix)

### Added

- Added automatic creation and saving of preview files #87 !64
- Added Admin API endpoints for file management #87 !64
- Added automatic obtaining of image resolution and timestamp #87 !64
- Added complete list of associated users to each collection #87 !64

### Changed

- Lazy load files to improve performance #87 !64
- Removed File viewed call from Frontend and use it instead in File endpoints #87 !64

## Release 4.5.0 (2024-12-08)

### Security

- Updated npm Dependencies (npm audit fix)

### Fixed

- Fixed notification bar icon and text alignment #85 !61
- Rewrote login expiry message #85 !61

### Added

- Added App version headers for Frontend and Microservices #85 !61
- Added Notification for version difference #85 !61
- Added check for login expiration to every request #85 !61

### Changed

- Updated Frontend to Angular 18 #78 !63
- Updated Frontend image to newest nginx version #78 !63

## Release 4.4.0 (2024-11-03)

### Fixed

- Frontend: Fixed version number display
- Pipeline: Fixed missing file-management version artifact

### Added

- Added optional image for display if no collections are found #84 !60

## Release 4.3.0 (2024-08-04)

### Security

- Updated npm Dependencies (npm audit fix)

### Added

- Activity Log for SmartHome #48 !41
- File management Microservice #82 !58

### Changed

- Updated Frontend to Angular 17 #71 !55
- Gateway: Use newest Node 16 (16.20.2) image because of onoff dependency #77 !56

## Release 4.2.0 (2023-12-02)

### BREAKING CHANGES

- Deployment

### Security

- Updated npm Dependencies
- Fix Frontend Production build #68

### Added

- Frontend: Add autocomplete Attributes in Login fields
- DB Changelog with Liquibase (Cloud Microservices) #58
- Second-Factor on different component #57
- User changeable Session Expiration #62
- Automatic delete expired sessions from the database #65

### Changed

- Switch Gateway from PostgreSQL to SQLite

### Fixed

- Fixed Gateway Healthchecks
- Socket.IO communication (Milestone-8) #63 #66 #67 #70

## Release 4.1.1 (2023-07-07)

### Added

- Gateway Health endpoints
- Automatic Healthchecks between gateway and smarthome-management microservice for automatic reconnects

### Fixed

- Automatic reconnect for Gateway

## Release 4.1.0 (2022-06-18)

### Security

- Updated npm Dependencies (Including migrations to new versions with breaking changes)
- Updated Frontend to Angular 13

### Added

- Horizontal Pod Autoscalers for authentication and frontend
- DeepSource Dockerfile analyzer
- Registration Microservice
- Deployment liveness probes
- Auto-Patch (Npm Audit Fix Auto)

### Fixed

- Entrypoints in publish stage
- Cobertura coverage reports
- Possible null bug in registration microservice

### Changed

- Improved Dockerfiles
- Deployment Resource limits

### Removed

- Deployment Resource requests

## Release 4.0.1 (2021-11-20)
### Successor of previous smarthome-without-lamp software
* Cloud
  * Frontend (Angular)
  * Authentication
    * OTP
  * SmartHome Management
    * Fetch Devices and Device States from Gateway
    * Gateway Connection Controller
    * Trigger devices (Cloud Trigger)
* Gateway
    * HTTP
      * Trigger
      * Action (formdata, url encode, json, xml, text/plain)
      * Sensor
    * GPIO
      * Trigger
      * Action
      * Sensor
    * Cloud
      * Trigger
* Pipeline
    * Liniting
    * Unit testing
    * Docker builds with kaniko
    * Kubernetes Deployment
   